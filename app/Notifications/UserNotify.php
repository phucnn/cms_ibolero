<?php

namespace App\Notifications;

use App\Helpers\Helpers;
use App\Notify;
use App\Setting;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserNotify extends Notification implements ShouldBroadcast
{
    use Queueable;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $dataNotify;

    public $tries = 3;

    public function __construct($dataNotify)
    {
        //
        $this->dataNotify = $dataNotify;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**npm install -g laravel-echo-server
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->dataNotify;
    }

    public function toBroadcast($notifiable)
    {
        $information  = config('information');
        $return = [];
        $data = $this->dataNotify;
        $userFrom = User::find($data['from']);
        $key = (new User())->prefixCountNotifyUnRead.$data['to'];
        //        update so notify chưa đọc
        if(\Cache::has($key)){
            \Cache::increment($key, 1);
        }else{
            \Cache::forever($key,1);
        }
        $countNotifyUnread = \Cache::get($key);

        $return['countUnRead'] = $countNotifyUnread;
        if (!empty($userFrom)) {
            if ($data['type'] == 'send-message') {
                $return['message'] = '<p class="mb-0">' . $data["message"] . '</p>';
                $return['title'] = $userFrom->full_name;
                $return['icon'] = $userFrom->getAvatar();
            }
            if ($data['type'] == 'add-friend') {
                $return['message'] = '<p class="mb-0">Đã gửi cho bạn lời mời kết bạn.</p><p class="float-right mt-2 mb-0"><button data-remote="'.$data['acceptedFriendUrl'].'" type="button" class="btn btn-sm badge-pill btn-success mr-1 acceptedFriendOk is_notify"><i class="fal fa-check-circle mr-1"></i>Đồng ý</button><button type="button" class="btn btn-sm badge-pill btn-danger acceptedFriendCancel is_notify"><i class="fal fa-ban mr-1"></i>Từ chối</button></p>';
                $return['title'] = $userFrom->full_name;
                $return['icon'] = $userFrom->getAvatar();
            }
            if ($data['type'] == 'system-send-message') {
                $return['message'] = '<p class="mb-0">' . $data["message"] . '</p>';
                $return['title'] = 'Ôn thi tiếng anh';
                $return['icon'] =$information['logo'];
            }
        }
        return $return;
    }
}
