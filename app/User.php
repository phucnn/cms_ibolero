<?php

namespace App;

use App\Overrides\Notification\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Maklad\Permission\Traits\HasRoles;
use Auth;


/**
 * App\User
 *
 * @property-read mixed $id
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Maklad\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Maklad\Permission\Models\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User role($roles)
 * @mixin \Eloquent
 */
class User extends Eloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use Notifiable;
    use HasRoles;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'users';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = '_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'password', 'full_name', 'email', 'gender', 'is_change_pass', 'favorite_song', 'status', 'created_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $prefixCountNotifyUnRead = 'un-read-notify-';

    public function getData($id){
        return self::findOrFail($id);
    }

    public function createdBy()
    {
        return $this->belongsTo(Admin::class, 'created_by');
    }


    public function artists()
    {
        return $this->belongsToMany(Artist::class);
    }

    public function composers()
    {
        return $this->belongsToMany(Composer::class);
    }

    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            self::newData($table);
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::id();
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            self::newData($table);
        });
    }

    public static function newData($table)
    {
        $table->slug = str_slug($table->name);
        $table->name_ansii = str_replace('-', ' ',$table->slug);
        $table->updated_by = Auth::id();
        $table->created_by = !empty($table->created_by) ? $table->created_by : Auth::id();
    }

}