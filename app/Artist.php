<?php

namespace App;



use App\Helpers\Helpers;
use Auth;
use Jenssegers\Mongodb\Eloquent\Model;

class Artist extends Model
{
    //
    protected $fillable = ['name', 'thumbnail_url', 'status', 'priority', 'description', 'created_by'];
    protected $collection='artist';



    public function getData($id){
        return self::findOrFail($id);
    }

    public function createdBy()
    {
        return $this->belongsTo(Admin::class, 'created_by');
    }

    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            self::newData($table);
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::id();
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            self::newData($table);
        });
    }

    public static function newData($table)
    {
        $table->slug = str_slug($table->name);
        $table->name_ansii = str_replace('-', ' ',$table->slug);
        $table->updated_by = Auth::id();
        $table->created_by = !empty($table->created_by) ? $table->created_by : Auth::id();
    }
}
