<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 12/6/2018
 * Time: 2:26 PM
 */

namespace App\Overrides\Notification;

use Jenssegers\Mongodb\Eloquent\Model;

class DatabaseNotification  extends Model
{
    protected $fillable=array('data');
//    protected $connection = 'notifications';
    public function markAsRead()
    {
        if (is_null($this->read_at)) {
            $this->forceFill(['read_at' => $this->freshTimestamp()])->save();
        }
    }

    /**
     * Mark the notification as unread.
     *
     * @return void
     */
    public function markAsUnread()
    {
        if (! is_null($this->read_at)) {
            $this->forceFill(['read_at' => null])->save();
        }
    }

    /**
     * Determine if a notification has been read.
     *
     * @return bool
     */
    public function read()
    {
        return $this->read_at !== null;
    }

    /**
     * Determine if a notification has not been read.
     *
     * @return bool
     */
    public function unread()
    {
        return $this->read_at === null;
    }
}

