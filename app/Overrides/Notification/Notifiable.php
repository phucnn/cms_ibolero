<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 12/6/2018
 * Time: 2:27 PM
 */
namespace  App\Overrides\Notification;
use App\Overrides\Notification\HasDatabaseNotifications;
use Illuminate\Notifications\RoutesNotifications;
trait Notifiable
{
    use HasDatabaseNotifications, RoutesNotifications;

//    use \Illuminate\Notifications\Notifiable {
//        \Illuminate\Notifications\Notifiable::notifications as baseNotificationsMethod;
//    }
//
//    public function notifications()
//    {
//        if (config('database.default') === 'mongodb') {
//            return $this->morphMany(DatabaseNotification::class, 'notifiable')
//                ->orderBy('created_at', 'desc');
//        }
//        return $this->DatabaseNotification();
//    }
}