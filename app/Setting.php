<?php

namespace App;


use Auth;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * App\Setting
 *
 * @property-read mixed $id
 * @mixin \Eloquent
 */
class Setting extends Model
{
    //
    protected $fillable = ['name', 'type', 'value'];
    protected $collection = 'setting';
    protected $primaryKey = 'type';

    public static function getValueByType($type)
    {
        $value = self::find($type, ['value']);
        return !empty($value->value) ? $value->value : "";
    }

    public static function boot()
    {
        parent::boot();

        // create a event to happen on updating
        static::updating(function ($table) {
            $table->updated_by = Auth::id();
        });

        // create a event to happen on deleting
        static::deleting(function ($table) {
            $table->deleted_by = Auth::id();
        });

        // create a event to happen on saving
        static::saving(function ($table) {
            $table->created_by = Auth::id();
        });
    }
}
