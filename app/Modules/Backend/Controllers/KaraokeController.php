<?php

namespace App\Modules\Backend\Controllers;

use App\Admin;
use App\Artist;
use App\Composer;
use App\Helpers\Common;
use App\Helpers\Helpers;
use App\LogActionUser;
use App\Karaoke;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Song;
use Carbon\Carbon;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class KaraokeController extends Controller
{
    protected $karaoke;

    public function __construct(Karaoke $karaoke)
    {
        $this->karaoke = $karaoke;
    }

    /**
     * danh sách 
     */
    public function index(Request $request)
    {
        if (!empty($request->get('typeChange'))) {
            $typeSubmit = $request->get('typeChange');
            $listId = $request->get('id');

            if (!empty($listId)) {
                switch ($typeSubmit) {
                    case 'publish':
                        Karaoke::whereIn('_id', $listId)
                            ->update(['status' => "1"]);
                        break;
                    case 'hidden':
                        Karaoke::whereIn('_id', $listId)
                            ->update(['status' => "0"]);
                        break;
                    case 'delete':
                        Karaoke::whereIn('_id', $listId)
                            ->update(['status' => "-1"]);
                        break;
                }
            }
            return redirect()->back();
        }
        return view('Backend::karaoke.index');
    }

    public function create()
    {
        $listSong = Song::where('status', '1')->get();
        $listCategory = Setting::where('type', 'CATEGORY')->first();
        return view('Backend::karaoke.create', compact(['listSong',  'listCategory']));
    }

    public function store(Request $karaokeRequest)
    {
        $this->validate($karaokeRequest, [
            'name' => 'required',
            'category' => 'required'
        ]);
        $input = $karaokeRequest->all();

        $songs = $karaokeRequest->get('song_ids');
        $data = $this->karaoke->create($input);
        $data->songs()->sync($songs);

        return redirect()->route('backend.karaoke.list')
            ->with('flash_message',
                $karaokeRequest->name . ' thêm mới thành công.');
    }

    /**
     * View request
     */
    public function edit($id)
    {
        $data = $this->karaoke->getData($id);
        if (!$data) {
            return redirect()->back();
        }
        $listSong = Song::where('status', '1')->get();
        $listCategory = Setting::where('type', 'CATEGORY')->first();
        $views = LogActionUser::where('karaoke_id', $id)->where('type', 'watch_video')->count();
        $addFavorite = LogActionUser::where('karaoke_id', $id)->where('type', 'add_favorite')->count();
        $removeFavorite = LogActionUser::where('karaoke_id', $id)->where('type', 'remove_favorite')->count();
        $favorite = $addFavorite - $removeFavorite;

        return view('Backend::karaoke.edit', compact(['data', 'listSong', 'listCategory', 'views', 'favorite']));
    }

    public function update(Request $karaokeRequest, $id)
    {
        $data = $this->karaoke->getData($id);
        $input = $karaokeRequest->all();
        $this->validate($karaokeRequest, [
            'name' => 'required',
            'category' => 'required'
        ]);

        $songs = $karaokeRequest->get('song_ids');

        // xóa đi để apply vị trí mới
        $data->songs()->detach($data->song_ids);
        $data->song_ids = [];

        $data->fill($input)->update();
        $data->songs()->sync($songs);

        return redirect()->route('backend.karaoke.list')
            ->with('flash_message',
                $karaokeRequest->name . ' thay đổi thành công');
    }

    public function destroy($id)
    {
        $data = $this->karaoke->getData($id);
        $data->status = "-1";
        $data->save();
        return redirect()->route('backend.karaoke.list')
            ->with('flash_message', $data->name .
                ' xóa thành công.');
    }

    /**
     * Json data table request
     */
    public function dataTable(Request $request)
    {
        //store cache
        $input = [
            'search' => $request->get('search', []),
            'search_name_id' => $request->get('search_name_id', ''),
            'status'=> $request->get('status',''),
            'length' => $request->get('length', 1),
            'start' => $request->get('start', 0),
            'draw' => $request->get('draw', 1),
        ];

        $query = $this->karaoke->orderBy('created_at', "DESC");
        if (!empty($input['search']['value'])) {
            $query->where('name', 'like', "%" . $input['search']['value'] . "%");
            $query->orWhere('_id', $input['search']['value']);
        };
        if (!empty($input['search_name_id'])) {
            $query->where('name', 'like', "%" . $input['search_name_id'] . "%");
            $query->orWhere('_id', $input['search_name_id']);
        };

        if ($input['status'] != "") {
            $query->where('status', (string)$input['status']);
        }else{
            $query->where('status', '!=', "-1");
        };

        $countTotal = $query->count();

        if (!empty($input['length'])) {
            $query->limit((int)$input['length']);
        }
        if (!empty($input['start'])) {
            $query->skip((int)$input['start']);
        }

        $data = $query->with(
            [
                'createdBy' => function ($q) {
                    $q->select('name');
                },
                'songs'
            ]
        )->get();

        $json = [
            "recordsTotal" => $data->count(),
            "recordsFiltered" => $countTotal,
            'data' => []
        ];
		if(!empty($data)){
            $listCategory = Setting::where('type', 'CATEGORY')->first();
            $arrCategory = [];
            foreach($listCategory->value as $value){
                $arrCategory[$value['code']] = $value['name'];
            }
			foreach ($data as $item => $value) {
                $urlEdit = route('backend.karaoke.edit', $value->_id);
                $urlDelete = route('backend.karaoke.delete', $value->_id);

                $showCategory = '';
                if(!empty($value->category)){
                    foreach($value->category as $oneValue){
                        $showCategory.= @$arrCategory[$oneValue].' - ';
                    }
                    $showCategory = rtrim($showCategory,' - ');
                }
                $statusChecked = $value->status == "1" ? "checked" : "";

                $json['data'][$item]['checkbox'] = '<input class="checkID" type="checkbox" name="id[]" value="' . $value->_id . '" data-id="' . $value->_id . '">';
                $json['data'][$item]['name'] = '<a href="' . $urlEdit . '">' . $value->name . '</a><br/>#'.$value->hashtag;
                $json['data'][$item]['thumbnail_url'] =!empty($value->thumbnail_url)? '<img class="img-responsive" style="width: 100px;" src="' .$value->thumbnail_url. '" alt="">' : '';
                $json['data'][$item]['link_video'] = '<video width="360" height="240" controls>
                                                          <source src="' . $value->link_stream["360"] . '" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                      </video>';
                $json['data'][$item]['category'] = $showCategory;
                $json['data'][$item]['created_time'] = '<p>' . $value->created_at . '</p>';
                $json['data'][$item]['status'] = '<label class="switch">
									<input type="checkbox" ' . $statusChecked . ' onchange="confirmStatus(this,\'' . $value->_id . '\')">
									<span class="slider round"></span>';
                $json['data'][$item]['action'] = '
				                <a href="' . $urlEdit . '">
                                    <button type="button" class="btn btn-xs btn-primary" title="Chỉnh sửa"><i class="fa fa-edit"></i></button>
                                </a>
                                <a href="' . $urlDelete . '">
                                    <button type="button" class="btn btn-xs btn-danger" title="Xóa"><i class="fa fa-trash"></i></button>
                                </a>';

			}
        }
		return response()->json($json);
    }

}
