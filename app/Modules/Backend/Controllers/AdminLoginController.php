<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/20/2018
 * Time: 9:32 PM
 */
namespace App\Modules\Backend\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Admin;
class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware(['guest:admin']);
    }
    public function showLoginForm()
    {
        return view('Backend::admins.login');
    }
    public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        // Attempt to log the user in
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('backend.dashboard'));
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}