<?php

namespace App\Modules\Backend\Controllers;

use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Song;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use function PHPSTORM_META\type;


class SettingController extends Controller
{

    public function index()
    {
        return view('Backend::setting.index');
    }

    public function package(Request $request)
    {
        $name = "Quản lý gói cước";
        $data = Setting::where('type', 'PACKAGE')->first();

        if ($request->isMethod('post')) {
            $value = $request->get('data');
            Setting::updateOrCreate(['type' => 'PACKAGE'], ['type' => 'PACKAGE', "value" => array_values($value)]);
            return redirect()->back()
                ->with('flash_message',
                    $name . ' thay đổi thành công');
        }
        return view('Backend::setting.package', compact(['data', 'name']));
    }

    public function page(Request $request)
    {
        $name = "Thiết đặt trang";
        $data = Setting::where('type', 'PAGE')->first();

        if ($request->isMethod('post')) {
            $value = [
                'title' => $request->get('title'),
                'header' => $request->get('header'),
                'footer' => $request->get('footer'),
                'logo' => $request->get('logo'),
                'banner' => $request->get('banner'),
            ];
            Setting::updateOrCreate(['type' => 'PAGE'], ['type' => 'PAGE', "value" => $value]);
            return redirect()->back()
                ->with('flash_message',
                    $name . ' thay đổi thành công');
        }
        return view('Backend::setting.page', compact(['data', 'name']));
    }

    public function category(Request $request)
    {
        $name = "Quản lý chuyên mục";
        $data = Setting::where('type', 'CATEGORY')->first();

        if ($request->isMethod('post')) {
            $value = $request->get('data');
            Setting::updateOrCreate(['type' => 'CATEGORY'], ['type' => 'CATEGORY', "value" => array_values($value)]);
            return redirect()->back()
                ->with('flash_message',
                    $name . ' thay đổi thành công');
        }
        return view('Backend::setting.category', compact(['data', 'name']));
    }

    public function banner(Request $request)
    {
        $name = "Quản lý banner";
        $data = Setting::where('type', 'BANNER')->first();
        $listSong = Song::where('status', '1')->get();

        if ($request->isMethod('post')) {
            $value = $request->get('data');
            Setting::updateOrCreate(['type' => 'BANNER'], ['type' => 'BANNER', "value" => array_values($value)]);
            return redirect()->back()
                ->with('flash_message',
                    $name . ' thay đổi thành công');
        }
        return view('Backend::setting.banner', compact(['data', 'name', 'listSong']));
    }

}

