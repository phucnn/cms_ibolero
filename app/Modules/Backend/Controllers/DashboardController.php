<?php

namespace App\Modules\Backend\Controllers;


use App\Artist;
use App\Card;
use App\CommissionLog;
use App\Composer;
use App\Helpers\Common;
use App\Helpers\Helpers;
use App\Income;
use App\Song;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        //self::firstCheck();
        $user = Auth::user();
        return view('Backend::dashboard.index', compact(['user']));
    }

    public static function changeStatus(Request $request)
    {
        $id = $request->id;
        $newStatus = $request->newStatus;
        $type = $request->type;
        $data = false;
        switch ($type) {
            case 'song':
                $data = Song::findOrFail($id);
                break;
            case 'artist':
                $data = Artist::findOrFail($id);
                break;
            case 'composer':
                $data = Composer::findOrFail($id);
                break;
            case 'user':
                $data = User::findOrFail($id);
                break;
        }
        if ($data && $data->status != $newStatus) {
            $data->status = (string)$newStatus;
            $data->updated_by = Auth::id();
            $data->save();
        }
        return response()->json([]);
    }
}