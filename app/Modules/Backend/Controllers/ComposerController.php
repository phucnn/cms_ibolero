<?php

namespace App\Modules\Backend\Controllers;

use App\Admin;
use App\Helpers\Common;
use App\Helpers\Helpers;
use App\Composer;
use App\Http\Controllers\Controller;
use App\Setting;
use Carbon\Carbon;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ComposerController extends Controller
{
    protected $composer;

    public function __construct(Composer $composer)
    {
        $this->composer = $composer;
    }

    /**
     * danh sách 
     */
    public function index(Request $request)
    {
        return view('Backend::composer.index');
    }

    public function create()
    {
        return view('Backend::composer.create');
    }

    public function store(Request $composerRequest)
    {
        $this->validate($composerRequest, [
            'name' => 'required',
        ]);
        $input = $composerRequest->all();
        $this->composer->create($input);
        return redirect()->route('backend.composer.list')
            ->with('flash_message',
                $composerRequest->name . ' thêm mới thành công.');
    }

    /**
     * View request
     */
    public function edit($id)
    {
        $data = $this->composer->getData($id);
        if (!$data) {
            return redirect()->back();
        }

        return view('Backend::composer.edit', compact(['data']));
    }

    public function update(Request $composerRequest, $id)
    {
        $data = $this->composer->getData($id);
        $input = $composerRequest->all();
        $this->validate($composerRequest, [
            'name' => 'required',
        ]);
        $data->fill($input)->update();
        return redirect()->route('backend.composer.list')
            ->with('flash_message',
                $composerRequest->name . ' thay đổi thành công');
    }

    public function destroy($id)
    {
        $data = $this->composer->getData($id);
        $data->status = "-1";
        $data->save();
        return redirect()->route('backend.composer.list')
            ->with('flash_message', $data->name .
                ' xóa thành công.');
    }

    /**
     * Json data table request
     */
    public function dataTable(Request $request)
    {
        //store cache
        $input = [
            'search' => $request->get('search', []),
            'search_name_id' => $request->get('search_name_id', ''),
            'status'=> $request->get('status',''),
            'length' => $request->get('length', 1),
            'start' => $request->get('start', 0),
            'draw' => $request->get('draw', 1),
        ];

        $query = $this->composer->orderBy('created_at', "DESC");
        if (!empty($input['search']['value'])) {
            $query->where('name', 'like', "%" . $input['search']['value'] . "%");
            $query->orWhere('_id', $input['search']['value']);
        };
        if (!empty($input['search_name_id'])) {
            $query->where('name', 'like', "%" . $input['search_name_id'] . "%");
            $query->orWhere('_id', $input['search_name_id']);
        };

        if ($input['status'] != "") {
            $query->where('status', (string)$input['status']);
        }else{
            $query->where('status', '!=', "-1");
        };

        $countTotal = $query->count();

        if (!empty($input['length'])) {
            $query->limit((int)$input['length']);
        }
        if (!empty($input['start'])) {
            $query->skip((int)$input['start']);
        }

        $data = $query->with(
            [
                'createdBy' => function ($q) {
                    $q->select('name');
                },
            ]
        )->get();

        $json = [
            "recordsTotal" => $data->count(),
            "recordsFiltered" => $countTotal,
            'data' => []
        ];
		if(!empty($data)){
			foreach ($data as $item => $value) {
                $urlEdit = route('backend.composer.edit', $value->_id);
                $urlDelete = route('backend.composer.delete', $value->_id);
                $json['data'][$item]['name'] = '<a href="' . $urlEdit . '">' . $value->name . '</a>';
                $json['data'][$item]['thumbnail_url'] =!empty($value->thumbnail_url)? '<img class="img-responsive" style="width: 100px;" src="' .$value->thumbnail_url. '" alt="">' : '';
                $json['data'][$item]['created_time'] = '<p>' . $value->created_at . '</p>';
                $json['data'][$item]['status'] = @Helpers::statusView($value->status, false);
                $json['data'][$item]['action'] = '
				                <a href="' . $urlEdit . '">
                                    <button type="button" class="btn btn-xs btn-primary" title="Chỉnh sửa"><i class="fa fa-edit"></i></button>
                                </a>
                                <a href="' . $urlDelete . '">
                                    <button type="button" class="btn btn-xs btn-danger" title="Xóa"><i class="fa fa-trash"></i></button>
                                </a>';

			}
        }
		return response()->json($json);
    }

}
