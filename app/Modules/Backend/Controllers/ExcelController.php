<?php

namespace App\Modules\Backend\Controllers;

use App\Artist;
use App\Composer;
use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Song;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

class ExcelController extends Controller
{

    protected $song;

    public function __construct(Song $song)
    {
        $this->song = $song;
    }
    public function download()
    {
        $pathToFile = public_path("/uploads/song_import_example.xlsx");
        return response()->download($pathToFile);
    }

    public function import()
    {
        ini_set('memory_limit', '-1'); // unlimited memory limit
        ini_set('max_execution_time', 3000);
        if (Input::hasFile('import_file')) {
            $path = Input::file('import_file')->getRealPath();
            return $this->importFile($path, 'question');
        } else {
            return back()->with('flash_message', "Chưa chọn file upload");
        }
    }


    /**
     * @param $sheetObject
     * @param array $listArtist
     * @param array $listComposer
     * @param array $listCategory
     * @return array
     */
    public function getDataFromSheet($sheetObject, $listArtist= [], $listComposer= [], $listCategory= []){
        $results = ['fails' =>[], 'data' => []];
        foreach ($sheetObject as $item => $value) {
            if(empty($value['name']) || empty($value['link_video'])){
                $results['fails'][] = "Dòng ".($item+2)." Không có tên bài hát hoặc link video";
                continue;
            }

            $value['artist_ids'] = [];
            // nhiều người hát
            if (strpos($value['artist_upload'], '-') !== false) {
                $artist = explode('-', $value['artist_upload']);
                foreach($artist as $oneValue){
                    if(isset($listArtist[str_slug(trim($oneValue))])){
                        $value['artist_ids'][] = $listArtist[str_slug(trim($oneValue))];
                    }
                }
            }else{
                if(isset($listArtist[str_slug(trim($value['artist_upload']))])){
                    $value['artist_ids'][] = $listArtist[str_slug(trim($value['artist_upload']))];
                }
            }

            $value['composer_ids'] = [];
            // nhiều tác giả
            if (strpos($value['composer_upload'], '-') !== false) {
                $composer = explode('-', $value['composer_upload']);
                foreach($composer as $oneValue){
                    if(isset($listComposer[str_slug(trim($oneValue))])){
                        $value['composer_ids'][] = $listComposer[str_slug(trim($oneValue))];
                    }
                }
            }else{
                if(isset($listComposer[str_slug(trim($value['composer_upload']))])){
                    $value['composer_ids'][] = $listComposer[str_slug(trim($value['composer_upload']))];
                }
            }

            $value['category'] = [];
            // nhiều tác giả
            if (strpos($value['category_upload'], '-') !== false) {
                $category = explode('-', $value['category_upload']);
                foreach($category as $oneValue){
                    var_dump($oneValue);
                    if(isset($listCategory[str_slug(trim($oneValue))])){
                        $value['category'][] = $listCategory[str_slug(trim($oneValue))];
                    }
                }
            }else{
                if(isset($listCategory[str_slug(trim($value['category_upload']))])){
                    $value['category'][] = $listCategory[str_slug(trim($value['category_upload']))];
                }
            }

            $value['is_upload'] = "1";
            $value['status'] = "0";
            unset($value['stt']);
            $results['data'][$item] = $value;
        }
        return $results;
    }

    /**
     * Import
     * @param $path
     * @param $type
     * @return \Illuminate\Http\RedirectResponse
     */
    public function importFile($path)
    {
        if ($path) {
            $import_success = $import_fail = array();
            $loadExcel = Excel::load($path)->ignoreEmpty()->get();
            if (!empty($loadExcel) and $loadExcel->count()) {
                $listArtist = Artist::where('status', '1')->get();
                $listComposer = Composer::where('status', '1')->get();
                $listCategory = Setting::where('type', 'CATEGORY')->first();

                // make array
                $listArtistArr = [];
                foreach($listArtist as $oneValue){
                    $listArtistArr[$oneValue->slug] = $oneValue->_id;
                }

                $listComposerArr = [];
                foreach($listComposer as $oneValue){
                    $listComposerArr[$oneValue->slug] = $oneValue->_id;
                }

                $listCategoryArr = [];
                foreach($listCategory['value'] as $oneValue){
                    $listCategoryArr[$oneValue['code']] = $oneValue['code'];
                };

                $results = $this->getDataFromSheet($loadExcel[0]->toArray(), $listArtistArr, $listComposerArr, $listCategoryArr);

                if(empty($results) && empty($import_fail)){
                    $import_fail[] = 'Không đọc được dữ liệu';
                }

                if(!empty($results)){
                    foreach ($results['data'] as $i => $oneSong) {
                        try {
                            $save = $this->song->create($oneSong);
                            $import_success[$i]['name'] = $save->name;
                            $import_success[$i]['id'] = $save->_id;
                            $save->artists()->sync($oneSong['artist_ids']);
                            $save->composers()->sync($oneSong['composer_ids']);
                        } catch (QueryException $exception) {
                            $import_fail[] = $oneSong['name'];
                        }
                    }

                }

                return back()->with('count_import_fail', count($import_fail))
                    ->with('count_import_success', count($import_success))
                    ->with('import_fail', $import_fail)
                    ->with('import_success', $import_success);
            } else {
                return back()->with('flash_message', 'Không tìm thấy file. Vui lòng kiểm tra lại');
            }
        } else {
            return back()->with('flash_message', 'Bạn chưa chọn file');
        }
    }


}
