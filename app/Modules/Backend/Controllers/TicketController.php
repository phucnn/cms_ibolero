<?php

namespace App\Modules\Backend\Controllers;

use App\Admin;
use App\Helpers\Common;
use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class TicketController extends Controller
{

    protected $ticket;

    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    public function index(Request $request)
    {
        $listUsers = Admin::all();
        return view('Backend::ticket.index', compact([ 'listUsers']));
    }

    public function edit($id)
    {
        $data = $this->ticket->getData($id);
        if (!$data) {
            return redirect()->back();
        }
        // chưa xem -> đã xem
        $users = Auth::user();
        $isAdmin = $users->can(['admins-manager']);
        $viewCheck = $isAdmin ? $data->admin_view : $data->user_view;
        if($viewCheck == 0){
            if($isAdmin){
                $data->admin_view = 1;
            }else{
                $data->user_view = 1;
            }
            $data->save();
        }
        $listAgency = Common::getAllAgent();

        return view('Backend::ticket.edit', compact(['data', 'listAgency']));
    }

    public function send(Request $request){
        $input = [
            'subject' => $request->get('subject'),
            'content' => $request->get('content'),
            'type' => $request->get('type'),
            'status' => 0,
            'user_view' => 1,
            'admin_view' => 0,
            'log_chat' => array([
                'user' => Auth::id(),
                'content' => $request->get('content'),
                'time' => date('d-m-Y H:i:s')
            ])
        ];
        $data = $this->ticket->create($input);
        if(!$data){
            return redirect()->back()
                ->withErrors(['msg', 'Có lỗi xảy ra. Xin thử lại hoặc liên hệ hotline']);
        }
        return redirect()->route('backend.ticket.list')
            ->withErrors(['msg', 'Báo lỗi thành công']);
    }

    public function reply(Request $request, $id){
        $data = $this->ticket->findOrFail($id);
        $users = Auth::user();
        $isAdmin = $users->can(['admins-manager']);
        $logChat = $data->log_chat;
        $logChat[] = [
            'user' => Auth::id(),
            'content' => $request->get('reply'),
            'time' => date('d-m-Y H:i:s')
        ];

        if($isAdmin){
            $data->user_view = 0;
            $data->status = 1;
        }else{
            $data->admin_view = 0;
            $data->status = 0;
        }
        $data->log_chat = $logChat;
        $data->save();

        return back()
            ->with('flash_message',
                $request->name . ' Trả lời thành công');
    }

    public function dataTable(Request $request)
    {
        //store cache
        $input = [
            'search'=>$request->get('search',''),
            'status'=> $request->get('status',''),
            'type'=> $request->get('type','')
        ];

        $users = Auth::user();
        $isAdmin = $users->can(['admins-manager']);

        if($isAdmin){
            $query = $this->ticket->orderBy('admin_view', "ASC");
            $created_by = $request->get('created_by','');
            if(!empty($created_by)){
                $query->where('created_by', $created_by);
            }
        }else{
            $query = $this->ticket->orderBy('user_view', "ASC");
            $query->where('created_by', Auth::id());
        }

        if (!empty($input['search'])) {
            $query->where('subject', 'like', "%" . $input['search'] . "%");
            $query->orWhere('_id', $input['search']);
        };

        if ($input['status'] != "") {
            $query->where('status', intval($input['status']));
        };

        if ($input['type'] != "") {
            $query->where('type', (string)$input['type']);
        };

        $data = $query->with(
            [
                'createdBy' => function ($q) {
                    $q->select('name');
                },
            ]
        )->get();

        $json = ['data' => []];
        if(!empty($data)){
            foreach ($data as $item => $value) {
                $urlEdit = route('backend.ticket.edit', $value->_id);
                $json['data'][$item]['subject'] ='<a href="' . $urlEdit . '">' . $value->subject . '</a>';
                $json['data'][$item]['name'] = @$value->createdBy->name;
                $json['data'][$item]['type'] = @Helpers::TICKET_TYPE[$value->type];
                $json['data'][$item]['time_information'] = '<p>' . $value->created_at . '</p>';
                $json['data'][$item]['status'] = @Helpers::TICKET_STATUS[$value->status];
                $json['data'][$item]['action'] = '<a href="' . $urlEdit . '"><button type="button" class="btn btn-xs btn-primary" title="Chỉnh sửa"><i class="fa fa-edit"></i></button></a>';
                $json['data'][$item]['view_check'] = $isAdmin ? $value->admin_view : $value->user_view;
            }
        }
        return response()->json($json);
    }

}
