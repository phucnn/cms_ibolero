<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:37 PM
 */

namespace App\Modules\Backend\Controllers;


use Illuminate\Http\Request;
use Auth;
use Maklad\Permission\Models\Permission;
use Maklad\Permission\Models\Role;
use Session;

class PermissionController extends Controller
{


    public function index()
    {
        $permissions = Permission::where('guard_name', 'admin')->get();
        return view('Backend::permissions.index')->with('permissions', $permissions);
    }

    public function create()
    {
        $roles = Role::where('guard_name', 'admin')->get();
        return view('Backend::permissions.create')->with('roles', $roles);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions|max:40',
            'display_name' => 'required',
        ]);

        $name = $request['name'];
        $display_name = $request['display_name'];
        $permission = new Permission();
        $permission->name = $name;
        $permission->display_name = $display_name;
        $permission->guard_name = 'admin';

        $roles = $request['roles'];

        $permission->save();

        if (!empty($request['roles'])) { //If one or more role
            foreach ($roles as $role) {
                $r = Role::find($role);
                $permission = Permission::where('name', '=', $name)->first();
                $r->givePermissionTo($permission);
            }
        }

        return redirect()->route('backend.permissions.list')
            ->with('flash_message',
                'Permission ' . $permission->name . ' added!');
    }

    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        return view('Backend::permissions.edit', compact('permission'));
    }

    public function update(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name' => 'required||max:40|unique:permissions,id,'.$id,
            'display_name' => 'required',
        ]);
        $input = $request->all();
        $a = $permission->fill($input)->save();

        return redirect()->route('backend.permissions.list')
            ->with('flash_message',
                'Permission ' . $permission->name . ' updated!');
    }

    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);

        if ($permission->name == "user-destroy") {
            return redirect()->route('backend.permissions.list')
                ->with('flash_message',
                    'Cannot delete this Permission!');
        }

        $permission->delete();

        return redirect()->route('backend.permissions.list')
            ->with('flash_message',
                'Permission xóa thành công!');
    }
}