<?php
/**
 * Created by PhpStorm.
 * Admin: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:49 PM
 */

namespace App\Modules\Backend\Controllers;


use App\CommissionLog;
use Hash;
use Illuminate\Http\Request;
use App\Admin;
use Auth;
use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;
use Session;

class AdminController extends Controller
{
    public function index()
    {
        $admins = Admin::all();
        return view('Backend::admins.index')->with('admins', $admins);
    }

    public function create()
    {
        $roles = Role::get();
        return view('Backend::admins.create', ['roles' => $roles]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required|max:120',
            'email'    => 'required|email|unique:admins',
            'phone'    => 'required|unique:admins|max:20',
            'password' => 'required|min:6|confirmed',
        ]);
        $data = [
            'name'=>$request->get('name'),
            'phone'=>$request->get('phone'),
            'email'=>$request->get('email'),
            'password'=>Hash::make($request->get('password'))
        ];

        $user = Admin::create($data);
        $roles = $request['roles']; //Retrieving the roles field
        //Checking if a role was selected
        if (isset($roles)) {
            foreach ($roles as $role) {
                $role_r = Role::findOrFail($role);
                $user->assignRole($role_r); //Assigning role to user
            }
        }
        //Redirect to the Backend::admins.index view and display message
        return redirect()->route('backend.admins.list')
            ->with('flash_message',
                'Admin thêm mới thành công.');
    }

    public function edit($id)
    {
        $user = Admin::findOrFail($id);
        $roles = Role::all(); //Get all roles
        return view('Backend::admins.edit', compact('user', 'roles')); //pass user and roles data to view
    }

    public function update(Request $request, $id)
    {
        $user = Admin::findOrFail($id);

        $input = $request->only(['name', 'email', 'phone']);
        $input['password']='';
        $roles = $request['roles'];
        if (!empty($request->password)) {
            $this->validate($request, [
                'name'        => 'required|max:120',
                'email'       => 'required|email|unique:admins,id,' . $id,
                'phone'       => 'required|max:20|unique:admins,id,' . $id,
                'password'    => 'required|min:6|confirmed'
            ]);
            $input['password'] = Hash::make($request->password);
        } else {
            $this->validate($request, [
                'name'  => 'required|max:120',
                'email' => 'required|email|unique:admins,id,' . $id,
                'phone'       => 'required|max:20|unique:admins,id,' . $id
            ]);
            unset($input['password']);
        }
        $user->fill($input)->save();

        if (isset($roles)) {
            $user->syncRoles(array_values($roles));
        }
        return redirect()->route('backend.admins.list')
            ->with('flash_message',
                'Admin thay đổi thành công');
    }

    public function destroy($id)
    {
        $user = Admin::findOrFail($id);
        $user->delete();

        return redirect()->route('backend.admins.list')
            ->with('flash_message',
                'Admin xóa thành công.');
    }

    public function getLogin()
    {
        return view('Backend::admins.login');
    }

    public function postLogin(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::guard('admin')->attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && !$lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function getLogout()
    {
        Auth::guard('admin')->logout();
        return redirect(route("backend.login"));
    }

}


