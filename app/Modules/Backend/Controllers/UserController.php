<?php

namespace App\Modules\Backend\Controllers;

use App\Admin;
use App\Artist;
use App\Composer;
use App\Helpers\Common;
use App\Helpers\Helpers;
use App\Song;
use App\User;
use App\Http\Controllers\Controller;
use App\Setting;
use Carbon\Carbon;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * danh sách 
     */
    public function index(Request $request)
    {
        if (!empty($request->get('typeChange'))) {
            $typeSubmit = $request->get('typeChange');
            $listId = $request->get('id');

            if (!empty($listId)) {
                switch ($typeSubmit) {
                    case 'publish':
                        User::whereIn('_id', $listId)
                            ->update(['status' => "1"]);
                        break;
                    case 'hidden':
                        User::whereIn('_id', $listId)
                            ->update(['status' => "0"]);
                        break;
                    case 'delete':
                        User::whereIn('_id', $listId)
                            ->update(['status' => "-1"]);
                        break;
                }
            }
            return redirect()->back();
        }
        return view('Backend::user.index');
    }

    public function create()
    {
        return view('Backend::user.create', compact(['listArtist']));
    }

    public function store(Request $userRequest)
    {
        $this->validate($userRequest, [
            'name' => 'required',
            'artist_ids' => 'required',
            'composer_ids' => 'required',
            'category' => 'required'
        ]);
        $input = $userRequest->all();

        $artists = $userRequest->get('artist_ids');
        $composers = $userRequest->get('composer_ids');

        $data = $this->user->create($input);
        $data->artists()->sync($artists);
        $data->composers()->sync($composers);

        return redirect()->route('backend.user.list')
            ->with('flash_message',
                $userRequest->name . ' thêm mới thành công.');
    }

    /**
     * View request
     */
    public function edit($id)
    {
        $data = $this->user->getData($id);
        if (!$data) {
            return redirect()->back();
        }
        $favorite = Song::whereIn('_id', $data->favorite_song)->get();
        return view('Backend::user.edit', compact(['data', 'favorite']));
    }

    public function update(Request $request, $id)
    {
        $data = $this->user->getData($id);
        $input = $request->only(['full_name', 'email', 'phone', 'gender', 'is_change_pass']);
        $input['password']='';
        $roles = $request['roles'];
        if (!empty($request->password)) {
            $this->validate($request, [
                'full_name'        => 'required|max:120',
                'email'       => 'required|email|unique:admins,id,' . $id,
                'phone'       => 'required|max:20|unique:admins,id,' . $id,
                'password'    => 'required|min:6|confirmed'
            ]);
            $input['password'] = Hash::make($request->password);
        } else {
            $this->validate($request, [
                'full_name'  => 'required|max:120',
                'email' => 'required|email|unique:admins,id,' . $id,
                'phone'       => 'required|max:20|unique:admins,id,' . $id
            ]);
            unset($input['password']);
        }
        $data->fill($input)->save();

        return redirect()->route('backend.user.list')
            ->with('flash_message',
                $data->phone . ' thay đổi thành công');
    }

    public function destroy($id)
    {
        $data = $this->user->getData($id);
        $data->status = "-1";
        $data->save();
        return redirect()->route('backend.user.list')
            ->with('flash_message', $data->name .
                ' xóa thành công.');
    }

    /**
     * Json data table request
     */
    public function dataTable(Request $request)
    {
        //store cache
        $input = [
            'search' => $request->get('search', []),
            'search_name_id' => $request->get('search_name_id', ''),
            'status'=> $request->get('status',''),
            'length' => $request->get('length', 1),
            'start' => $request->get('start', 0),
            'draw' => $request->get('draw', 1),
        ];

        $query = $this->user->orderBy('created_at', "DESC");
        if (!empty($input['search']['value'])) {
            $query->where('name', 'like', "%" . $input['search']['value'] . "%");
            $query->orWhere('_id', $input['search']['value']);
        };
        if (!empty($input['search_name_id'])) {
            $query->where('name', 'like', "%" . $input['search_name_id'] . "%");
            $query->orWhere('_id', $input['search_name_id']);
        };

        if (!empty($input['phone'])) {
            $query->where('phone', 'like', "%" . $input['phone'] . "%");
        };

        if ($input['status'] != "") {
            $query->where('status', (string)$input['status']);
        }else{
            $query->where('status', '!=', "-1");
        };

        $countTotal = $query->count();

        if (!empty($input['length'])) {
            $query->limit((int)$input['length']);
        }
        if (!empty($input['start'])) {
            $query->skip((int)$input['start']);
        }

        $data = $query->get();

        $json = [
            "recordsTotal" => $data->count(),
            "recordsFiltered" => $countTotal,
            'data' => []
        ];
		if(!empty($data)){

			foreach ($data as $item => $value) {
                $urlEdit = route('backend.user.edit', $value->_id);
                $urlDelete = route('backend.user.delete', $value->_id);
                $statusChecked = $value->status == "1" ? "checked" : "";
                $json['data'][$item]['checkbox'] = '<input class="checkID" type="checkbox" name="id[]" value="' . $value->_id . '" data-id="' . $value->_id . '">';
                $json['data'][$item]['name'] = '<a href="' . $urlEdit . '">' . $value->full_name . '</a>';
                $json['data'][$item]['phone'] = '<p>' . $value->phone . '</p>';
                $json['data'][$item]['email'] = '<p>' . $value->email . '</p>';
                $json['data'][$item]['gender'] = '<p>' . ($value->gender == "1" ? "Nam" : "Nữ") . '</p>';
                $json['data'][$item]['is_change_pass'] = '<p>' . ($value->is_change_pass == "1" ? "Có" : "Không") . '</p>';
                $json['data'][$item]['status'] = '<label class="switch">
									<input type="checkbox" ' . $statusChecked . ' onchange="confirmStatus(this,\'' . $value->_id . '\')">
									<span class="slider round"></span>';
                $json['data'][$item]['action'] = '
				                <a href="' . $urlEdit . '">
                                    <button type="button" class="btn btn-xs btn-primary" title="Chỉnh sửa"><i class="fa fa-edit"></i></button>
                                </a>
                                <a href="' . $urlDelete . '">
                                    <button type="button" class="btn btn-xs btn-danger" title="Xóa"><i class="fa fa-trash"></i></button>
                                </a>';

			}
        }
		return response()->json($json);
    }

}
