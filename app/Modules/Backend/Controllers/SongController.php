<?php

namespace App\Modules\Backend\Controllers;

use App\Admin;
use App\Artist;
use App\Composer;
use App\Helpers\Common;
use App\Helpers\Helpers;
use App\LogActionUser;
use App\Song;
use App\Http\Controllers\Controller;
use App\Setting;
use Carbon\Carbon;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class SongController extends Controller
{
    protected $song;

    public function __construct(Song $song)
    {
        $this->song = $song;
    }

    /**
     * danh sách 
     */
    public function index(Request $request)
    {
        if (!empty($request->get('typeChange'))) {
            $typeSubmit = $request->get('typeChange');
            $listId = $request->get('id');

            if (!empty($listId)) {
                switch ($typeSubmit) {
                    case 'publish':
                        Song::whereIn('_id', $listId)
                            ->update(['status' => "1"]);
                        break;
                    case 'hidden':
                        Song::whereIn('_id', $listId)
                            ->update(['status' => "0"]);
                        break;
                    case 'delete':
                        Song::whereIn('_id', $listId)
                            ->update(['status' => "-1"]);
                        break;
                }
            }
            return redirect()->back();
        }
        return view('Backend::song.index');
    }

    public function create()
    {
        $listArtist = Artist::where('status', '1')->get();
        $listComposer = Composer::where('status', '1')->get();
        $listCategory = Setting::where('type', 'CATEGORY')->first();
        return view('Backend::song.create', compact(['listArtist', 'listComposer', 'listCategory']));
    }

    public function store(Request $songRequest)
    {
        $this->validate($songRequest, [
            'name' => 'required',
            'artist_ids' => 'required',
            'composer_ids' => 'required',
            'category' => 'required'
        ]);
        $input = $songRequest->all();

        $artists = $songRequest->get('artist_ids');
        $composers = $songRequest->get('composer_ids');

        $data = $this->song->create($input);
        $data->artists()->sync($artists);
        $data->composers()->sync($composers);

        return redirect()->route('backend.song.list')
            ->with('flash_message',
                $songRequest->name . ' thêm mới thành công.');
    }

    /**
     * View request
     */
    public function edit($id)
    {
        $data = $this->song->getData($id);
        if (!$data) {
            return redirect()->back();
        }
        $listArtist = Artist::where('status', '1')->get();
        $listComposer = Composer::where('status', '1')->get();
        $listCategory = Setting::where('type', 'CATEGORY')->first();
        $views = LogActionUser::where('song_id', $id)->where('type', 'watch_video')->count();
        $addFavorite = LogActionUser::where('song_id', $id)->where('type', 'add_favorite')->count();
        $removeFavorite = LogActionUser::where('song_id', $id)->where('type', 'remove_favorite')->count();
        $favorite = $addFavorite - $removeFavorite;

        return view('Backend::song.edit', compact(['data', 'listArtist', 'listComposer', 'listCategory', 'views', 'favorite']));
    }

    public function update(Request $songRequest, $id)
    {
        $data = $this->song->getData($id);
        $input = $songRequest->all();
        $this->validate($songRequest, [
            'name' => 'required',
            'artist_ids' => 'required',
            'composer_ids' => 'required',
            'category' => 'required'
        ]);

        $artists = $songRequest->get('artist_ids');
        $composers = $songRequest->get('composer_ids');

        // xóa đi để apply vị trí mới
        $data->artists()->detach($data->artist_ids);
        $data->composers()->detach($data->composer_ids);
        $data->artist_ids = [];
        $data->composer_ids = [];

        $data->fill($input)->update();
        $data->artists()->sync($artists);
        $data->composers()->sync($composers);

        return redirect()->route('backend.song.list')
            ->with('flash_message',
                $songRequest->name . ' thay đổi thành công');
    }

    public function destroy($id)
    {
        $data = $this->song->getData($id);
        $data->status = "-1";
        $data->save();
        return redirect()->route('backend.song.list')
            ->with('flash_message', $data->name .
                ' xóa thành công.');
    }

    /**
     * Json data table request
     */
    public function dataTable(Request $request)
    {
        //store cache
        $input = [
            'search' => $request->get('search', []),
            'search_name_id' => $request->get('search_name_id', ''),
            'status'=> $request->get('status',''),
            'length' => $request->get('length', 1),
            'start' => $request->get('start', 0),
            'draw' => $request->get('draw', 1),
        ];

        $query = $this->song->orderBy('created_at', "DESC");
        if (!empty($input['search']['value'])) {
            $query->where('name', 'like', "%" . $input['search']['value'] . "%");
            $query->orWhere('_id', $input['search']['value']);
        };
        if (!empty($input['search_name_id'])) {
            $query->where('name', 'like', "%" . $input['search_name_id'] . "%");
            $query->orWhere('_id', $input['search_name_id']);
        };

        if ($input['status'] != "") {
            $query->where('status', (string)$input['status']);
        }else{
            $query->where('status', '!=', "-1");
        };

        $countTotal = $query->count();

        if (!empty($input['length'])) {
            $query->limit((int)$input['length']);
        }
        if (!empty($input['start'])) {
            $query->skip((int)$input['start']);
        }

        $data = $query->with(
            [
                'createdBy' => function ($q) {
                    $q->select('name');
                },
                'artists',
                'composers'
            ]
        )->get();

        $json = [
            "recordsTotal" => $data->count(),
            "recordsFiltered" => $countTotal,
            'data' => []
        ];
		if(!empty($data)){
            $listCategory = Setting::where('type', 'CATEGORY')->first();
            $arrCategory = [];
            foreach($listCategory->value as $value){
                $arrCategory[$value['code']] = $value['name'];
            }
			foreach ($data as $item => $value) {
                $urlEdit = route('backend.song.edit', $value->_id);
                $urlDelete = route('backend.song.delete', $value->_id);
                $listArtists = '';
                foreach($value->artists as $oneValue){
                    $listArtists.= $oneValue->name.' - ';
                }
                $listArtists = rtrim($listArtists,' - ');

                $listComposers = '';
                foreach($value->composers as $oneValue){
                    $listComposers.= $oneValue->name.' - ';
                }

                $showCategory = '';
                if(!empty($value->category)){
                    foreach($value->category as $oneValue){
                        $showCategory.= @$arrCategory[$oneValue].' - ';
                    }
                    $showCategory = rtrim($showCategory,' - ');
                }

                $statusChecked = $value->status == "1" ? "checked" : "";

                $listComposers = rtrim($listComposers,' - ');
                $json['data'][$item]['checkbox'] = '<input class="checkID" type="checkbox" name="id[]" value="' . $value->_id . '" data-id="' . $value->_id . '">';
                $json['data'][$item]['name'] = '<a href="' . $urlEdit . '">' . $value->name . '</a><br/>#'.$value->hashtag;
                $json['data'][$item]['thumbnail_url'] =!empty($value->thumbnail_url)? '<img class="img-responsive" style="width: 100px;" src="' .$value->thumbnail_url. '" alt="">' : '';
                $json['data'][$item]['link_video'] = '<video width="360" height="240" controls>
                                                          <source src="' . $value->link_stream["360"] . '" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                      </video>';
                $json['data'][$item]['composer'] = $listComposers;
                $json['data'][$item]['artist'] = $listArtists;
                $json['data'][$item]['category'] = $showCategory;
                $json['data'][$item]['created_time'] = '<p>' . $value->created_at . '</p>';
                $json['data'][$item]['status'] = '<label class="switch">
									<input type="checkbox" ' . $statusChecked . ' onchange="confirmStatus(this,\'' . $value->_id . '\')">
									<span class="slider round"></span>';
                $json['data'][$item]['action'] = '
				                <a href="' . $urlEdit . '">
                                    <button type="button" class="btn btn-xs btn-primary" title="Chỉnh sửa"><i class="fa fa-edit"></i></button>
                                </a>
                                <a href="' . $urlDelete . '">
                                    <button type="button" class="btn btn-xs btn-danger" title="Xóa"><i class="fa fa-trash"></i></button>
                                </a>';

			}
        }
		return response()->json($json);
    }

}
