<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:40 PM
 */

namespace App\Modules\Backend\Controllers;


use Illuminate\Http\Request;
use Auth;
use Maklad\Permission\Models\Permission;
use Maklad\Permission\Models\Role;
use Session;

class RoleController extends Controller {

    public function __construct()
    {
//        $this->middleware(['auth', 'isAdmin']);//middleware
    }

    public function index()
    {
        $roles = Role::where('guard_name', 'admin')->get();
        $permission = Permission::where('guard_name', 'admin')->get();
        return view('Backend::roles.index')->with(['roles' => $roles, 'listPermissions' => $permission]);
    }

    public function create()
    {
        $permissions = Permission::where('guard_name', 'admin')->get();
        return view('Backend::roles.create', ['permissions'=>$permissions]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
                'name'=>'required|unique:roles|max:100',
                'permissions' =>'required',
                'display_name' => 'required',
            ]
        );

        $name = $request['name'];
        $display_name = $request['display_name'];
        $role = new Role();
        $role->name = $name;
        $role->display_name = $display_name;
        $role->guard_name = 'admin';


        $permissions = $request['permissions'];

        $role->save();

        foreach ($permissions as $permission) {
            $p = Permission::where('_id', '=', $permission)->firstOrFail();
            $role = Role::where('name', '=', $name)->first();
            $role->givePermissionTo($p);
        }

        return redirect()->route('backend.roles.list')
            ->with('flash_message',
                'Role '. $role->display_name.' added!');
    }


    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::where('guard_name', 'admin')->get();

        return view('Backend::roles.edit', compact('role', 'permissions'));
    }

    public function update(Request $request, $id)
    {

        $role = Role::findOrFail($id);

        $this->validate($request, [
            'name'=>'required|max:100|unique:roles,id,'.$id,
            'permissions' =>'required',
            'display_name' => 'required',
        ]);


        $input = $request->except(['permissions']);
        $permissions = $request['permissions'];
        $role->fill($input)->save();

        $p_all = Permission::where('guard_name', 'admin')->get();

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p);
        }


        foreach ($permissions as $permission) {
            $p = Permission::where('_id', '=', $permission)->firstOrFail();
            $role->givePermissionTo($p);
        }


        return redirect()->route('backend.roles.list')
            ->with('flash_message',
                'Role '. $role->display_name.' updated!');
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $name = $role->display_name;
        $role->delete();

        return redirect()->route('backend.roles.list')
            ->with('flash_message',
                'Role '. $name.' xóa thành công!');
    }

    public function refresh(Request $request, $type = 0){
        $roles = Role::where('guard_name', 'admin')->get();
        $p_all = Permission::where('guard_name', 'admin')->get();
        foreach ($p_all as $p) {
            $rs = $p->role_ids;
            if(!self::checkFormatJson($rs)) {
                $temp = array();
                foreach ($rs as $key => $r) {
                    $temp[count($temp)] = $r;
                }

                $p->role_ids = $temp;
                $p->save();
            }
        }

        foreach($roles as $role){
            $permissions = $role->permission_ids;
            if($permissions){
                if(!self::checkFormatJson($permissions)) {
                    $temp = array();
                    foreach ($permissions as $key => $permission) {
                        $temp[count($temp)] = $permission;
                    }

                    $role->permission_ids = $temp;
                    $role->save();
                    $permissions = $role->permission_ids;
                }
                foreach ($p_all as $p) {
                    $role->revokePermissionTo($p);
                }

                foreach ($permissions as $permission) {
                    $p = Permission::where('_id', '=', $permission)->firstOrFail();
                    $role->givePermissionTo($p);
                }
            }
        }
        if($type == 1){
            return true;
        }else{
            return redirect()->route('backend.roles.list')
                ->with('flash_message',
                    'Refresh phân quyền thành công!');
        }

    }

    private function checkFormatJson($arrs) {
        foreach ($arrs as $key => $arr) {
            if(!is_int($key))
                return false;
            if($key >= count($arrs))
                return false;
        }
        return true;
    }

    public static function changeStatus(Request $request)
    {
        $role_id = $request->role_id;
        $permission_id = $request->permission_id;
        $newStatus = $request->newStatus;

        $role  = Role::findOrFail($role_id);
        $permission = Permission::findOrFail($permission_id);
        $listPermission = $role->permission_ids;
        $listRole = $permission->role_ids;
        if($newStatus == 'sync'){
            $listPermission[] = $permission_id;
            $listRole[] = $role_id;
        }else{
            foreach($listPermission as $key => $val){
                if($val == $permission_id){
                    unset($listPermission[$key]);
                }
            }
            foreach($listRole as $key => $val){
                if($val == $role_id){
                    unset($listRole[$key]);
                }
            }
        }
        $listPermission = array_unique($listPermission);
        $listRole = array_unique($listRole);
        $role->permission_ids = $listPermission;
        $role->save();
        $permission->role_ids = $listRole;
        $permission->save();
        self::refresh($request, 1);

        return response()->json([]);
    }
}