<?php

namespace App\Modules\Backend\Controllers;

use App\Admin;
use App\Helpers\Common;
use App\Helpers\Helpers;
use App\Artist;
use App\Http\Controllers\Controller;
use App\Setting;
use Carbon\Carbon;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ArtistController extends Controller
{
    protected $artist;

    public function __construct(Artist $artist)
    {
        $this->artist = $artist;
    }

    /**
     * danh sách 
     */
    public function index(Request $request)
    {
        return view('Backend::artist.index');
    }

    public function create()
    {
        return view('Backend::artist.create');
    }

    public function store(Request $artistRequest)
    {
        $this->validate($artistRequest, [
            'name' => 'required',
        ]);
        $input = $artistRequest->all();
        $this->artist->create($input);
        return redirect()->route('backend.artist.list')
            ->with('flash_message',
                $artistRequest->name . ' thêm mới thành công.');
    }

    /**
     * View request
     */
    public function edit($id)
    {
        $data = $this->artist->getData($id);
        if (!$data) {
            return redirect()->back();
        }

        return view('Backend::artist.edit', compact(['data']));
    }

    public function update(Request $artistRequest, $id)
    {
        $data = $this->artist->getData($id);
        $input = $artistRequest->all();
        $this->validate($artistRequest, [
            'name' => 'required',
        ]);
        $data->fill($input)->update();
        return redirect()->route('backend.artist.list')
            ->with('flash_message',
                $artistRequest->name . ' thay đổi thành công');
    }

    public function destroy($id)
    {
        $data = $this->artist->getData($id);
        $data->status = "-1";
        $data->save();
        return redirect()->route('backend.artist.list')
            ->with('flash_message', $data->name .
                ' xóa thành công.');
    }

    /**
     * Json data table request
     */
    public function dataTable(Request $request)
    {
        //store cache
        $input = [
            'search' => $request->get('search', []),
            'search_name_id' => $request->get('search_name_id', ''),
            'status'=> $request->get('status',''),
            'length' => $request->get('length', 1),
            'start' => $request->get('start', 0),
            'draw' => $request->get('draw', 1),
        ];

        $query = $this->artist->orderBy('created_at', "DESC");
        if (!empty($input['search']['value'])) {
            $query->where('name', 'like', "%" . $input['search']['value'] . "%");
            $query->orWhere('_id', $input['search']['value']);
        };
        if (!empty($input['search_name_id'])) {
            $query->where('name', 'like', "%" . $input['search_name_id'] . "%");
            $query->orWhere('_id', $input['search_name_id']);
        };

        if ($input['status'] != "") {
            $query->where('status', (string)$input['status']);
        }else{
            $query->where('status', '!=', "-1");
        };

        $countTotal = $query->count();

        if (!empty($input['length'])) {
            $query->limit((int)$input['length']);
        }
        if (!empty($input['start'])) {
            $query->skip((int)$input['start']);
        }

        $data = $query->with(
            [
                'createdBy' => function ($q) {
                    $q->select('name');
                },
            ]
        )->get();

        $json = [
            "recordsTotal" => $data->count(),
            "recordsFiltered" => $countTotal,
            'data' => []
        ];
		if(!empty($data)){
			foreach ($data as $item => $value) {
				$urlEdit = route('backend.artist.edit', $value->_id);
                $urlDelete = route('backend.artist.delete', $value->_id);
				$json['data'][$item]['name'] = '<a href="' . $urlEdit . '">' . $value->name . '</a>';
                $json['data'][$item]['thumbnail_url'] =!empty($value->thumbnail_url)? '<img class="img-responsive" style="width: 100px;" src="' .$value->thumbnail_url. '" alt="">' : '';
				$json['data'][$item]['created_time'] = '<p>' . $value->created_at . '</p>';
				$json['data'][$item]['status'] = @Helpers::statusView($value->status, false);
				$json['data'][$item]['action'] = '
				                <a href="' . $urlEdit . '">
                                    <button type="button" class="btn btn-xs btn-primary" title="Chỉnh sửa"><i class="fa fa-edit"></i></button>
                                </a>
                                <a href="' . $urlDelete . '">
                                    <button type="button" class="btn btn-xs btn-danger" title="Xóa"><i class="fa fa-trash"></i></button>
                                </a>';

			}
        }
		return response()->json($json);
    }

}
