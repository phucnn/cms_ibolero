<?php
/**
 * Created by PhpStorm.
 * User: Mr-ha
 * Date: 12/10/2017
 * Time: 11:12 PM
 */
$namespace = 'App\Modules\Backend\Controllers';

Route::group(
    ['middleware' => ['web'], 'module' => 'Backend', 'namespace' => $namespace],
    function () {
        Route::get('/login', ['as' => 'backend.login', 'uses' => "AdminLoginController@showLoginForm"]);
        Route::post('/login', ['as' => 'backend.login.post', 'uses' => "AdminLoginController@login"]);

        Route::get('/api_agent', ['as' => 'api.agent.info', 'uses' => "ApiController@listAgent"]);
        Route::group(['middleware' => ["auth:admin"]], function () {
            Route::match(['get', 'post'], '/', ['as' => 'backend.dashboard', 'uses' => 'DashboardController@index']);
            Route::post('/upload-record', ['as' => 'backend.dashboard.upload', 'uses' => 'DashboardController@testUploadRecord']);
            Route::get('/logout', ['as' => 'backend.admins.logout', 'uses' => "AdminController@getLogout"]);
            Route::post('/change-status', ['as' => 'backend.dashboard.changeStatus', 'uses' => 'DashboardController@changeStatus']);
            Route::get('/refresh', ['as' => 'backend.roles.refresh', 'uses' => "RoleController@refresh"]);


            Route::group(['prefix' => 'admins', 'middleware' => "permission:admins-manager"], function () {
                Route::get('/', ['as' => 'backend.admins.list', 'uses' => "AdminController@index"]);
                Route::get('/add', ['as' => 'backend.admins.add', 'uses' => "AdminController@create"]);
                Route::post('/add', ['as' => 'backend.admins.add.post', 'uses' => "AdminController@store"]);
                Route::get('/edit/{id}', ['as' => 'backend.admins.edit', 'uses' => "AdminController@edit"]);
                Route::post('/edit/{id}', ['as' => 'backend.admins.edit.post', 'uses' => "AdminController@update"]);
                Route::get('/delete/{id}', ['as' => 'backend.admins.delete', 'uses' => "AdminController@destroy"]);
            });

            Route::group(['prefix' => 'roles', 'middleware' => "permission:roles-manager"], function () {
                Route::get('/', ['as' => 'backend.roles.list', 'uses' => "RoleController@index"]);
                Route::get('/add', ['as' => 'backend.roles.add', 'uses' => "RoleController@create"])->middleware('permission:roles-edit');
                Route::post('/add', ['as' => 'backend.roles.add.post', 'uses' => "RoleController@store"]);
                Route::get('/edit/{id}', ['as' => 'backend.roles.edit', 'uses' => "RoleController@edit"])->middleware('permission:roles-edit');
                Route::post('/edit/{id}', ['as' => 'backend.roles.edit.post', 'uses' => "RoleController@update"]);
                Route::get('/delete/{id}', ['as' => 'backend.roles.delete', 'uses' => "RoleController@destroy"])->middleware('permission:roles-edit');
                Route::post('/change-status', ['as' => 'backend.roles.changeStatus', 'uses' => 'RoleController@changeStatus']);
            });

            Route::group(['prefix' => 'permissions', 'middleware' => "permission:permissions-manager"], function () {
                Route::get('/', ['as' => 'backend.permissions.list', 'uses' => "PermissionController@index"]);
                Route::get('/add', ['as' => 'backend.permissions.add', 'uses' => "PermissionController@create"])->middleware('permission:permissions-edit');
                Route::post('/add', ['as' => 'backend.permissions.add.post', 'uses' => "PermissionController@store"])->middleware('permission:permissions-edit');
                Route::get('/edit/{id}', ['as' => 'backend.permissions.edit', 'uses' => "PermissionController@edit"])->middleware('permission:permissions-edit');
                Route::post('/edit/{id}', ['as' => 'backend.permissions.edit.post', 'uses' => "PermissionController@update"])->middleware('permission:permissions-edit');
                Route::get('/delete/{id}', ['as' => 'backend.permissions.delete', 'uses' => "PermissionController@destroy"])->middleware('permission:permissions-edit');
            });
            
            Route::group(['prefix' => 'composer', 'middleware' => "permission:composers-manager"], function () {
                Route::match(['get', 'post'],'/', ['as' => 'backend.composer.list', 'uses' => "ComposerController@index"]);
                Route::get('/add', ['as' => 'backend.composer.add', 'uses' => "ComposerController@create"]);
                Route::post('/add', ['as' => 'backend.composer.add.post', 'uses' => "ComposerController@store"]);
                Route::get('/edit/{id}', ['as' => 'backend.composer.edit', 'uses' => "ComposerController@edit"]);
                Route::post('/edit/{id}', ['as' => 'backend.composer.edit.post', 'uses' => "ComposerController@update"]);
                Route::get('/delete/{id}', ['as' => 'backend.composer.delete', 'uses' => "ComposerController@destroy"]);
                Route::post('/dataTable', ['as' => 'backend.composer.dataTable', 'uses' => 'ComposerController@dataTable']);
            });

            Route::group(['prefix' => 'artist', 'middleware' => "permission:artists-manager"], function () {
                Route::match(['get', 'post'],'/', ['as' => 'backend.artist.list', 'uses' => "ArtistController@index"]);
                Route::get('/add', ['as' => 'backend.artist.add', 'uses' => "ArtistController@create"]);
                Route::post('/add', ['as' => 'backend.artist.add.post', 'uses' => "ArtistController@store"]);
                Route::get('/edit/{id}', ['as' => 'backend.artist.edit', 'uses' => "ArtistController@edit"]);
                Route::post('/edit/{id}', ['as' => 'backend.artist.edit.post', 'uses' => "ArtistController@update"]);
                Route::get('/delete/{id}', ['as' => 'backend.artist.delete', 'uses' => "ArtistController@destroy"]);
                Route::post('/dataTable', ['as' => 'backend.artist.dataTable', 'uses' => 'ArtistController@dataTable']);
            });

            Route::group(['prefix' => 'song', 'middleware' => "permission:songs-manager"], function () {
                Route::match(['get', 'post'],'/', ['as' => 'backend.song.list', 'uses' => "SongController@index"]);
                Route::get('/add', ['as' => 'backend.song.add', 'uses' => "SongController@create"]);
                Route::post('/add', ['as' => 'backend.song.add.post', 'uses' => "SongController@store"]);
                Route::get('/edit/{id}', ['as' => 'backend.song.edit', 'uses' => "SongController@edit"]);
                Route::post('/edit/{id}', ['as' => 'backend.song.edit.post', 'uses' => "SongController@update"]);
                Route::get('/delete/{id}', ['as' => 'backend.song.delete', 'uses' => "SongController@destroy"]);
                Route::post('/dataTable', ['as' => 'backend.song.dataTable', 'uses' => 'SongController@dataTable']);
                Route::post('/import', ['as' => 'backend.song.import', 'uses' => 'ExcelController@import']);
                Route::get('/download', ['as' => 'backend.song.download', 'uses' => "ExcelController@download"]);
            });

            Route::group(['prefix' => 'karaoke', 'middleware' => "permission:karaokes-manager"], function () {
                Route::match(['get', 'post'],'/', ['as' => 'backend.karaoke.list', 'uses' => "KaraokeController@index"]);
                Route::get('/add', ['as' => 'backend.karaoke.add', 'uses' => "KaraokeController@create"]);
                Route::post('/add', ['as' => 'backend.karaoke.add.post', 'uses' => "KaraokeController@store"]);
                Route::get('/edit/{id}', ['as' => 'backend.karaoke.edit', 'uses' => "KaraokeController@edit"]);
                Route::post('/edit/{id}', ['as' => 'backend.karaoke.edit.post', 'uses' => "KaraokeController@update"]);
                Route::get('/delete/{id}', ['as' => 'backend.karaoke.delete', 'uses' => "KaraokeController@destroy"]);
                Route::post('/dataTable', ['as' => 'backend.karaoke.dataTable', 'uses' => 'KaraokeController@dataTable']);
                Route::post('/import', ['as' => 'backend.karaoke.import', 'uses' => 'ExcelController@import']);
                Route::get('/download', ['as' => 'backend.karaoke.download', 'uses' => "ExcelController@download"]);
            });


            Route::group(['prefix' => 'user', 'middleware' => "permission:users-manager"], function () {
                Route::match(['get', 'post'],'/', ['as' => 'backend.user.list', 'uses' => "UserController@index"]);
                Route::get('/add', ['as' => 'backend.user.add', 'uses' => "UserController@create"]);
                Route::post('/add', ['as' => 'backend.user.add.post', 'uses' => "UserController@store"]);
                Route::get('/edit/{id}', ['as' => 'backend.user.edit', 'uses' => "UserController@edit"]);
                Route::post('/edit/{id}', ['as' => 'backend.user.edit.post', 'uses' => "UserController@update"]);
                Route::get('/delete/{id}', ['as' => 'backend.user.delete', 'uses' => "UserController@destroy"]);
                Route::post('/dataTable', ['as' => 'backend.user.dataTable', 'uses' => 'UserController@dataTable']);
            });

            Route::group(['prefix' => 'setting', 'middleware' => "permission:setting-manager"], function () {
                Route::get('/', ['as' => 'backend.setting.index', 'uses' => "SettingController@index"]);
                Route::match(['get', 'post'], '/page', ['as' => 'backend.setting.page', 'uses' => "SettingController@page"]);
                Route::match(['get', 'post'], '/package', ['as' => 'backend.setting.package', 'uses' => "SettingController@package"]);
                Route::match(['get', 'post'], '/category', ['as' => 'backend.setting.category', 'uses' => "SettingController@category"]);
                Route::match(['get', 'post'], '/banner', ['as' => 'backend.setting.banner', 'uses' => "SettingController@banner"]);
            });

            Route::group(['prefix' => 'ticket'], function () {
                Route::match(['get', 'post'], '/list', ['as' => 'backend.ticket.list', 'uses' => "TicketController@index"]);
                Route::match(['get', 'post'], '/send', ['as' => 'backend.ticket.send', 'uses' => "TicketController@send"]);
                Route::post('/dataTableAjax/ticket', ['as' => 'backend.dataTable.ticket', 'uses' => 'TicketController@dataTable']);
                Route::get('/edit/{id}', ['as' => 'backend.ticket.edit', 'uses' => "TicketController@edit"]);
                Route::post('/reply/{id}', ['as' => 'backend.ticket.reply', 'uses' => "TicketController@reply"]);
            });
            
        });


    }
);
