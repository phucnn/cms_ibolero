<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;

?>
@extends('Backend::layouts.app')

@section('title', '| Create Permission')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="clearfix">
                        <h3 class="box-title pull-left">{{$data->name}}</h3>
                    </div>
                </div>
                <div class="box-body">
                    @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <form action="{{route('backend.composer.edit.post',$data->id)}}" method="POST" class="">
            <div class="col-md-8">
                <div class="box box-primary box-solid ">
                    <div class="box-header">
                        <h3 class="box-title">Thông tin chung</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">{{__("messages.name")}}</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old("name",$data->name)}}" placeholder="Name">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description">{{__("messages.content")}} </label>
                            <textarea name="description" id="description" class="form-control tinymce-editor">{!! old('description',$data->description) !!}</textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                            @endif
                        </div>


                    </div>
                    <!-- /.box-body -->

                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Uploads</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('thumbnail_url') ? ' has-error' : '' }}">
                            <label for="uploadAvatar"><i class='fa fa-img'></i> {{__("messages.avatar")}}</label>
                            <div class="input-group">
                                         <span class="input-group-btn">
                             <a id="uploadAvatar" data-input="thumbnail-url" data-preview="thumbnail-preview" class="btn btn-primary">
                               <i class="fa fa-picture-o"></i> {{__("messages.uploads")}}</a>
                           </span>
                                <input id="thumbnail-url" class="form-control" value="{{old('thumbnail_url',$data->thumbnail_url)}}" type="text" name="thumbnail_url">
                            </div>
                            <img id="thumbnail-preview" src="{{@$data->thumbnail_url}}" style="margin-top:15px;max-height:100px;"/>
                            @if ($errors->has('thumbnail_url'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('thumbnail_url') }}</strong>
                                    </span>
                            @endif
                        </div>

                    </div>
                </div>

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Publish</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('priority') ? ' has-error' : '' }}">
                            <label for="name">{{__("messages.priority")}}</label>
                            <input type="number" class="form-control" id="priority" name="priority" value="{{old("priority",!empty($data->priority)?$data->priority:0)}}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('priority') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status">{{__("messages.status")}}</label>
                            {!! Helpers::statusSelect(old('status',$data->status)) !!}
                            @if ($errors->has('status'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{__("messages.submit")}}</button>
                    </div>
                </div>
            </div>
        </form>

        <!-- /.col -->
    </div>
@endsection
@section('script')
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script>
        $('#uploadthumbnail_url').filemanager('image');
        $('#uploadVideo').filemanager('file');
        $('#singer').select2({
            multiple: true,
        })
    </script>
@endsection