<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;
use Illuminate\Support\Facades\Input;

?>
@extends('Backend::layouts.app')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="clearfix">
                        <h3 class="box-title pull-left"><i class="fa fa-card-circle-o"></i> Danh sách karaoke </h3>
                        <div class="pull-right">
                            <a href="{{ route('backend.karaoke.add') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                    @endif

                </div>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="box box-primary box-solid ">
                <div class="box-header">
                    <h3 class="box-title">Import file excel</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sx-12" style="padding : 0 10px">
                            @if(Session::has('import_success'))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i>Import thành công : {{Session::get('count_import_success')}} câu</h4>
                                </div>
                            @endif
                            @if(!empty(Session::has('import_fail')))
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> Import thất bại {{Session::get('count_import_fail')}} câu</h4>
                                    <div>
                                        @if(Session::get('import_fail') > 0)
                                            @foreach(Session::get('import_fail') as $oneFail)
                                                <p>{!! $oneFail !!}</p>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-sx-12" style="padding : 10px">
                            <form action="{{route('backend.karaoke.import')}}" method="post" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <input class="" type="file" name="import_file"/>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> {{__("messages.uploads")}}</button>
                                    <button type="button" class="btn btn-success" onclick="window.location.href='{{route('backend.karaoke.download')}}'"><i class="fa fa-download"></i> Download file mẫu</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-8">
            <div class="box box-primary box-solid">
                <div class="box-header">
                    <h3 class="box-title">Tìm kiếm</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <form action="{{route('backend.karaoke.list')}}" method="get" class="form-horizontal" id="search-form-data">
                    <div class="box-body">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Tên ca khúc</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="search_name_id" id="search_name_id" value="{{@$input['search_name_id']}}" class="form-control" placeholder="Tên ca khúc">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Trạng thái</label>
                                    <div class="col-sm-9">
                                        {{\App\Helpers\Helpers::statusSelect('', 1)}}
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-primary pull-right" value="" id="update-new-data"><i class="fa fa-search"></i> {{__("messages.search")}}</button>
                        <a href="{{route('backend.karaoke.list')}}" class="btn btn-warning "><i class="fa fa-refresh"></i> Reset</a>
                    </div>
                </form>
            </div>
        </div>



        <div class="col-xs-12">
            <form action="{{route('backend.karaoke.list')}}" method="post" id="tableList">
                {!! csrf_field() !!}
                <input type="hidden" name="typeChange" id="typeChange" value="">
                <div class="box box-primary box-solid ">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>

                    </div>
                    <div class="box-body">
                        <div>
                            <button type="button" class="btn btn-primary" value="publish" onclick="confirmChange('publish')"><i class="fa  fa-long-arrow-up"></i> Publish</button>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn btn-dark" value="hidden" onclick="confirmChange('hidden')"><i class="fa  fa-long-arrow-down"></i> Hidden</button>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn btn-danger" value="delete" onclick="confirmChange('delete')"><i class="fa fa-trash"></i> Delete</button>
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-hover table-striped vertical-middle">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" onchange="checkAll(this)"></th>
                                    <th>Tên</th>
                                    <th>Ảnh</th>
                                    <th>Video</th>
                                    <th>Chuyên mục</th>
                                    <th>Thời gian tạo</th>
                                    <th width="5%">Trạng thái</th>
                                    <th width="5%">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </form>
        </div>
        <!-- /.col -->
    </div>
@endsection
@section("script")
    <script>
        $(function () {
            var table = $("#example2").DataTable({
                processing: true,
                serverSide: true,
                "pageLength": 25,
                "lengthMenu": [10, 25, 50],
                "columnDefs": [
                    {"orderable": false, "targets": 0}
                ],
                "ajax":
                    {
                        "url": "{{route('backend.karaoke.dataTable')}}",
                        "type": "POST"
                    },
                "columns": [
                    { "data": "checkbox" },
                    { "data": "name" },
                    { "data": "thumbnail_url" },
                    { "data": "link_video" },
                    { "data": "category" },
                    { "data": "created_time" },
                    { "data": "status" },
                    { "data": "action" },
                ],
                "order" : [[1, "desc"]]
            });
        });

        $('#update-new-data').on('click', function () {
            var data = {};
            $.map($("#search-form-data [id]"), function(n, i) {
                data[n.id] = $('#'+n.id).val();
            });
            $("#example2").DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                "pageLength": 25,
                "lengthMenu": [10, 25, 50],
                "columnDefs": [
                    {"orderable": false, "targets": 0}
                ],
                "ajax":
                    {
                        "url": "{{route('backend.karaoke.dataTable')}}",
                        "type": "POST",
                        "data" : data
                    },
                "columns": [
                    { "data": "checkbox" },
                    { "data": "name" },
                    { "data": "thumbnail_url" },
                    { "data": "link_video" },
                    { "data": "category" },
                    { "data": "created_time" },
                    { "data": "status" },
                    { "data": "action" },
                ],
                "order" : [[1, "desc"]]
            });
        });

        function confirmStatus(box, id) {
            var txt = 'Are you sure you want to hidden ?';
            var oldStatus = '1';
            var newStatus = '0';
            var newChange = false;
            var oldChange = true;
            if (box.checked) {
                txt = "Are you sure you want to publish ?";
                oldStatus = '0';
                newStatus = '1';
                newChange = true;
                oldChange = false;
            }

            let r = confirm(txt);
            if (r === true) {
                $.ajax({
                    type: "POST",
                    url: "{{route('backend.dashboard.changeStatus')}}",
                    data: {
                        type: 'karaoke',
                        newStatus: newStatus,
                        id: id.toString()
                    }
                }).done(function (data) {
                    box.checked = newChange;
                });
            } else {
                box.checked = oldChange;
            }
        }

        function checkAll(box) {
            if (box.checked) {
                $('.checkID').prop('checked', true);
                return;
            }
            $('.checkID').prop('checked', false);

        }

        function confirmChange(confirmChange) {
            var txt = 'Are you sure you want to ' + confirmChange + ' selected record ?';
            $('#typeChange').val(confirmChange);
            let r = confirm(txt);
            if (r === true) {
                $('#tableList').submit();
            }
        }


    </script>
@endsection

