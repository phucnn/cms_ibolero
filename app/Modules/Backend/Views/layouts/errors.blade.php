<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    {{--meta--}}
    <meta property="og:title" content="@yield('title') | {{config('site.title')}} "/>
    <meta property="og:description" content="@yield('description') {{config('site.description')}}"/>
    <meta property="og:image" content="{{url('/')}}/img/logo.png"/>
    <meta property="og:image:url" content="{{url('/')}}/img/logo.png"/>
    <meta property="og:url" content="{{url('/')}}"/>
    <meta property="og:site_name" content="Nhạc online"/>
    <meta property="og:type" content="article"/>
    <meta content='1825889694150415' property='fb:app_id'/>
    <!-- CSS -->
    <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css/fontawesome.all.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css/animate.css">
    <link rel="stylesheet" href="{{url('/')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css/otta-large-desktops.css">
    <link rel="stylesheet" media="screen and (max-width: 1199.98px)" href="{{url('/')}}/css/otta-desktops.css">
    <link rel="stylesheet" media="screen and (max-width: 991.98px)" href="{{url('/')}}/css/otta-tablets.css">
    <link rel="stylesheet" media="screen and (max-width: 767.98px)" href="{{url('/')}}/css/otta-landscape-phones.css">
    <link rel="stylesheet" media="screen and (max-width: 575.98px)" href="{{url('/')}}/css/otta-portrait-phones.css">
</head>
<body>
@yield('content')
</body>
</html>

