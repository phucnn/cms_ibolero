<?php
/**
 * Created by PhpStorm.
 * User: Mr-ha
 * Date: 12/12/2017
 * Time: 12:20 AM
 */
?>

</div>
<!-- /.content-wrapper -->



<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>&copy; <a href="mailto:nnp1.happy@gmail.com">PhucNN</a> 2019 </strong>
</footer>

<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<div class="modal fade" id="sendTicketModal" data-backdrop="" style="padding-top:150px;">
    <div class="modal-dialog modal-lg" style="border: 1px solid rgba(255, 0, 0, 0.5)">
        <div class="modal-content">
            <form action="{{route('backend.ticket.send')}}" method="post">
                {!! csrf_field() !!}
                <input type="hidden" name="type" id="type" value="khac">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Báo lỗi</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" required class="form-control" name="subject" placeholder="Tiêu đề">
                    </div>
                    <textarea name="content" class="textarea" required
                              style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px;"
                              placeholder="Nội dung"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Send <i class="fa fa-arrow-circle-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- /AdminLTE App -->
<script src="/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- SlimScroll -->
<script src="/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- DataTables -->
<script src="/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="/AdminLTE/plugins/iCheck/icheck.js"></script>
<!-- ChartJs -->
<script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js"></script>
<!-- date-range-picker -->
<script src="/AdminLTE/bower_components/moment/min/moment.min.js"></script>
<script src="/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js"></script>

{{--<script src="/js/tinymce/jquery.tinymce.min.js"></script>--}}
<script src="/js/tinymce/tinymce.min.js"></script>

<script src="/vendor/laravel-filemanager/js/lfm.js"></script>

<script>
    $(function () {

        var editor_config = {
            path_absolute: "/",
            selector: "textarea.tinymce-editor",
            content_css : '/css/admincp.css, /css/fontawesome-all.min.css, /css/bootstrap.min.css,' +
                '/css/animate.css,' +
                '/css/owl.carousel.min.css,' +
                '/css/owl.theme.default.min.css,' +
                '/css/otta-large-desktops.css',
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern",
                'iconspeaker importcss'
            ],

            toolbar: "insertfile undo redo | styleselect | bold italic  forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | preview | iconspeaker | template",
            templates :"/cms/assets/template/lesson_word.php",
            template_replace_values: {
                id: Number(new Date())
            },
            relative_urls: false,
            height: "200",
            valid_elements: "*[*]",
            invalid_elements: "strong,b,em",
            file_browser_callback: function (field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                console.log(cmsURL);
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file: cmsURL,
                    title: 'Filemanager',
                    width: x * 0.8,
                    height: y * 0.8,
                    resizable: "yes",
                    close_previous: "no"
                });
            },
        };
        var editor_config_mini = {
            path_absolute: "/",
            selector: "textarea.tinymce-editor-mini",
            toolbar: "bold italic underline  forecolor backcolor | alignleft aligncenter alignright alignjustify",
            relative_urls: false,
            height: "100",
            valid_elements: "*[*]",
            invalid_elements: "strong,b,em",
            content_css: ['/css/admincp.css', '/css/fontawesome-all.min.css'],
        };

        if($('textarea.tinymce-editor').length){
            tinymce.init(editor_config);
        }

        if($('textarea.tinymce-editor-mini').length){
            tinymce.init(editor_config_mini);
        }

        $('.select2').select2()
    });

    function initIcheck() {
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })
    }
    initIcheck();
</script>
@yield('sub_script')
@yield('script')

</body>
</html>
