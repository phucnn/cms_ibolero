<?php
use Illuminate\Support\Facades\Auth;
$user = Auth::user();
?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title') | {{$globalData['meta']['title']}} </title>
    {{--meta--}}
    <meta property="og:title" content="@yield('title') | {{config('site.title')}} "/>
    <meta property="og:description" content="@yield('description') {{config('site.description')}}"/>
    <meta property="og:image" content="{{url('/')}}/img/logo.png"/>
    <meta property="og:image:url" content="{{url('/')}}/img/logo.png"/>
    <meta property="og:url" content="{{url('/')}}"/>
    <meta property="og:site_name" content="Nhạc online"/>
    <meta property="og:type" content="article"/>
    <meta content='1825889694150415' property='fb:app_id'/>

    <!-- CSS -->
    <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css/fontawesome.all.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css/animate.css">
    <link rel="stylesheet" href="{{url('/')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{url('/')}}/css/otta-large-desktops.css">
    <link rel="stylesheet" media="screen and (max-width: 1199.98px)" href="{{url('/')}}/css/otta-desktops.css">
    <link rel="stylesheet" media="screen and (max-width: 991.98px)" href="{{url('/')}}/css/otta-tablets.css">
    <link rel="stylesheet" media="screen and (max-width: 767.98px)" href="{{url('/')}}/css/otta-landscape-phones.css">
    <link rel="stylesheet" media="screen and (max-width: 575.98px)" href="{{url('/')}}/css/otta-portrait-phones.css">
    <link rel="stylesheet" href="{{url('/')}}/css/hatt.css">
</head>
<body>

<!-- Right-iconNav -->
<div class="nav-icon-right d-flex align-items-center position-fixed">
    <ul class="nav flex-column text-right">
        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="left" title="Từ điển">
                <i class="fal fa-book-reader"></i>
            </a>
        </li>
        <li class="nav-item" data-toggle="modal" data-target="#searchModal">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="left" title="Tìm kiếm">
                <i class="fal fa-search"></i>
            </a>
        </li>
        <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="left" title="Nhật ký của tôi">
                <i class="fal fa-swatchbook"></i>
            </a>
        </li>
        <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="left" title="Phòng thi thử">
                <i class="fal fa-users-class"></i>
            </a>
        </li>
        <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="left" title="Thành viên">
                <i class="fal fa-users"></i>
            </a>
        </li>
        <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="left" title="Bài hát tiếng Anh">
                <i class="fal fa-music"></i>
            </a>
        </li>
        <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="left" title="Truyện tiếng Anh">
                <i class="fal fa-atlas"></i>
            </a>
        </li>
        <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="left" title="Danh ngôn tiếng Anh">
                <i class="fal fa-theater-masks"></i>
            </a>
        </li>
        <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="left" title="Games tiếng Anh">
                <i class="fal fa-gamepad"></i>
            </a>
        </li>
        <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="#" data-toggle="tooltip" data-placement="left" title="Tiếng Anh hàng ngày">
                <i class="fal fa-window-restore"></i>
            </a>
        </li>
    </ul>
</div>
<!-- Nav Index-->
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <a class="navbar-brand d-block d-md-none" href="{{url('/')}}">
            <img class="logo" src="{{@$globalData['information']['logo']}}" alt="">
        </a>
        <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/')}}">Trang Chủ <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Học Phí</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Bài giảng</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Luyện tập</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Đề thi thử</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Giải trí
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Bài hát tiếng Anh</a>
                        <a class="dropdown-item" href="#">Truyện tiếng Anh</a>
                        <a class="dropdown-item" href="#">Danh Ngôn tiếng Anh</a>
                        <a class="dropdown-item" href="#">Tiếng Anh hàng ngày</a>
                        <a class="dropdown-item" href="#">Game tiếng Anh</a>
                    </div>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="#">Phòng Thi Thử</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Thành Viên</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Liên Hệ</a>
                </li>
            </ul>

                <ul class="nav nav-user pr-0">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" data-toggle="modal" data-target="#loginModal"><i class="fas fa-user-graduate mr-2"></i>Đăng Nhập</a>
                    </li>
                    <li class="nav-item d-none d-lg-block">
                        <a class="nav-link active" href="#"><i class="fas fa-user-edit mr-2"></i>Đăng Ký</a>
                    </li>
                </ul>
        </div>
    </div>
</nav>
<!-- header -->
<header class="d-none d-lg-block">
    <div class="container">
        <div class="row d-flex">
            <div class="col-3">
                <a class="d-inline-block" href="{{url('/')}}"><img class="p-2 pt-md-2 logo" src="{{@$globalData['information']['logo']}}" alt="Ôn Thi Tiếng Anh"></a>
            </div>
            <div class="col">
                <ul class="nav float-right">
                    @if(!empty($globalData['information']['hotline']))
                        <li class="media py-2 px-3 border-right">
                            <i class="fal fa-mobile-alt fa-2x mt-2 mr-3 icon-blue"></i>
                            <div class="media-body text-muted">
                                Tổng đài hỗ trợ
                                <h5 class="mt-0 mb-1 font-weight-bold text-blue">{{$globalData['information']['hotline']}}</h5>
                            </div>
                        </li>
                    @endif
                    @if(!empty($globalData['information']['email']))
                        <li class="media py-2 px-3">
                            <i class="fal fa-envelope-open fa-2x mt-2 mr-3 icon-blue"></i>
                            <div class="media-body text-muted">
                                Mail liên hệ
                                <h6 class="mt-0 mb-1 font-weight-bold text-blue">{{$globalData['information']['email']}}</h6>
                            </div>
                        </li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
</header>
    @yield('content')
<!-- Footer -->
<footer class="footer">
    <div class="container ct-footer">
        <div class="row">
            <div class="col-4 pl-0 py-4 left-footer">
                <a href="{{url('/')}}"><img class="logo-footer mb-2" src="{{@$globalData['information']['logo']}}" alt=""></a>
                @if(!empty($globalData['information']['description']))
                    <p class="mb-2 d-none d-lg-block">
                        <small>
                            {{$globalData['information']['description']}}
                        </small>
                    </p>
                @endif
                @if(!empty($globalData['information']['email']))

                    <p class="mb-2">
                        <i class="fab fa-internet-explorer mr-2"></i>{{$globalData['information']['email']}}
                    </p>
                @endif
                @if(!empty($globalData['information']['email']))
                    <p>
                        <i class="fas fa-map-marker-alt mr-2"></i>
                        <small>Địa chỉ: {{$globalData['information']['address']}}</small>
                    </p>
                @endif

            </div>
            <div class="col-8 pr-0 py-4">
                <ul class="nav nav-footer mb-3 justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Giới thiệu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">điều khoản</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">chính sách bảo mật</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#subscribeModal">Nhận bản tin</a>
                    </li>
                </ul>
                @if(!empty($globalData['information']['copyright']))
                    <p class="text-right copyright">
                        <small class="text-muted">© {{$globalData['information']['copyright']}}</small>
                    </p>
                @endif
                @if(!empty($globalData['social']))
                    <ul class="nav justify-content-end">
                        @foreach($globalData['social'] as $social)
                            @if(!empty($social['status']))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{$social['url']}}"><i class="{{$social['icon_class']}}"></i></a>
                                </li>
                            @endif
                        @endforeach

                    </ul>
                @endif
            </div>
        </div>
    </div>
</footer>
<a class="to-top" data-toggle="tooltip" data-placement="left" title="Lên đầu trang"><i class="fal fa-arrow-to-top"></i></a>
<!-- Optional JavaScript -->
<script src="{{url('/')}}/js/jquery-3.3.1.min.js"></script>
<script src="{{url('/')}}/js/jquery-ui.js"></script>
<script src="{{url('/')}}/js/jquery.ui.touch-punch.min.js"></script>
<script src="{{url('/')}}/js/popper.min.js"></script>
<script src="{{url('/')}}/js/bootstrap.min.js"></script>
<script src="{{url('/')}}/js/jquery.toTop.js"></script>
<script src="{{url('/')}}/js/owl.carousel.min.js"></script>
<script src="{{url('/')}}/js/wow.min.js"></script>
<script src='{{url('/')}}/js/responsivevoice.js'></script>
@yield('script')
<script src="{{url('/')}}/js/hatt.js"></script>
<script src="{{url('/')}}/js/practice.js"></script>
</body>
</html>

