<?php
use Illuminate\Support\Facades\Auth;
$user = Auth::user();
?>
    @include('Backend::layouts.header')
    @yield('content')
    @include('Backend::layouts.footer')

