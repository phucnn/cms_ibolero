<ul class="sidebar-menu" data-widget="tree">
    @can('admins-manager')
    <li class="treeview">
        <a href="#">
            <i class="fa fa-users"></i>
            <span>Quản lý cms</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu" style="display: none;">
            <li>
                <a href="{{route("backend.admins.list")}}">
                    <i class="fa fa-user"></i> <span>List Thành Viên</span>
                </a>
            </li>
            <li>
                <a href="{{route("backend.roles.list")}}">
                    <i class="fa  fa-users"></i> <span>{{__("messages.roles")}}</span>
                </a>
            </li>
            <li>
                <a href="{{route("backend.permissions.list")}}">
                    <i class="fa fa-key"></i> <span>{{__("messages.permissions")}}</span>
                </a>
            </li>
        </ul>
    </li>

    <li><a href="{{route('backend.setting.index')}}"><i class="fa fa-gear "></i> <span>Cài đặt chung</span></a></li>
    <li><a href="{{route('backend.ticket.list')}}"><i class="fa fa-question "></i> <span>Báo lỗi</span></a></li>
    @endcan

    @can('composers-manager')
        <li><a href="{{route('backend.composer.list')}}"><i class="fa fa-pencil"></i> <span>Tác giả</span></a></li>
    @endcan
    @can('artists-manager')
        <li><a href="{{route('backend.artist.list')}}"><i class="fa fa-star"></i> <span>Ca sĩ</span></a></li>
    @endcan
    @can('songs-manager')
        <li><a href="{{route('backend.song.list')}}"><i class="fa fa-music"></i> <span>Ca khúc</span></a></li>
    @endcan
    @can('karaokes-manager')
        <li><a href="{{route('backend.karaoke.list')}}"><i class="fa fa-microphone"></i> <span>Karaoke</span></a></li>
    @endcan
    @can('users-manager')
        <li><a href="{{route('backend.user.list')}}"><i class="fa  fa-user"></i> <span>Người dùng</span></a></li>
    @endcan

</ul>