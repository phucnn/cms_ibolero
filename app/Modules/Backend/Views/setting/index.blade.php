<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;
use Illuminate\Support\Facades\Input;

?>
@extends('Backend::layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary box-solid">
                <div class="box-header">
                    <h3 class="box-title">Cài đặt</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <a href="{{route("backend.setting.page")}}" class="btn btn-app">
                        <i class="fa fa-info-circle"></i> Thông tin chung
                    </a>
                    <a href="{{route("backend.setting.package")}}" class="btn btn-app">
                        <i class="fa fa-info-circle"></i> Gói cước
                    </a>
                    <a href="{{route("backend.setting.category")}}" class="btn btn-app">
                        <i class="fa fa-info-circle"></i> Chuyên mục
                    </a>
                    <a href="{{route("backend.setting.banner")}}" class="btn btn-app">
                        <i class="fa fa-info-circle"></i> Banner
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection