<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;
use App\Song;

?>
@extends('Backend::layouts.app')


@section('content')
    <div class="row">

        <form action="{{route('backend.setting.package')}}" method="POST" class="">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><i class='fa fa-tags'></i> {{$name}}</h3>
                        @if(Session::has('flash_message'))
                            <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                        @endif
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="table-responsive">
                            <table class="table table-hover tabler-borded table_commission ">
                                <thead>
                                <tr>
                                    <th>Mã gói</th>
                                    <th>Tên gói</th>
                                    <th width="30%">Thông tin chi tiết</th>
                                    <th>Giá tiền</th>
                                    <th>Thời gian sử dụng</th>
                                    <th>Loại thời gian</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody class="list_question_content">
                                @if(!empty($data) or !empty(old('data')))
                                    <?php
                                    if (!empty(old('data'))) {
                                        $data = old('data');
                                    }
                                    $count = count($data['value']);
                                    ?>
                                    @foreach($data['value'] as $item=>$value)
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control" name="data[{{$item}}][code]" value="{{@$value['code']}}">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="data[{{$item}}][name]" value="{{@$value['name']}}">
                                            </td>
                                            <td>
                                                <textarea rows="3" class="form-control" name="data[{{$item}}][description]">{{@$value['description']}}</textarea>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="data[{{$item}}][price]" value="{{@$value['price']}}">
                                            </td>
                                            <td>
                                                <input type="number" min="1" class="form-control" name="data[{{$item}}][number_use]" value="{{@$value['number_use']}}">
                                            </td>
                                            <td>
                                                <select style="width: 100%;padding: 5px;" name="data[{{$item}}][type_use]">
                                                    <option value="day" {{@$value['type_use'] == "day" ? "selected" : ""}} >Ngày</option>
                                                    <option value="week" {{@$value['type_use'] == "week" ? "selected" : ""}}>Tuần</option>
                                                    <option value="month" {{@$value['type_use'] == "month" ? "selected" : ""}}>Tháng</option>
                                                </select>
                                            </td>
                                            <td>
                                                <span class="btn btn-primary "><i class="fa fa-arrows-v"></i></span>
                                                <span class="btn btn-danger  removeCommission"><i class="fa fa-trash"></i></span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" data-count="{{!empty($count)? $count : 0}}" onclick="addCommission($(this))" class="btn btn-primary"><i class="fa fa-plus"></i>
                            Thêm
                        </button>
                        <button type="submit" class="btn btn-primary pull-right"><i
                                    class="fa fa-save"></i> {{__("messages.submit")}}</button>
                    </div>
                </div>
            </div>

        </form>

        <!-- /.col -->
    </div>

@endsection
@section('script')
    <script>
        function addCommission(obj) {
            eq = obj.data('count') + 1;
            obj.data('count', (eq));
            var html = '<tr>\n' +
                '                            <td>\n' +
                '                                <input type="text" class="form-control" name="data[' + eq + '][code]">\n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                <input type="text" class="form-control" name="data[' + eq + '][name]">\n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                 <textarea rows="3" class="form-control" name="data[' + eq + '][description]"></textarea>\n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                <input type="text" class="form-control" name="data[' + eq + '][price]">\n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                <input type="number" class="form-control" name="data[' + eq + '][number_use]">\n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                <select style="width: 100%;padding: 5px;" name="data[' + eq + '][type_use]">\n' +
                '                                    <option value="day">Ngày</option>' +
                '                                   <option value="week">Tuần</option>' +
                '                                   <option value="month">Tháng</option>' +
                '                               </select>\n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                <span class="btn btn-primary "><i class="fa fa-arrows-v"></i></span>\n' +
                '                                <span class="btn btn-danger  removeCommission"><i class="fa fa-trash"></i></span>\n' +
                '                            </td>\n' +
                '                        </tr>';
            $('.table_commission tbody').append(html);
        }

        $(document).on('click', '.removeCommission', function () {
            var e = $(this);
            e.closest('tr').remove();
        })
    </script>
@endsection
