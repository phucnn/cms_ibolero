<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;
use App\Song;

?>
@extends('Backend::layouts.app')


@section('content')
    <div class="row">

        <form action="{{route('backend.setting.category')}}" method="POST" class="">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><i class='fa fa-tags'></i> {{$name}}</h3>
                        @if(Session::has('flash_message'))
                            <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                        @endif
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="table-responsive">
                            <table class="table table-hover tabler-borded table_commission ">
                                <thead>
                                <tr>
                                    <th>Tên chuyên mục</th>
                                    <th>Code chuyên mục</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody class="list_question_content">
                                @if(!empty($data) or !empty(old('data')))
                                    <?php
                                    if (!empty(old('data'))) {
                                        $data = old('data');
                                    }
                                    $count = count($data['value']);
                                    ?>
                                    @foreach($data['value'] as $item=>$value)
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control" name="data[{{$item}}][name]" value="{{@$value['name']}}">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="data[{{$item}}][code]" value="{{@$value['code']}}">
                                            </td>
                                            <td>
                                                <span class="btn btn-primary "><i class="fa fa-arrows-v"></i></span>
                                                <span class="btn btn-danger  removeCommission"><i class="fa fa-trash"></i></span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" data-count="{{!empty($count)? $count : 0}}" onclick="addCommission($(this))" class="btn btn-primary"><i class="fa fa-plus"></i>
                            Thêm
                        </button>
                        <button type="submit" class="btn btn-primary pull-right"><i
                                    class="fa fa-save"></i> {{__("messages.submit")}}</button>
                    </div>
                </div>
            </div>

        </form>

        <!-- /.col -->
    </div>

@endsection
@section('script')
    <script>
        function addCommission(obj) {
            eq = obj.data('count') + 1;
            obj.data('count', (eq));
            var html = '<tr>\n' +
                '                            <td>\n' +
                '                                <input type="text" class="form-control" name="data[' + eq + '][name]">\n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                <input type="text" class="form-control" name="data[' + eq + '][code]">\n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                <span class="btn btn-primary "><i class="fa fa-arrows-v"></i></span>\n' +
                '                                <span class="btn btn-danger  removeCommission"><i class="fa fa-trash"></i></span>\n' +
                '                            </td>\n' +
                '                        </tr>';
            $('.table_commission tbody').append(html);
        }

        $(document).on('click', '.removeCommission', function () {
            var e = $(this);
            e.closest('tr').remove();
        })
    </script>
@endsection
