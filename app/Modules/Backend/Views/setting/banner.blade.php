<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;
use App\Song;

?>
@extends('Backend::layouts.app')


@section('content')
    <div class="row">

        <form action="{{route('backend.setting.banner')}}" method="POST" class="">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><i class='fa fa-tags'></i> {{$name}}</h3>
                        @if(Session::has('flash_message'))
                            <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                        @endif
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="table-responsive">
                            <table class="table table-hover table-borded table_data ">
                                <thead>
                                <tr>
                                    <th>Link Banner</th>
                                    <th>Bài hát liên kết</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody class="list_question_content">
                                @if(!empty($data) or !empty(old('data')))
                                    <?php
                                    if (!empty(old('data'))) {
                                        $data = old('data');
                                    }
                                    $count = count($data['value']);
                                    ?>
                                    @foreach($data['value'] as $item=>$value)
                                        <tr>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a id="upload_{{$item}}" data-input="thumbnail-url-{{$item}}" data-preview="thumbnail-url-preview_{{$item}}" class="btn btn-primary uploadThumbnail_url">
                                                        <i class="fa fa-picture-o"></i> Upload</a>
                                                    </span>
                                                    <input id="thumbnail-url-{{$item}}" class="form-control" value="{{@$value['thumbnail_url']}}" type="text" name="data[{{$item}}][thumbnail_url]">
                                                </div>
                                                <img id="thumbnail-url-preview_{{$item}}" src="{{@$value['thumbnail_url']}}" style="margin-top:15px;max-height:500px;"/>
                                            </td>
                                            <td>
                                                <select class="js-example-basic-multiple" style="width: 100%" id="song_id_{{$item}}" name="data[{{$item}}][song_id]">
                                                    @foreach($listSong as $oneSong)
                                                        <option value="{{$oneSong->_id}}" @if($oneSong->_id == @$value['song_id']) selected @endif>{{$oneSong->name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <span class="btn btn-primary "><i class="fa fa-arrows-v"></i></span>
                                                <span class="btn btn-danger  removeLess"><i class="fa fa-trash"></i></span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" data-count="{{!empty($count)? $count : 0}}" onclick="addMore($(this))" class="btn btn-primary"><i class="fa fa-plus"></i>
                            Thêm
                        </button>
                        <button type="submit" class="btn btn-primary pull-right"><i
                                    class="fa fa-save"></i> {{__("messages.submit")}}</button>
                    </div>
                </div>
            </div>

        </form>

        <!-- /.col -->
    </div>

@endsection
@section('script')
    <script>
        $('.uploadThumbnail_url').filemanager('image');
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2({
            }).on("select2:select select2:unselect", function (e) {
                //this returns all the selected item

            })
        });

        function addMore(obj) {
            eq = obj.data('count') + 1;
            obj.data('count', (eq));
            var html = '<tr>\n' +
                '                            <td>\n' +
                '                                <div class="input-group">' +
                '                                   <span class="input-group-btn">' +
                '                                       <a id="upload_' + eq + '" data-input="thumbnail-url-' + eq + '" data-preview="thumbnail-url-preview_' + eq + '" class="btn btn-primary uploadThumbnail_url">' +
                '                                       <i class="fa fa-picture-o"></i> Upload</a>' +
                '                                   </span>' +
                '                                   <input id="thumbnail-url-' + eq + '" class="form-control" value="" type="text" name="data[' + eq + '][thumbnail_url]">' +
                '                               </div>' +
                '                               <img id="thumbnail-url-preview_' + eq + '" src="" style="margin-top:15px;max-height:500px;"/>' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                <select class="js-example-basic-multiple" style="width: 100%" id="song_id_' + eq + '" name="data[' + eq + '][song_id]">' +
                                                    @foreach($listSong as $value)
                                                        '<option value="{{$value->_id}}" >{{$value->name}}</option>' +
                                                    @endforeach
                '                                </select>\n' +
                '                            </td>\n' +
                '                            <td>\n' +
                '                                <span class="btn btn-primary "><i class="fa fa-arrows-v"></i></span>\n' +
                '                                <span class="btn btn-danger  removeCommission"><i class="fa fa-trash"></i></span>\n' +
                '                            </td>\n' +
                '                        </tr>';
            $('.table_data tbody').append(html);

            $('.uploadThumbnail_url').filemanager('image');
            $('.js-example-basic-multiple').select2({
            }).on("select2:select select2:unselect", function (e) {
                //this returns all the selected item

            })
        }

        $(document).on('click', '.removeLess', function () {
            var e = $(this);
            e.closest('tr').remove();
        });


    </script>
@endsection
