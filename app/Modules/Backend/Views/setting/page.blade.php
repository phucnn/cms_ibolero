<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;
use App\Song;

?>
@extends('Backend::layouts.app')


@section('content')
    <div class="row">

        <form action="{{route('backend.setting.page')}}" method="POST" class="">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><i class='fa fa-tags'></i> {{$name}}</h3>
                        @if(Session::has('flash_message'))
                            <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                        @endif
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="name">Title</label>
                                    <input type="text" class="form-control" id="title" name="title" value="{{old("title",@$data->value['title'])}}" placeholder="">
                                </div>

                                <div class="form-group">
                                    <label for="name">Script header</label>
                                    <textarea class="form-control" id="header" name="header">{{old("header",@$data->value['header'])}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="name">Script footer</label>
                                    <textarea class="form-control" id="footer" name="footer">{{old("footer",@$data->value['footer'])}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="uploadLogo"><i class='fa fa-img'></i> Logo</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <a id="uploadLogo" data-input="logo-url" data-preview="logo-preview" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> {{__("messages.uploads")}}</a>
                                        </span>
                                        <input id="logo-url" class="form-control" value="{{old("logo",@$data->value['logo'])}}" type="text" name="logo">
                                    </div>
                                    <img id="logo-preview" src="{{old("logo",@$data->value['logo'])}}" style="margin-top:15px;max-height:100px;"/>
                                </div>
                            </div>
                        </div>



                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right ">{{__("messages.submit")}}</button>
                    </div>
                </div>
            </div>

        </form>

        <!-- /.col -->
    </div>

@endsection

@section('script')
    <script>
        $('#uploadLogo').filemanager('image');
        $('#uploadBanner').filemanager('image');
    </script>
@endsection
