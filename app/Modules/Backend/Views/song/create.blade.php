<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;
?>
@extends('Backend::layouts.app')

@section('title', '| Create Permission')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="clearfix">
                        <h3 class="box-title pull-left">Thêm mới</h3>
                    </div>
                </div>
                <div class="box-body">
                    @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <form action="{{route('backend.song.add.post')}}" method="POST" class="">
            <div class="col-md-8">
                <div class="box box-primary box-solid ">
                    <div class="box-header">
                        <h3 class="box-title">Thông tin chung</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">{{__("messages.name")}}</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old("name")}}" placeholder="Name">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('hashtag') ? ' has-error' : '' }}">
                            <label for="name">Hashtag</label>
                            <input type="text" class="form-control" id="hashtag" name="hashtag" value="{{old("hashtag")}}" placeholder="Hashtag">
                            @if ($errors->has('hashtag'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('hashtag') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group" {{ $errors->has('composer_ids') ? ' has-error' : '' }}>
                            <label for="name">Sáng tác</label>
                            <select class="js-example-basic-multiple" style="width: 100%" id="composer_ids" name="composer_ids[]" multiple="multiple">
                                @foreach($listComposer as $value)
                                    <option value="{{$value->_id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('composer_ids'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('composer_ids') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group" {{ $errors->has('artist_ids') ? ' has-error' : '' }}>
                            <label for="name">Ca sĩ</label>
                            <select class="js-example-basic-multiple" style="width: 100%" id="artist_ids" name="artist_ids[]" multiple="multiple">
                                @foreach($listArtist as $value)
                                    <option value="{{$value->_id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('artist_ids'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('artist_ids') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group" {{ $errors->has('category') ? ' has-error' : '' }}>
                            <label for="name">Chuyên mục</label>
                            <select class="js-example-basic-multiple" style="width: 100%" id="category" name="category[]" multiple="multiple">
                                @foreach($listCategory->value as $value)
                                    <option value="{{$value['code']}}">{{$value['name']}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('category'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description">{{__("messages.content")}} </label>
                            <textarea name="description" id="description" class="form-control tinymce-editor">{!! old('description') !!}</textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                            @endif
                        </div>


                    </div>
                    <!-- /.box-body -->

                </div>
                <div class="box box-primary box-solid ">
                    <div class="box-header">
                        <h3 class="box-title">Thông tin phụ</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="has_download">Quyền Liên Quan &nbsp;&nbsp;&nbsp;</label>
                            <label class="switch">
                                <input type="checkbox"  name="qlq" id="qlq">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="form-group">
                            <label for="has_download">Quyền Tác Giả &nbsp;&nbsp;&nbsp;</label>
                            <label class="switch">
                                <input type="checkbox"  name="qtg" id="qtg">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="form-group">
                            <label for="has_download">Độc quyền &nbsp;&nbsp;&nbsp;</label>
                            <label class="switch">
                                <input type="checkbox"  name="dq" id="dq">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="form-group {{ $errors->has('lyrics') ? ' has-error' : '' }}">
                            <label for="description">Lyric </label>
                            <textarea name="lyric" id="lyrics" class="form-control tinymce-editor">{!! old('lyrics') !!}</textarea>
                            @if ($errors->has('lyrics'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('lyrics') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label class="control-label">Ngày bắt đầu:</label>
                            <input type="text" class="form-control pull-right" id="start_time" name="start_time" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Ngày kết thúc:</label>
                            <input type="text" class="form-control pull-right" id="end_time" name="end_time" value="">

                        </div>


                    </div>
                    <!-- /.box-body -->

                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Link Stream</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('link_stream.360') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i>360</i>
                                </div>
                                <input type="text" class="form-control" name="link_stream[360]" value="{{old("link_stream.360")}}" placeholder="Link Video 360">
                            </div>
                            @if ($errors->has('link_stream.360'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('link_stream.360') }}</strong>
                                    </span>
                        @endif
                        <!-- /.input group -->
                        </div>

                        <div class="form-group {{ $errors->has('link_stream.480') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i>480</i>
                                </div>
                                <input type="text" class="form-control" name="link_stream[480]" value="{{old("link_stream.480")}}" placeholder="Link Video 480">
                            </div>
                            @if ($errors->has('link_stream.480'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('link_stream.480') }}</strong>
                                    </span>
                        @endif
                        <!-- /.input group -->
                        </div>

                        <div class="form-group {{ $errors->has('link_stream.720') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i>720</i>
                                </div>
                                <input type="text" class="form-control" name="link_stream[720]" value="{{old("link_stream.720")}}" placeholder="Link Video 720">
                            </div>
                            @if ($errors->has('link_stream.720'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('link_stream.720') }}</strong>
                                    </span>
                        @endif
                        <!-- /.input group -->
                        </div>

                        <div class="form-group {{ $errors->has('link_stream.1080') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i>1080</i>
                                </div>
                                <input type="text" class="form-control" name="link_stream[1080]" value="{{old("link_stream.1080")}}" placeholder="Link Video 1080">
                            </div>
                            @if ($errors->has('link_stream.1080'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('link_stream.1080') }}</strong>
                                    </span>
                        @endif
                        <!-- /.input group -->
                        </div>


                    </div>
                </div>

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Link Download</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('link_download.360') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i>360</i>
                                </div>
                                <input type="text" class="form-control" name="link_download[360]" value="{{old("link_download.360")}}" placeholder="Link Video 360">
                            </div>
                            @if ($errors->has('link_download.360'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('link_download.360') }}</strong>
                                    </span>
                            @endif
                            <!-- /.input group -->
                        </div>

                        <div class="form-group {{ $errors->has('link_download.480') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i>480</i>
                                </div>
                                <input type="text" class="form-control" name="link_download[480]" value="{{old("link_download.480")}}" placeholder="Link Video 480">
                            </div>
                            @if ($errors->has('link_download.480'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('link_download.480') }}</strong>
                                    </span>
                            @endif
                            <!-- /.input group -->
                        </div>

                        <div class="form-group {{ $errors->has('link_download.720') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i>720</i>
                                </div>
                                <input type="text" class="form-control" name="link_download[720]" value="{{old("link_download.720")}}" placeholder="Link Video 720">
                            </div>
                            @if ($errors->has('link_download.720'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('link_download.720') }}</strong>
                                    </span>
                            @endif
                            <!-- /.input group -->
                        </div>

                        <div class="form-group {{ $errors->has('link_download.1080') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i>1080</i>
                                </div>
                                <input type="text" class="form-control" name="link_download[1080]" value="{{old("link_download.1080")}}" placeholder="Link Video 1080">
                            </div>
                            @if ($errors->has('link_download.1080'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('link_download.1080') }}</strong>
                                    </span>
                        @endif
                        <!-- /.input group -->
                        </div>

                        <div class="form-group {{ $errors->has('has_ring') ? ' has-error' : '' }}">
                            <label for="is_feature">Có link nhạc chờ &nbsp;&nbsp;&nbsp;</label>
                            <label class="switch">
                                <input type="checkbox" name="has_ring">
                                <span class="slider round"></span>
                            </label>
                            @if ($errors->has('has_ring'))
                                <span class="text-danger">
                                            <strong>{{ $errors->first('has_ring') }}</strong>
                                        </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('has_download') ? ' has-error' : '' }}">
                            <label for="name">Cho phép download &nbsp;&nbsp;&nbsp;</label>
                            <label class="switch">
                                <input type="checkbox" name="has_download">
                                <span class="slider round"></span>
                            </label>
                            @if ($errors->has('has_download'))
                                <span class="text-danger">
                                            <strong>{{ $errors->first('has_download') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Publish</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('thumbnail_url') ? ' has-error' : '' }}">
                            <label for="uploadthumbnail_url"><i class='fa fa-img'></i> {{__("messages.avatar")}}</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <a id="uploadthumbnail_url" data-input="thumbnail-url" data-preview="thumbnail-preview" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> {{__("messages.uploads")}}</a>
                                </span>
                                <input id="thumbnail-url" class="form-control" type="text" name="thumbnail_url" value="{{old('thumbnail_url')}}">
                            </div>
                            <img id="thumbnail-preview" src="{{old('thumbnail_url')}}" style="margin-top:15px;max-height:100px;"/>
                            @if ($errors->has('thumbnail_url'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('thumbnail_url') }}</strong>
                                    </span>
                            @endif
                        </div>



                        <div class="form-group {{ $errors->has('priority') ? ' has-error' : '' }}">
                            <label for="name">{{__("messages.priority")}}</label>
                            <input type="number" class="form-control" id="priority" name="priority" value="{{old("priority",0)}}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('priority') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status">{{__("messages.status")}}</label>
                            {!! Helpers::statusSelect(old('status')) !!}
                            @if ($errors->has('status'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{__("messages.submit")}}</button>
                    </div>
                </div>
            </div>
        </form>

        <!-- /.col -->
    </div>
@endsection
@section('script')
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script>
        $('#uploadthumbnail_url').filemanager('image');
        $('#uploadVideo').filemanager('file');
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2({
            }).on("select2:select select2:unselect", function (e) {
                //this returns all the selected item

            })
        });
    </script>
@endsection