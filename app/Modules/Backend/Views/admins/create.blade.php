<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */
?>
@extends('Backend::layouts.app')

@section('title', '| Add User')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class='fa fa-user-plus'></i>Thêm mới thành viên</h3>
                </div>
                @if ($errors->any())

                    <div class="col-sm-12">
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i></h4>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
            @endif
                <!-- /.box-header -->
                <form action="{{route("backend.admins.add.post")}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">{{__("messages.name")}}</label>
                            <input type="text" name="name" class="form-control" required id="name" value="{!! old('name') !!}" >
                        </div>
                        <div class="form-group">
                            <label for="phone">Số điện thoại</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="text" class="form-control" name="phone" id="phone" required value="{!! old('phone') !!}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">{{__("messages.email")}}</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <input type="text" class="form-control" name="email" id="email" required value="{!! old('email') !!}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password">{{__("messages.password")}}</label>
                            <input type="password" name="password" class="form-control" id="password" required>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">{{__("messages.password_confirmation")}}</label>
                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" required>
                        </div>
                        <div class="checkbox">
                            @foreach ($roles as $role)
                                <label>
                                    <input type="radio" name="roles[]" value="{{$role->id}}"> {{$role->display_name}}
                                </label>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">{{__("messages.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.col -->
    </div>
@endsection
@section('script')
@endsection
