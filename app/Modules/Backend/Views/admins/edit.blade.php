<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */
?>
@extends('Backend::layouts.app')

@section('title', '| Edit User')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class='fa fa-user-plus'></i> Chỉnh sửa thành viên</h3>
                </div>
                @if ($errors->any())
                    <div class="col-sm-12">
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i></h4>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
            @endif
            <!-- /.box-header -->
                <form action="{{route("backend.admins.edit.post",$user->id)}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">{{__("messages.name")}}</label>
                            <input type="text" class="form-control" name="name" id="name" required value="{!! old('name',isset($user->name)?$user->name:null) !!}" >
                        </div>
                        <div class="form-group">
                            <label for="name">Số điện thoại</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="text" class="form-control" name="phone" required id="phone" value="{!! old('phone',isset($user->phone)?$user->phone:null) !!}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">{{__("messages.email")}}</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <input type="text" class="form-control" name="email" required id="email" value="{!! old('email',isset($user->email)?$user->email:null) !!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password">{{__("messages.password")}}</label>
                            <input type="password" name="password" class="form-control"  value="" id="password" >
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">{{__("messages.password_confirmation")}}</label>
                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" >
                        </div>
                        <div class="checkbox">
                            <h4>{{__("messages.assign.roles")}}</h4>
                            <?php
                                $userRole = $user->getRoleNames()->toArray();
                            ;?>
                            @foreach ($roles as $role)
                                <label>
                                    <input type="radio" name="roles[]" @if(in_array($role->name,$userRole)) checked @endif  value="{{$role->name}}"> {{$role->display_name}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </label>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">{{__("messages.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.col -->
    </div>

@endsection
@section('script')
@endsection