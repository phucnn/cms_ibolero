<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */
?>
@extends('Backend::layouts.app')

@section('title', '| Users')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Danh sách đại lý</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a href="{{ route('backend.admins.add') }}" class="btn btn-primary">{{__("messages.add.new")}}</a>
                            <a href="{{ route("backend.roles.list") }}" class="btn btn-default">{{__("messages.roles")}}</a>
                            <a href="{{ route("backend.permissions.list") }}" class="btn btn-default">{{__("messages.permissions")}}</a>
                        </div>
                    </div>
                    <table _id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{__("messages.name")}}</th>
                            <th>{{__("messages.email")}}</th>
                            <th>{{__("messages.created_date")}}</th>
                            <th>{{__("messages.roles")}}</th>
                            <th>{{__("messages.actions")}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($admins as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                                <td>{{  $user->roles()->pluck('display_name')->implode(', ') }}</td>{{-- Retrieve array of roles associated to a user and convert to string --}}
                                <td>
                                    <a href="{{route('backend.admins.edit',$user->_id)}}">
                                        <button class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></button>
                                    </a>
                                    <a href="{{route('backend.admins.delete',$user->_id)}}" onclick="return confirm('Are you sure you want to delete?')">
                                        <button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.col -->
    </div>
@endsection