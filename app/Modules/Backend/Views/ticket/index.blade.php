<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;
use Illuminate\Support\Facades\Input;

?>
@extends('Backend::layouts.app')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="clearfix">
                        <h3 class="box-title pull-left"><i class="fa fa-card-circle-o"></i> Danh sách báo lỗi</h3>
                    </div>
                </div>
                <div class="box-body">
                    @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                    @endif

                </div>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="box box-primary box-solid">
                <div class="box-header">
                    <h3 class="box-title">Tìm kiếm</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <form action="{{route('backend.ticket.list')}}" method="get" class="form-horizontal" id="card-form-data">
                    <div class="box-body">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Name/ID</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="search" id="search" value="{{@$input['search']}}" class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Trạng thái</label>
                                    <div class="col-sm-9">
                                        {!!  \App\Helpers\Helpers::ticketStatusSearch()!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Loại</label>
                                    <div class="col-sm-9">
                                        {!!  \App\Helpers\Helpers::ticketTypeSearch()!!}
                                    </div>
                                </div>
                                @can('admins-manager')
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Users</label>
                                    <div class="col-sm-9">
                                        {{\App\Helpers\Helpers::selectCreatedBy($listUsers)}}
                                    </div>
                                </div>
                                @endcan
                            </div>
                        </div>


                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-primary pull-right" value="" id="update-new-data"><i class="fa fa-search"></i> {{__("messages.search")}}</button>
                        <a href="{{route('backend.ticket.list')}}" class="btn btn-warning "><i class="fa fa-refresh"></i> Reset</a>
                    </div>
                </form>
            </div>
        </div>



        <div class="col-xs-8">
            <form action="{{route('backend.ticket.list')}}" method="post" id="ticketList">
                {!! csrf_field() !!}
                <div class="box box-primary box-solid ">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>

                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-hover table-striped vertical-middle">
                                <thead>
                                <tr>
                                    <th>Tiêu đề</th>
                                    <th>Người tạo</th>
                                    <th>Loại</th>
                                    <th width="20%">Thời gian tạo</th>
                                    <th width="10%">Status</th>
                                    <th width="5%">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </form>
        </div>
        <!-- /.col -->
    </div>
@endsection
@section("script")
    <script>

        $(document).on("click","#sendTicket",function() {
            $('#sendTicketModal').modal('show');
        });

        $(function () {
            $("#example2").DataTable({
                "pageLength": 25,
                "lengthMenu": [10, 25, 50, 100, 150, 200],
                "columnDefs": [
                    {"orderable": false, "targets": 0}
                ],
                "ajax":
                    {
                        "url": "{{route('backend.dataTable.ticket')}}",
                        "type": "POST"
                    },
                "columns": [
                    { "data": "subject" },
                    { "data": "name" },
                    { "data": "type" },
                    { "data": "time_information" },
                    { "data": "status" },
                    { "data": "action" },
                ],
                "order" : [[1, "desc"]],
                "createdRow": function( row, data, dataIndex){
                    if( data['view_check'] ===  0){
                        $(row).addClass('text-bold');
                    }
                }
            });
        });

        $('#update-new-data').on('click', function () {
            var data = {};
            $.map($("#card-form-data [id]"), function(n, i) {
                data[n.id] = $('#'+n.id).val();
            });
            $("#example2").DataTable({
                destroy: true,
                "pageLength": 25,
                "lengthMenu": [10, 25, 50, 100, 150, 200],
                "columnDefs": [
                    {"orderable": false, "targets": 0}
                ],
                "ajax":
                    {
                        "url": "{{route('backend.dataTable.ticket')}}",
                        "type": "POST",
                        "data" : data
                    },
                "columns": [
                    { "data": "subject" },
                    { "data": "name" },
                    { "data": "type" },
                    { "data": "time_information" },
                    { "data": "status" },
                    { "data": "action" },
                ],
                "order" : [[1, "desc"]],
                "createdRow": function( row, data, dataIndex){
                    if( data['view_check'] ===  0){
                        $(row).addClass('text-bold');
                    }
                }
            });
        });

    </script>
@endsection

