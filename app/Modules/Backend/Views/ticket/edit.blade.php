<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;

?>
@extends('Backend::layouts.app')


@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="clearfix">
                        <h3 class="box-title"><i class='fa fa-envelope'></i> Tiêu đề báo lỗi: {{$data->subject}}</h3>
                    </div>
                </div>
                <div class="box-body">
                    @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary box-solid">
                <div class="box-body no-padding">
                    <div class="mailbox-read-message">
                        <h3>{{$data->content}}</h3>
                        <span class="mailbox-read-time pull-right">{{date('d-m-Y H:i:s',strtotime($data->created_at))}}</span></h5>
                    </div>
                </div>
            </div>


            <!-- Construct the box with style you want. Here we are using box-danger -->
            <!-- Then add the class direct-chat and choose the direct-chat-* contexual class -->
            <!-- The contextual class should match the box, so we are using direct-chat-danger -->
            <div class="box box-danger direct-chat direct-chat-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Log support</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages">
                        <!-- Message. Default to the left -->
                        @foreach($data->log_chat as $oneLog)
                        <div class="direct-chat-msg @if($oneLog['user'] == \Illuminate\Support\Facades\Auth::id())  @else right @endif">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name @if($oneLog['user'] == \Illuminate\Support\Facades\Auth::id()) pull-left @else pull-right @endif">{{ $listAgency[$oneLog['user']]}}</span>
                                <span class="direct-chat-timestamp @if($oneLog['user'] == \Illuminate\Support\Facades\Auth::id()) pull-right @else pull-left @endif">{{ $oneLog['time'] }}</span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img" @if($oneLog['user'] == \Illuminate\Support\Facades\Auth::id()) src="/AdminLTE/dist/img/user1-128x128.jpg" @else src="/AdminLTE/dist/img/user3-128x128.jpg" @endif alt="">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                                {!! $oneLog['content'] !!}
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                        @endforeach
                        <!-- /.direct-chat-msg -->

                    </div>
                    <!--/.direct-chat-messages-->

                </div>
                <!-- /.box-body -->
            </div>
            <!--/.direct-chat -->
        </div>
        <div class="col-md-6">
            <form action="{{route('backend.ticket.reply',$data->id)}}" method="POST" class="">
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Trả lời</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="form-group col-xs-12 {{ $errors->has('reply') ? ' has-error' : '' }}">
                                <label for="reply">Trả lời</label>
                                <textarea name="reply" id="reply" class="form-control tinymce-editor"></textarea>
                                @if ($errors->has('reply'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reply') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">Gửi trả lời</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection