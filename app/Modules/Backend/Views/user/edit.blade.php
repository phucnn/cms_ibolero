<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;

?>
@extends('Backend::layouts.app')

@section('title', '| Chỉnh sửa User')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="clearfix">
                        <h3 class="box-title pull-left">{{$data->full_name}}</h3>
                    </div>
                </div>
                <div class="box-body">
                    @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <form action="{{route('backend.song.edit.post',$data->id)}}" method="POST" class="">
            <div class="col-md-8">
                <div class="box box-primary box-solid ">
                    <div class="box-header">
                        <h3 class="box-title">Thông tin chung</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group {{ $errors->has('full_name') ? ' has-error' : '' }}">
                            <label for="full_name">{{__("messages.name")}}</label>
                            <input type="text" class="form-control" id="full_name" name="full_name" value="{{old("full_name",$data->full_name)}}" placeholder="Name">
                            @if ($errors->has('full_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('full_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{old("email",$data->email)}}" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{old("phone",$data->phone)}}" placeholder="Phone">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">{{__("messages.password")}}</label>
                            <input type="password" name="password" class="form-control"  value="" id="password" >
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">{{__("messages.password_confirmation")}}</label>
                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" >
                        </div>

                        <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status">{{__("messages.status")}}</label>
                            {!! Helpers::statusSelect(old('status',$data->status)) !!}
                            @if ($errors->has('status'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            Nữ &nbsp;&nbsp;&nbsp;
                            <label class="switch" style="margin-top: 5px">
                                <input type="checkbox" name="gender" {{ $data->gender == "1" ? "checked" : "" }}>
                                <span class="slider round"></span>
                            </label>
                            &nbsp;&nbsp;&nbsp;Nam
                        </div>
                        <div class="form-group">
                            Chưa đổi pass &nbsp;&nbsp;&nbsp;
                            <label class="switch" style="margin-top: 5px">
                                <input type="checkbox" name="is_change_pass" {{ $data->is_change_pass == "1" ? "checked" : "" }}>
                                <span class="slider round"></span>
                            </label>
                            &nbsp;&nbsp;&nbsp;Đã đổi pass
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{__("messages.submit")}}</button>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Danh sách bài hát ưa thích</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        @foreach($favorite as $oneSong)
                            <p><a href="{{ route('backend.song.edit', $oneSong->_id) }}">{{ $oneSong->name }}</a></p>
                        @endforeach
                    </div>
                </div>
            </div>
        </form>

        <!-- /.col -->
    </div>
@endsection
@section('script')
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script>
        $('#uploadthumbnail_url').filemanager('image');
        $('#uploadVideo').filemanager('file');
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2({
            }).on("select2:select select2:unselect", function (e) {
                //this returns all the selected item

            })
        });
    </script>
@endsection