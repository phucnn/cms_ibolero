<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */
?>
@extends('Backend::layouts.app')

@section('title', '| Create Permission')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class='fa fa-key'></i> {{__("messages.permission.add")}}</h3>
                </div>
                @if ($errors->any())

                    <div class="col-sm-12">
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i></h4>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
            @endif
            <!-- /.box-header -->
                <form action="{{route('backend.permissions.add.post')}}" method="POST" class="">
                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="display_name">{{__("messages.name")}} hiển thị</label>
                            <input type="text" name="display_name" class="form-control" id="display_name" value="{{old("display_name")}}" placeholder="Tên hiển thị">
                        </div>
                        <div class="form-group">
                            <label for="name">{{__("messages.name")}}</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old("name")}}" placeholder="Name">
                        </div>
                        @if(!$roles->isEmpty())
                            {{--If no roles exist yet--}}
                            <h4>{{__("messages.assign.roles")}}</h4>
                            <div class="checkbox">
                                @foreach ($roles as $role)
                                    <div class="col-xs-2 col-sm-3 col-md-2">
                                        <label>
                                            <input type="checkbox" name="roles[]" value="{{$role->id}}"> {{ucfirst($role->display_name)}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">{{__("messages.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.col -->
    </div>

@endsection