<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */
?>
@extends('Backend::layouts.app')

@section('title', '| Permissions')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="clearfix">
                        <h3 class="box-title pull-left"><i class="fa fa-key"></i>{{__("messages.permissions")}}</h3>
                        <div class="pull-right">
                            <a href="{{ route('backend.permissions.add') }}" class="btn btn-primary">{{__("messages.permission.add")}}</a>
                            <a href="{{ route('backend.admins.list') }}" class="btn btn-default">{{__("messages.admins")}}</a>
                            <a href="{{ route('backend.roles.list') }}" class="btn btn-default">{{__("messages.roles")}}</a>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                    @endif
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{__("messages.name")}}</th>
                            <th>{{__("messages.name")}}</th>
                            <th>{{__("messages.actions")}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($permissions as $permission)
                            <tr>
                                <td>{{ $permission->display_name }}</td>
                                <td>{{ $permission->name }}</td>
                                <td>
                                    <a href="{{route("backend.permissions.edit",$permission->id)}}">
                                        <button class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></button>
                                    </a>
                                    <a href="{{route('backend.permissions.delete',$permission->id)}}" onclick="return confirm('Are you sure you want to delete?')">
                                        <button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.col -->
    </div>
@endsection