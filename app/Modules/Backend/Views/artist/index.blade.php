<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */

use App\Helpers\Helpers;
use Illuminate\Support\Facades\Input;

?>
@extends('Backend::layouts.app')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="clearfix">
                        <h3 class="box-title pull-left"><i class="fa fa-card-circle-o"></i> Danh sách ca sĩ </h3>
                        <div class="pull-right">
                            <a href="{{ route('backend.artist.add') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                    @endif

                </div>
            </div>
        </div>


        <div class="col-xs-4">
            <div class="box box-primary box-solid">
                <div class="box-header">
                    <h3 class="box-title">Tìm kiếm</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <form action="{{route('backend.artist.list')}}" method="get" class="form-horizontal" id="search-form-data">
                    <div class="box-body">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Tên ca sĩ</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="search_name_id" id="search_name_id" value="{{@$input['search_name_id']}}" class="form-control" placeholder="Tên ca sĩ">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Trạng thái</label>
                                    <div class="col-sm-9">
                                        {{\App\Helpers\Helpers::statusSelect('', 1)}}
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-primary pull-right" value="" id="update-new-data"><i class="fa fa-search"></i> {{__("messages.search")}}</button>
                        <a href="{{route('backend.artist.list')}}" class="btn btn-warning "><i class="fa fa-refresh"></i> Reset</a>
                    </div>
                </form>
            </div>
        </div>



        <div class="col-xs-8">
            <form action="{{route('backend.artist.list')}}" method="post" id="cardList">
                {!! csrf_field() !!}
                <input type="hidden" name="typeChange" id="typeChange" value="">
                <div class="box box-primary box-solid ">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>

                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-hover table-striped vertical-middle">
                                <thead>
                                <tr>
                                    <th>Tên</th>
                                    <th>Ảnh</th>
                                    <th width="20%">Thời gian tạo</th>
                                    <th width="10%">Trạng thái</th>
                                    <th width="5%">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </form>
        </div>
        <!-- /.col -->
    </div>
@endsection
@section("script")
    <script>
        $(function () {
            $("#example2").DataTable({
                processing: true,
                serverSide: true,
                "pageLength": 25,
                "lengthMenu": [10, 25, 50],
                "columnDefs": [
                    {"orderable": false, "targets": 0}
                ],
                "ajax":
                    {
                        "url": "{{route('backend.artist.dataTable')}}",
                        "type": "POST"
                    },
                "columns": [
                    { "data": "name" },
                    { "data": "thumbnail_url" },
                    { "data": "created_time" },
                    { "data": "status" },
                    { "data": "action" },
                ],
                "order" : [[1, "desc"]]
            });
        });

        $('#update-new-data').on('click', function () {
            var data = {};
            $.map($("#search-form-data [id]"), function(n, i) {
                data[n.id] = $('#'+n.id).val();
            });
            $("#example2").DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                "pageLength": 25,
                "lengthMenu": [10, 25, 50],
                "columnDefs": [
                    {"orderable": false, "targets": 0}
                ],
                "ajax":
                    {
                        "url": "{{route('backend.artist.dataTable')}}",
                        "type": "POST",
                        "data" : data
                    },
                "columns": [
                    { "data": "name" },
                    { "data": "thumbnail_url" },
                    { "data": "created_time" },
                    { "data": "status" },
                    { "data": "action" },
                ],
                "order" : [[1, "desc"]]
            });
        });


    </script>
@endsection

