<?php
/**
 * Created by PhpStorm.
 * User: Mr-ha
 * Date: 12/11/2017
 * Time: 12:10 AM
 */
use App\Helpers\Helpers;
use Illuminate\Support\Facades\Input;
?>
@extends('Backend::layouts.app')
@section('content')
    <div class="box box-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-aqua-active">
            <h3 class="widget-user-username">{{!empty($user->name)?$user->name:""}}</h3>
            <h5>{{!empty($user->phone)?$user->phone:""}}</h5>
            <h5>{{!empty($user->address)?$user->address:""}}</h5>
        </div>
        <div class="widget-user-image">
            <img class="img-circle" src="/AdminLTE/dist/img/user2-160x160.jpg" alt="User Avatar">
        </div>
        <div class="box-footer">
            <div class="row">
                <!-- /.col -->
                <div class="col-sm-6 border-right">
                    <div class="description-block">
                        <h5 class="description-header">{{ $user->email }}</h5>
                        <span class="description-text">EMAIL</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <div class="description-block">
                        <h5 class="description-header">{{ $user->created_at }}</h5>
                        <span class="description-text">NGÀY GIA NHẬP</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>
    <style>
        #selected_month .ui-datepicker-calendar {
            display: none;
        }
    </style>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#selected_month').datepicker( {
                format: "mm-yyyy",
                viewMode: "months",
                minViewMode: "months"
            });
        });
    </script>
@endsection