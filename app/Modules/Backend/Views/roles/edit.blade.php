<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */
?>
@extends('Backend::layouts.app')

@section('title', '| Edit Role')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class='fa fa-key'></i> {{__("messages.role.edit")}}: {{$role->name}}</h3>
                </div>
                @if ($errors->any())

                    <div class="col-sm-12">
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i></h4>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
            @endif
            <!-- /.box-header -->
                <form action="{{route('backend.roles.edit.post', $role->id)}}" method="post" class="">
                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">{{__("messages.name")}} hiển thị</label>
                            <input type="text" name="display_name" class="form-control" id="display_name"  value="{{old("display_name",$role->display_name)}}" placeholder="Tên hiển thị">
                        </div>
                        <div class="form-group">
                            <label for="name">{{__("messages.name")}}</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old('name',$role->name)}}" >
                        </div>
                        <div class="checkbox">
                            <h5><b>{{__("messages.assign.permissions")}}</b></h5>

                            @foreach ($permissions as $permission)
                                <div class="col-xs-2 col-sm-3 col-md-2">
                                    <label>
                                        <input type="checkbox" @if(in_array($permission->id,$role->permission_ids)) checked @endif name="permissions[]"
                                               value="{{$permission->id}}"> {{ucfirst($permission->display_name)}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.col -->
    </div>

@endsection