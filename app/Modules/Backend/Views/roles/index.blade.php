<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:39 PM
 */
?>
@extends('Backend::layouts.app')

@section('title', '| Roles')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="clearfix">
                        <h3 class="box-title pull-left"><i class="fa fa-key"></i>{{__("messages.roles")}}</h3>
                            <div class="pull-right">
                                <a href="{{ route('backend.roles.refresh') }}" class="btn btn-success"><i class="fa fa-refresh"></i> Refresh</a>
                                <a href="{{ route('backend.roles.add') }}" class="btn btn-primary">{{__("messages.role.add")}}</a>
                                <a href="{{ route('backend.admins.list') }}" class="btn btn-default">{{__("messages.admins")}}</a>
                                <a href="{{ route('backend.permissions.list') }}" class="btn btn-default">{{__("messages.permissions")}}</a>
                            </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                    @endif
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            @foreach ($roles as $role)
                                <th> <a href="{{route('backend.roles.edit',$role->id)}}" >{{ @$role->display_name }}</a>
                                    @if($role->name != "admin")
                                        <a href="{{route('backend.roles.delete',$role->id)}}" onclick="return confirm('Bạn có thực sự muốn xóa quyền này?')">
                                            <button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                        </a>
                                    @endif
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($listPermissions as $onePermission)
                            <tr>
                                <td>{{ $onePermission->display_name }}
                                </td>
                                @foreach ($roles as $role)
                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" {!! in_array($role->id, $onePermission->role_ids) ? 'checked="checked"' : '' !!} onchange="confirmStatus(this, '{!! $role->id !!}', '{!! $onePermission->id !!}')">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.col -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="loadMeLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="loader"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("script")
    <script>
        function confirmStatus(box, role_id, permission_id) {
            var txt = 'Bạn có chắc muốn gỡ phân quyền này ?';
            var oldStatus = 'sync';
            var newStatus = 'async';
            var newChange = false;
            var oldChange = true;
            if (box.checked) {
                txt = "Bạn có chắc muốn gán phân quyền này ?";
                oldStatus = 'async';
                newStatus = 'sync';
                newChange = true;
                oldChange = false;
            }

            let r = confirm(txt);
            if (r === true) {
                $.ajax({
                    type : "POST",
                    url: "{{route('backend.roles.changeStatus')}}",
                    data : {
                        newStatus : newStatus,
                        role_id : role_id.toString(),
                        permission_id : permission_id.toString()
                    },
                    beforeSend : function (json) {
                        $("#loadMe").modal({
                            backdrop: "static", //remove ability to close modal with click
                            keyboard: false, //remove option to close with keyboard
                            show: true //Display loader!
                        });
                    },
                    complete : function () {
                        $("#loadMe").modal('hide');
                    }
                }).done(function(data) {
                    box.checked = newChange;
                });
            }else{
                box.checked = oldChange;
            }
        }
    </script>
@endsection