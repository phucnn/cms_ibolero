<?php
namespace App\Helpers;
use App\Admin;
use App\CardLog;
use Illuminate\Support\Facades\Log;

class Common{
    static function getCurrentIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) //check ip from share internet
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    static function getCurrentUrl() {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
            $url = "https://";
        }else{
            $url = "http://";
        }
        $url .= $_SERVER['SERVER_NAME'];
        if($_SERVER['SERVER_PORT'] != 80){
            $url .= ":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        }else{
            $url .= $_SERVER["REQUEST_URI"];
        }
        $url = htmlspecialchars($url,ENT_QUOTES, 'UTF-8');
        return $url;
    }

    static function formatNumber($value) {
        $str = number_format($value, 0, "", ".");
        return $str;
    }

   

    static function writeLog($msg = "")
    {
        $date = date('d-m-Y');
        Log::useFiles(storage_path()."/logs/api/request-{$date}.log");
        Log::info($msg);
    }

}