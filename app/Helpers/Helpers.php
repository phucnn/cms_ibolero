<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 12/12/2017
 * Time: 2:53 PM
 */

namespace App\Helpers;


use App\Category;
use App\Page;
use App\Singer;
use DateInterval;
use DatePeriod;
use DateTime;

class Helpers
{
    const STATUS = [
        "0" => 'hidden',
        "1" => 'publish',
    ];

    const  STATUSCLASS = [
        1 => 'success',
        2 => 'warning',
    ];

    const TICKET_TYPE = [
        'khac' => '<span style="color: darkgreen">Khác</span>',
        'yeu_cau' => '<span style="color: sandybrown">Yêu cầu</span>',
    ];

    const TICKET_STATUS_WAITING = 0;
    const TICKET_STATUS_RESOLVED = 1;

    const TICKET_STATUS = [
        self::TICKET_STATUS_WAITING => '<span class="label label-danger">Chờ xử lý</span>',
        self::TICKET_STATUS_RESOLVED => '<span class="label label-success">Đã xử lý</span>',
    ];

    public static function ticketStatusSearch($select = "")
    {
        $html = '<select name="status" id="status" class="form-control"><option  value="">' . trans('messages.select.one') . '</option>';

        foreach (self::TICKET_STATUS as $key => $value) {
            if ($select === $key) {
                $html .= '<option selected  value="' . $key . '" >' . ucfirst($value) . '</option>';
            } else {
                $html .= '<option value="' . $key . '" >' . ucfirst($value) . '</option>';
            }
        }
        $html .= "<select>";
        return $html;
    }

    public static function ticketTypeSearch($select = "")
    {
        $html = '<select name="type" id="type" class="form-control"><option  value="">' . trans('messages.select.one') . '</option>';

        foreach (self::TICKET_TYPE as $key => $value) {
            if ($select === $key) {
                $html .= '<option selected  value="' . $key . '" >' . ucfirst($value) . '</option>';
            } else {
                $html .= '<option value="' . $key . '" >' . ucfirst($value) . '</option>';
            }
        }
        $html .= "<select>";
        return $html;
    }

    public static function selectCreatedBy($data, $select = false)
    {
        $html = '<select name="created_by" id="created_by" class="form-control"><option  value="">' . trans('messages.select.one') . '</option>';

        foreach ($data as $value) {
            if ($select == $value->id) {
                $html .= '<option selected  value="' . $value->id . '" >' . ucfirst($value->name) . '</option>';
            } else {
                $html .= '<option value="' . $value->id . '" >' . ucfirst($value->name) . '</option>';
            }
        }
        $html .= "<select>";
        echo $html;
    }

    public static function statusView($status, $echo = true)
    {
        $statusClass = self::STATUSCLASS;
        foreach (self::STATUS as $key => $value) {
            if ($status == $key) {
                if ($echo) {
                    echo '<span class="label label-' . $statusClass[$key] . '">' . ucfirst($value) . '</span>';

                } else {
                    return '<span class="label label-' . $statusClass[$key] . '">' . ucfirst($value) . '</span>';

                }
            }
        }
    }

    public static function statusSelect($select = false, $type = 0)
    {
        $html = '<select name="status" id="status" class="form-control">';

        if($type == 1){
            $html .= '<option   value="" >' . trans('messages.select.one') . '</option>';
        }

        foreach (self::STATUS as $key => $value) {
            if ($select == $key) {
                $html .= '<option selected  value="' . $key . '" >' . ucfirst($value) . '</option>';
            } else {
                $html .= '<option value="' . $key . '" >' . ucfirst($value) . '</option>';
            }
        }
        $html .= "<select>";
        echo $html;
    }

}