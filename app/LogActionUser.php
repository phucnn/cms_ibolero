<?php

namespace App;


use Auth;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * App\Setting
 *
 * @property-read mixed $id
 * @mixin \Eloquent
 */
class LogActionUser extends Model
{
    //
    protected $fillable = ['customer_id', 'type', 'song_id', 'song_time', 'song_total_time'];
    protected $collection = 'log_action_user';


    public static function boot()
    {
        parent::boot();

        // create a event to happen on updating
        static::updating(function ($table) {
            $table->updated_by = Auth::id();
        });

        // create a event to happen on deleting
        static::deleting(function ($table) {
            $table->deleted_by = Auth::id();
        });

        // create a event to happen on saving
        static::saving(function ($table) {
            $table->created_by = Auth::id();
        });
    }
}
