<?php

namespace App;



use Auth;
use Jenssegers\Mongodb\Eloquent\Model;

class Notify extends Model
{
    //
    protected $fillable = ['title','sub_title','message','from','to','data','type'];
    protected $collection='notify';
}
