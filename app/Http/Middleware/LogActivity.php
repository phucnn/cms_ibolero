<?php

namespace App\Http\Middleware;

use Closure;

class LogActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check()) {

            $data = [];
            $data['method'] = $request->getMethod();
            $data['path'] = $request->path();
            $data['full_url'] = $request->fullUrl();
            $data['user-agent'] = $request->header('user-agent');
            $data['route']['module'] = $request->route()->getAction('module');
            $data['route']['as'] = $request->route()->getAction('as');
            $data['route']['attr'] = $request->route()->parameters();
            $data['ip'] = $request->getClientIp();
            $data['user_id'] = \Auth::id();
        }
        dd($data);
        return $next($request);
    }
}
