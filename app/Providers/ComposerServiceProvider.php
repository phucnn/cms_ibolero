<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 3/30/2018
 * Time: 10:30 AM
 */

namespace App\Providers;


use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return  void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            'Frontend::composer.sidebar', 'App\Modules\Frontend\Http\ViewComposers\SidebarComposer'
        );
        View::composer(
            'Frontend::composer.slider', 'App\Modules\Frontend\Http\ViewComposers\SliderComposer'
        );
    }

    /**
     * Register the service provider.
     *
     * @return  void
     */
    public function register()
    {
        //
    }
}