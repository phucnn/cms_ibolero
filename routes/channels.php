<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

/*
     * Authenticate the user's personal channel...
     */

Broadcast::channel('App.User.{id}', function ($user, $id) {
//    \Debugbar::disable();
    app('debugbar')->disable();
    return (int) $user->_id === (int) $id;
});

