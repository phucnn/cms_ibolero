require("./bootstrap");


var UserId = $('meta[name="userId"]').attr('content');
if (typeof UserId !== 'undefined') {
    window.Echo.private('App.User.' + UserId)
        .notification((notification) => {
            $('.readAllNotifiy').html(' <i class="fas fa-bell"></i><span class="badge badge-pill badge-danger position-absolute" style="top:0; right: 10px;">'+notification.countUnRead+'</span>');
            $.notify({
                icon: notification.icon,
                title: notification.title,
                message: notification.message
            }, {
                type: 'minimalist',
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                },
                delay: 5000,
                icon_type: 'image',
                mouse_over: 'pause',
                template: '<div data-notify="container" class="col-11 col-md-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<img data-notify="icon" class="rounded-circle float-left">' +
                '<span class="mb-0" data-notify="title">{1}</span>' +
                '<span data-notify="message">{2}</span>' +
                '</div>'
            });
        });
}

window.Echo.channel('web-notify').listen('WebNotify', function (data) {
    $('.readAllNotifiy').html(' <i class="fas fa-bell"></i><span class="badge badge-pill badge-danger position-absolute" style="top:0; right: 10px;">'+data.countUnRead+'</span>');
    $.notify({
        icon: data.icon,
        title: data.title,
        message: data.message
    }, {
        type: 'minimalist',
        animate: {
            enter: 'animated bounceIn',
            exit: 'animated bounceOut'
        },
        delay: 5000,
        icon_type: 'image',
        mouse_over: 'pause',
        template: '<div data-notify="container" class="col-11 col-md-3 alert alert-{0}" role="alert">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<img data-notify="icon" class="rounded-circle float-left">' +
        '<span class="mb-0" data-notify="title">{1}</span>' +
        '<span data-notify="message">{2}</span>' +
        '</div>'
    });
})

