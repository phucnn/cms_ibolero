<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:43 PM
 */
?>
@extends('Backend::layouts.errors')
@section('content')
    <div class="error-page">
        <h2 class="headline text-red">403</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
            <p>
                {{ $exception->getMessage() }}
                You may <a href="{{url("/")}}">return to home</a>
            </p>
        </div>
    </div>
@endsection