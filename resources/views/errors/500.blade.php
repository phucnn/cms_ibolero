<?php
/**
 * Created by PhpStorm.
 * User: Mr-Ha
 * Date: 1/18/2018
 * Time: 1:43 PM
 */
?>
@extends('Backend::layouts.errors')
@section('content')
    <!-- Content -->
    <div class="container-fluid py-3 py-md-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center mb-3">
                <img class="mw-100 mb-4" src="{{url('/')}}/img/img-404.png" alt="">
                <h1><span class="font-weight-bold text-uppercase badge badge-pill badge-danger">Error</span></h1>
            </div>
            <div class="col-12 col-md-5 text-center">
                <h2>Xin lỗi, Vui lòng thử lại sau</h2>
                <a href="{{url('/')}}" class="btn badge-pill px-3 text-uppercase btn-outline-success mb-3"><i class="fas fa-home mr-2"></i>Về trang chủ</a>
            </div>
        </div>
    </div>
@endsection