var socketGlobal = null;
window.onload = function(){   
    var username_ptt = $('#username_hidden').val();
    if(username_ptt == null || username_ptt == '' || username_ptt == 'null') {
        username_ptt = null;
    }
    
    if (username_ptt != null) {

        var socket = io.connect('http://192.168.100.250:8890');
        socketGlobal = socket;
        socket.on("connect", function () {
            try {
                console.log("Connected to the server");
                if (username_ptt != null) {
                    excuteRequest('{"code":"add-user"}');
                    
                    setTimeout(function(){
                        if (window.location.href.endsWith("/phong-thi-thu")) {
                            excuteRequest('{"code":"CHECK_ROOM"}');
                        } else if (window.location.href.endsWith("/phong-thi-thu/phong-thi-cua-ban")) {
                            create_select2();
                            excuteRequest('{"code":"LIST_ROOM_PTCB"}');
                        } else if (window.location.href.endsWith("/phong-thi-thu/phong-thi-cua-ban-tao-phong")) {
                            //$('.countdown').ClassyCountdown({theme: "flat-colors-very-wide",end: $.now() + 10000});
                        } else if (window.location.href.endsWith("/phong-thi-thu/phong-thi-ao-danh-sach")) {
                            excuteRequest('{"code":"LIST_ROOM_PTA","level":1,"duration":0}');
                        } else if (window.location.href.endsWith("/phong-thi-thu/phong-thi-ao-lich-thi")) {
                            excuteRequest('{"code":"LIST_ROOM_PTA","level":1,"duration":6}');
                        }                    
                    }, 500);
                }
            } catch (ex) {
            }
        });

        socket.on('message', function (data) {
            try {
                var obj = JSON.parse(data);
                //alert(data);
                if (obj.code == 'MESSAGE') {
                    show_confirm('no_action()', '/img/img-post.jpg', 'Thông báo', obj.message, false);
                } else if (obj.code == 'LIST_ROOM_PTA') {
                    if(window.location.href.endsWith("/phong-thi-thu/phong-thi-ao-lich-thi"))
                        listRoomPTA(obj);
                    else
                        listRoomPTA2(obj);
                } else if (obj.code == 'LIST_ROOM_PTCB') {
                    listRoomPTCB(obj);
                } else if (obj.code == 'LEAVE_ROOM_OK') {
                    window.location.href = "/";
                } else if (obj.code == 'KICK_OUT_BY_ADMIN_OK') {
                    kickOutDialog(obj);
                } else if (obj.code == 'RECEIVE_INVITE') {
                    receiveInviteDialog(obj);
                } else if (obj.code == 'CREATE_ROOM_FAIL') {
                    window.location.href = "/";
                } else if (obj.code == 'LEAVE_ROOM_OK') {
                    window.location.href = "/";
                } else if (obj.code == 'IN_ROOM') {
                    if (obj.type == 1 && !obj.is_finish && window.location.href.endsWith("/phong-thi-thu/phong-thi-cua-ban-tao-phong")) {
                        $('.close').click();
                        setTimeout(function(){inRoomPTCB(obj, false); }, 700);                        
                    } else if (obj.type == 1 && obj.is_finish && window.location.href.endsWith("/phong-thi-thu/phong-thi-cua-ban-ket-qua")) {
                        ketquaPTCB(obj);
                    } else if (obj.type == 0 && !obj.is_finish && window.location.href.endsWith("/phong-thi-thu/phong-thi-ao-tao-phong")) {
                        $('.close').click();
                        setTimeout(function(){inRoomPTA(obj, false); }, 700);
                    } else if (obj.is_finish && window.location.href.endsWith("/phong-thi-thu/phong-thi-ao-ket-qua")) {
                        ketquaPTA(obj);
                    } else {//Khong phai link trong phong thi => chuyen sang link do
                        $('#phongthithu_form').attr('action', obj.type == 1 ? (!obj.is_finish ? '/phong-thi-thu/phong-thi-cua-ban-tao-phong' : '/phong-thi-thu/phong-thi-cua-ban-ket-qua')
                                : (!obj.is_finish ? '/phong-thi-thu/phong-thi-ao-tao-phong' : '/phong-thi-thu/phong-thi-ao-ket-qua'));
                        $('#phongthithu_hidden').val(data);
                        $('#phongthithu_form').submit();
                    }
                } else if (obj.code == 'START_TRUE') {
                    if (obj.type == 1 && window.location.href.endsWith("/phong-thi-thu/phong-thi-cua-ban-tao-phong")) {
                        $('.close').click();
                        setTimeout(function(){inRoomPTCB(obj, true); }, 700);
                    } else if (obj.type == 0 && window.location.href.endsWith("/phong-thi-thu/phong-thi-ao-tao-phong")) {
                        $('.close').click();
                        setTimeout(function(){inRoomPTA(obj, true); }, 700);
                    } else {//Khong phai link trong phong thi => chuyen sang link do
                        $('#phongthithu_form').attr('action', obj.type == 1 ? '/phong-thi-thu/phong-thi-cua-ban-tao-phong'
                                : '/phong-thi-thu/phong-thi-ao-tao-phong');
                        $('#phongthithu_hidden').val(data);
                        $('#phongthithu_form').submit();
                    }
                } else if (obj.code == 'NOP_BAI') {
                    finish_ques();
                }
            } catch (ex) {
            }
        });

    }

}

function show_confirm(action, avatar, title, mess, isConfirm) {
    $.notify({
        icon: avatar,//'/img/img-post.jpg',
        title: title, //'Lò Thị Thành',
        message: '<p class="mb-0">' + mess + '</p>'
               + '<p class="float-right mt-2 mb-0"><button type="button" data-notify="dismiss2" class="btn btn-sm badge-pill btn-success mr-1" onclick=' + action + ';><i class="fal fa-check-circle mr-1"></i>Đồng ý</button>'
               + (isConfirm ? '<button type="button" class="btn btn-sm badge-pill btn-danger" data-notify="dismiss2" ><i class="fal fa-ban mr-1"></i>Từ chối</button>' : '') + '</p>'
    }, {
        type: 'minimalist',
        animate: {
            enter: 'animated bounceIn',
            exit: 'animated bounceOut'
        },
        delay: 20000,
        icon_type: 'image',
        mouse_over: 'pause',
        template: '<div data-notify="container" class="col-11 col-md-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" id="alert_button_close" class="close" data-notify="dismiss">×</button>' +
                '<img data-notify="icon" class="rounded-circle float-left">' +
                '<span class="mb-0" data-notify="title">{1}</span>' +
                '<span data-notify="message">{2}</span>' +
                '</div>'
    });
}

function no_action() {}

function ketquaPTCB(obj) {
    try {
        if(obj.status == 3) {
            $('#li_countdown1').html('');
            $('#li_countdown1').remove();
            var html = '';
            for(var i = 0; i < obj.result.length; i++) {
                html += '<tr>';
                html += '<td>' + obj.result[i].stt + '</td>';
                html += '<td>' + obj.result[i].member_name + '</td>';
                html += '<td>' + obj.result[i].point + '</td>';
                html += '</tr>';
            }
            $('#tbody_bxh').html(html);

            $('#div_result_da').show();
        }
    } catch(ex) {}
}

function ketquaPTA(obj) {
    try {
        if(obj.status == 3) {
            $('#li_countdown1').html('');
            $('#li_countdown1').remove();
            var html = '';
            for(var i = 0; i < obj.result.length; i++) {
                html += '<tr>';
                html += '<td>' + obj.result[i].stt + '</td>';
                html += '<td>' + obj.result[i].member_name + '</td>';
                html += '<td>' + obj.result[i].point + '</td>';
                html += '</tr>';
            }
            $('#tbody_bxh').html(html);

            $('#div_result_da').show();
        }
    } catch(ex) {}
}

function leaveRoom() {
    try {
        var data = '{"code":"LEAVE_ROOM"}';
        excuteAjax(data);
    } catch(ex) {}
}

function finish_ques() {
    try {
        if ($('#input_max_question').val() == null) {
            var data2 = '{"code":"FINISH", "question":[]}';
            excuteAjax(data2);
            return;
        }
        var maxIndex = Number($('#input_max_question').val());
        var data = '{"code":"FINISH", "question":[';
        for (var i = 1; i <= maxIndex; i++) {
            if ($("input[name='inlineRadioOptions" + i + "']:checked").val() != null) {
                if (i > 1)
                    data += ',';
                data += '{"stt":' + i + ',"answer":"' + $("input[name='inlineRadioOptions" + i + "']:checked").val() + '"}';
            }
        }
        data += ']}';
        excuteAjax(data);
    } catch (ex) {
    }
}

function inRoomPTA(obj, isStart) {
    try {
        var html = '';
        $('#collapseThanhVien').html('');
        $('#collapseThanhVien').remove();
        
        for(var i = 0; i < obj.invite.length; i++) {
            if(i == 8) {
                html += '<div class="collapse" id="collapseThanhVien">';
            }
            html += '<a class="btn btn-sm btn-';
            html += obj.invite[i].is_member ? 'success text-white' : 'warning';
            html += ' mb-2" data-toggle="modal" data-target="#collapse';
            html += obj.invite[i].member;
            html += '">';
            html += '<i class="fas fa-user-graduate mr-2"></i>';
            html += obj.invite[i].member_name;
            html += '</a>&nbsp;';
            if(i == obj.invite.length - 1) {
                html += '</div>';
            }
        }
        $('#ptt_thi_sinh_tham_gia').html(html);
        $('#ptt_show_all').html(obj.invite.length >= 8 ? '<button type="button" data-toggle="collapse" data-target="#collapseThanhVien" class="btn btn-sm btn-outline-secondary btn-lg">Xem thêm ...</button>' : '');
        
        $('#strong_quantity').html(obj.quantity + '/' + obj.max_quantity);
        
        html = '';
        for(var i = 0; i < obj.invite.length; i++) {
            html += '<div class="modal fade" id="collapse' + obj.invite[i].member + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
            html += '    <div class="modal-dialog" role="document">';
            html += '        <div class="modal-content border-0 rounded-0">';
            html += '            <div class="modal-header">';
            html += '                <h5 class="modal-title" id="exampleModalLabel">';
            html += '                    <img class="w-75" src="/img/logo.png">';
            html += '                </h5>';
            html += '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
            html += '                    <span aria-hidden="true">&times;</span>';
            html += '                </button>';
            html += '            </div>';
            html += '            <div class="modal-body">';
            html += '                <div class="media">';
            html += '                    <div class="image-box rounded ratio-1-1 position-relative mr-3 w-25">';
            html += '                        <span href="javascript:void(0);" class="img-post" style="background-image:url(' + obj.invite[i].avatar + ');"></span>';
            html += '                    </div>';
            html += '                    <div class="media-body">';
            html += '                        <h5 class="mt-0 mb-2 font-weight-bold">' + obj.invite[i].member_name + '</h5>';
            html += '                        <p class="mb-0">Thành tích: ' + obj.invite[i].point + ' điểm</p>';
            html += '                        <p class="text-' + (obj.invite[i].is_member ? 'success' : 'danger') + ' mb-2">';
            html += '                            <i class="fal ' + (obj.invite[i].is_member ? 'fa-check-double' : 'fa-times-circle') + ' mr-2"></i>' + (obj.invite[i].is_member ? 'Đã chấp nhận lời mời vào phòng thi.' : 'Chưa chấp nhận lời thách đấu của bạn.') + ' ';
            html += '                        </p>';                     
            html += '                    </div>';
            html += '                </div>';
            html += '            </div>';
            html += '        </div>';
            html += '    </div>';
            html += '</div>';
        }
        $('#ptt_chi_tiet').html(html);
        
        if(isStart) {
            $('#ptt_p_batdau').hide();
            $('#ptt_div_clock').show();
            html = '';
            $('#countdown1').html('');
            $('#li_countdown1').html('');
            $('#li_countdown1').remove();
            $('#countdown2').html('');
            $('#countdown2').ClassyCountdown({theme: "flat-colors-very-wide",end: $.now() + obj.time});
            
            $('#divQuestionTotal').show();
            html = '';
            for(var i = 0; i < obj.question.length; i++) {
                html += '<div class="col-12 mb-4" id = "divQuestion' + obj.question[i].stt + '" ' + (obj.question[i].stt > 1000 ? 'style="display:none"' : '') + '>';
                html += '    <ul class="list-question list-group list-group-flush">';
                html += '        <li class="question list-group-item px-0">';
                if(obj.question[i].title.length > 0) {
                    html += '        <p class="content">';
                    html += obj.question[i].title;
                    html += '        </p>';
                }
                html += '            <p class="content">';
                html += '                <strong class="mr-3 badge badge-pill badge-dark">Question ' + obj.question[i].stt + '</strong>' + obj.question[i].content;
                html += '            </p>';
                html += '            <div class="row mx-0">';
                html += '                <div class="col-6 col-md-3 form-check form-check-inline mr-0">';
                html += '                    <input class="form-check-input" type="radio" name="inlineRadioOptions' + obj.question[i].stt + '" id="inlineRadio1_' + obj.question[i].stt + '" value="' + obj.question[i].answer1 + '">';
                html += '                    <label class="form-check-label font-weight-bold2" for="inlineRadio1_' + obj.question[i].stt + '">' + obj.question[i].answer1 + '</label>';
                html += '                </div>';
                html += '                <div class="col-6 col-md-3 form-check form-check-inline mr-0">';
                html += '                    <input class="form-check-input" type="radio" name="inlineRadioOptions' + obj.question[i].stt + '" id="inlineRadio2_' + obj.question[i].stt + '" value="' + obj.question[i].answer2 + '">';
                html += '                    <label class="form-check-label font-weight-bold2" for="inlineRadio2_' + obj.question[i].stt + '">' + obj.question[i].answer2 + '</label>';
                html += '                </div>';
                html += '                <div class="col-6 col-md-3 form-check form-check-inline mr-0">';
                html += '                    <input class="form-check-input" type="radio" name="inlineRadioOptions' + obj.question[i].stt + '" id="inlineRadio3_' + obj.question[i].stt + '" value="' + obj.question[i].answer3 + '">';
                html += '                    <label class="form-check-label font-weight-bold2" for="inlineRadio3_' + obj.question[i].stt + '">' + obj.question[i].answer3 + '</label>';
                html += '                </div>';
                html += '                <div class="col-6 col-md-3 form-check form-check-inline mr-0">';
                html += '                    <input class="form-check-input" type="radio" name="inlineRadioOptions' + obj.question[i].stt + '" id="inlineRadio4_' + obj.question[i].stt + '" value="' + obj.question[i].answer4 + '">';
                html += '                    <label class="form-check-label font-weight-bold2" for="inlineRadio4_' + obj.question[i].stt + '">' + obj.question[i].answer4 + '</label>';
                html += '                </div>';
                html += '            </div>';
                html += '        </li>';
                html += '    </ul>';
                html += '</div>';
            }
            $('#divQuestionTotal').html(html);
            
            $('#input_max_question').val(obj.question.length);
        }
    } catch(ex) {}
}

function inRoomPTCB(obj, isStart) {
    try {
        var html = '';
        $('#collapseThanhVien').html('');
        $('#collapseThanhVien').remove();
        
        for(var i = 0; i < obj.invite.length; i++) {
            if(i == 8) {
                html += '<div class="collapse" id="collapseThanhVien">';
            }
            html += '<a class="btn btn-sm btn-';
            html += obj.invite[i].is_member ? 'success text-white' : 'warning';
            html += ' mb-2" data-toggle="modal" data-target="#collapse';
            html += obj.invite[i].member;
            html += '">';
            html += '<i class="fas fa-user-graduate mr-2"></i>';
            html += obj.invite[i].member_name;
            html += '</a>&nbsp;';
            if(i == obj.invite.length - 1) {
                html += '</div>';
            }
        }
        $('#ptt_thi_sinh_tham_gia').html(html);
        $('#ptt_show_all').html(obj.invite.length >= 8 ? '<button type="button" data-toggle="collapse" data-target="#collapseThanhVien" class="btn btn-sm btn-outline-secondary btn-lg">Xem thêm ...</button>' : '');
        
        $('#strong_quantity').html(obj.quantity);
        
        html = '';
        for(var i = 0; i < obj.invite.length; i++) {
            html += '<div class="modal fade" id="collapse' + obj.invite[i].member + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
            html += '    <div class="modal-dialog" role="document">';
            html += '        <div class="modal-content border-0 rounded-0">';
            html += '            <div class="modal-header">';
            html += '                <h5 class="modal-title" id="exampleModalLabel">';
            html += '                    <img class="w-75" src="/img/logo.png">';
            html += '                </h5>';
            html += '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
            html += '                    <span aria-hidden="true">&times;</span>';
            html += '                </button>';
            html += '            </div>';
            html += '            <div class="modal-body">';
            html += '                <div class="media">';
            html += '                    <div class="image-box rounded ratio-1-1 position-relative mr-3 w-25">';
            html += '                        <span href="javascript:void(0);" class="img-post" style="background-image:url(' + obj.invite[i].avatar + ');"></span>';
            html += '                    </div>';
            html += '                    <div class="media-body">';
            html += '                        <h5 class="mt-0 mb-2 font-weight-bold">' + obj.invite[i].member_name + '</h5>';
            html += '                        <p class="mb-0">Thành tích: ' + obj.invite[i].point + ' điểm</p>';
            html += '                        <p class="text-' + (obj.invite[i].is_member ? 'success' : 'danger') + ' mb-2">';
            html += '                            <i class="fal ' + (obj.invite[i].is_member ? 'fa-check-double' : 'fa-times-circle') + ' mr-2"></i>' + (obj.create_member == obj.invite[i].member ? 'Chủ phòng' :  (obj.invite[i].is_member ? 'Đã chấp nhận lời mời vào phòng thi.' : 'Chưa chấp nhận lời thách đấu của bạn.')) + ' ';
            html += '                        </p>';           
            html += '                        <p class="p_kickout_moilai">';
            if(obj.status == 1 && obj.create_member == obj.member && obj.member != obj.invite[i].member) {
                html += '                            <button type="button" onclick="' + (obj.invite[i].is_member ? (obj.create_member == obj.member && obj.member == obj.invite[i].member ? 'confirmKickOut(\'' + obj.invite[i].member + '\');' : 'kickOut(\'' + obj.invite[i].member + '\');') : 'invitePlay(\'' + obj.invite[i].member + '\');') + '" class="btn btn-sm btn-' + (obj.invite[i].is_member ? 'danger' : 'blue') + '" data-dismiss="modal"><i class="fal fa-times mr-2"></i>' + (obj.invite[i].is_member ? (obj.member == obj.invite[i].member ? 'Thoát' : 'Kick out') : 'Mời lại') + '</button>';
            }
            html += '                        </p>';          
            html += '                    </div>';
            html += '                </div>';
            html += '            </div>';
            html += '        </div>';
            html += '    </div>';
            html += '</div>';
        }
        $('#ptt_chi_tiet').html(html);
        
        if(isStart) {
            $('.p_kickout_moilai').html('');
            $('#ptt_p_batdau').hide();
            $('#ptt_div_clock').show();
            html = '';
            $('.countdown').html('');
            $('.countdown').ClassyCountdown({theme: "flat-colors-very-wide",end: $.now() + obj.time});
            
            $('#divQuestionTotal').show();
            html = '';
            for(var i = 0; i < obj.question.length; i++) {
                html += '<div class="col-12 mb-4" id = "divQuestion' + obj.question[i].stt + '" ' + (obj.question[i].stt > 1000 ? 'style="display:none"' : '') + '>';
                html += '    <ul class="list-question list-group list-group-flush">';
                html += '        <li class="question list-group-item px-0">';
                if(obj.question[i].title.length > 0) {
                    html += '        <p class="content">';
                    html += obj.question[i].title;
                    html += '        </p>';
                }
                html += '            <p class="content">';
                html += '                <strong class="mr-3 badge badge-pill badge-dark">Question ' + obj.question[i].stt + '</strong>' + obj.question[i].content;
                html += '            </p>';
                html += '            <div class="row mx-0">';
                html += '                <div class="col-6 col-md-3 form-check form-check-inline mr-0">';
                html += '                    <input class="form-check-input" type="radio" name="inlineRadioOptions' + obj.question[i].stt + '" id="inlineRadio1_' + obj.question[i].stt + '" value="' + obj.question[i].answer1 + '">';
                html += '                    <label class="form-check-label font-weight-bold2" for="inlineRadio1_' + obj.question[i].stt + '">' + obj.question[i].answer1 + '</label>';
                html += '                </div>';
                html += '                <div class="col-6 col-md-3 form-check form-check-inline mr-0">';
                html += '                    <input class="form-check-input" type="radio" name="inlineRadioOptions' + obj.question[i].stt + '" id="inlineRadio2_' + obj.question[i].stt + '" value="' + obj.question[i].answer2 + '">';
                html += '                    <label class="form-check-label font-weight-bold2" for="inlineRadio2_' + obj.question[i].stt + '">' + obj.question[i].answer2 + '</label>';
                html += '                </div>';
                html += '                <div class="col-6 col-md-3 form-check form-check-inline mr-0">';
                html += '                    <input class="form-check-input" type="radio" name="inlineRadioOptions' + obj.question[i].stt + '" id="inlineRadio3_' + obj.question[i].stt + '" value="' + obj.question[i].answer3 + '">';
                html += '                    <label class="form-check-label font-weight-bold2" for="inlineRadio3_' + obj.question[i].stt + '">' + obj.question[i].answer3 + '</label>';
                html += '                </div>';
                html += '                <div class="col-6 col-md-3 form-check form-check-inline mr-0">';
                html += '                    <input class="form-check-input" type="radio" name="inlineRadioOptions' + obj.question[i].stt + '" id="inlineRadio4_' + obj.question[i].stt + '" value="' + obj.question[i].answer4 + '">';
                html += '                    <label class="form-check-label font-weight-bold2" for="inlineRadio4_' + obj.question[i].stt + '">' + obj.question[i].answer4 + '</label>';
                html += '                </div>';
                html += '            </div>';
                html += '        </li>';
                html += '    </ul>';
                html += '</div>';
            }
            $('#divQuestionTotal').html(html);
            
            $('#input_max_question').val(obj.question.length);
        }
    } catch(ex) {}
}

function listRoomPTCB(obj) {
    try {
        var html = '';
        for(var i = 0; i < obj.room.length; i++) {
            html += '<li class="list-group-item px-0">';
            html += '<a class="text-success font-weight-bold d-block w-100" href="javascript:joinRoom(\'' + obj.room[i].room_id + '\');">';
            html += obj.room[i].room_name;
            html += '<span class="badge badge-';
            html += obj.room[i].level == 1 ? 'success' : (obj.room[i].level == 2 ? 'primary' : 'danger');
            html += ' float-right mt-1 ml-3">';
            html += obj.room[i].level == 1 ? 'Dễ' : (obj.room[i].level == 2 ? 'TB' : 'Khó');
            html += '</span></a></li>';
        }
        $('#ptcb_ul_list_phong_thi').html(html);
    } catch(ex){}
}

function joinRoom(room_id) {
    try {
        var data = '{"code":"JOIN_ROOM","room_id":"' + room_id + '"}';
        excuteAjax(data);
    } catch (ex) {
    }
}

function listRoomPTA(obj) {
    try {
        var html = '';
        for(var i = 0; i < obj.room.length; i++) {
            html += '<div class="col-6 col-md-3">';
            html += '    <div class="card rounded-0 border-0 mb-4">';
            html += '        <div class="image-box ratio-16-9 position-relative">';
            html += '            <a href="javascript:void(0);" class="img-post" style="background-image:url(' + obj.room[i].avatar.split(" ").join("%20") + ');"></a>';
            html += '        </div>';
            html += '        <div class="card-body px-0">';
            html += '            <h5 class="card-title mb-1">';
            html += '                <a class="d-block w-100" href="javascript:void(0);">' + obj.room[i].room_name + '</a>';
            html += '            </h5>';
            //html += obj.room[i].current_member + '/' + obj.room[i].max_member + ' thí sinh</br>';
            html += 'Ngày thi: ' + obj.room[i].start_time;
            html += '        </div>';
            html += '    </div>';
            html += '</div>';
        }
        $('#pta_div_list_phong_thi').html(html);
    } catch(ex){}
}

function listRoomPTA2(obj) {
    try {
        var html = '';
        for(var i = 0; i < obj.room.length; i++) {
            html += '<div class="col-6 col-md-3">';
            html += '    <div class="card rounded-0 border-0 mb-4">';
            html += '        <div class="image-box ratio-16-9 position-relative">';
            html += '            <a href="javascript:joinRoom(\'' + obj.room[i].room_id + '\');" class="img-post" style="background-image:url(' + obj.room[i].avatar.split(" ").join("%20") + ');"></a>';
            html += '        </div>';
            html += '        <div class="card-body px-0">';
            html += '            <h5 class="card-title mb-1">';
            html += '                <a class="d-block w-100" href="javascript:joinRoom(\'' + obj.room[i].room_id + '\');">' + obj.room[i].room_name + '</a>';
            html += '            </h5>';
            html += obj.room[i].current_member + '/' + obj.room[i].max_member + ' thí sinh';
            html += '        </div>';
            html += '    </div>';
            html += '</div>';
        }
        $('#pta_div_list_phong_thi').html(html);
    } catch(ex){}
}

function kickOutDialog(obj) {
    try {
        show_confirm('no_action()', obj.kickout_avata, obj.kickout_name, obj.message, false);
        setTimeout(function(){ window.location.href = "/"; }, 2000);
    } catch(ex){}
}

function receiveInviteDialog(obj) {
    try {
        show_confirm('receiveInviteAccept("' + obj.action_id + '")', obj.invite_avatar, obj.invite_name, obj.message, true);
    } catch(ex){}
}

function receiveInviteAccept(action_id) {
    try {          
        excuteAjax('{"code":"INVITE_PLAY_ACCEPT","action_id":"' + action_id + '"}');
    } catch(ex){}
}

function excuteRequest(data) {
    var CSRF_TOKEN = $('meta[name="ajax-csrf-token"]').attr('content');
    $.ajax({
        type: "POST",
        url: '/phong-thi-thu/request',
        sync: true,
        data: {_token: CSRF_TOKEN, data: data},
        dataType: 'JSON',
        success: function(msg) {
            try {
                if(socketGlobal != null)
                    socketGlobal.emit(msg.action, msg.data);
            } catch(ex) {}
        }
    });
}

function excuteResponse(data) {
    var CSRF_TOKEN = $('meta[name="ajax-csrf-token"]').attr('content');
    $.ajax({
        type: "POST",
        url: '/phong-thi-thu/response',
        sync: true,
        data: {_token: CSRF_TOKEN, data: data},
        dataType: 'JSON',
        success: function(msg) {
            try {
                if(socketGlobal != null)
                    socketGlobal.emit(msg.action, JSON.stringify(msg));
            } catch(ex) {}
        }
    });
}

function excuteAjax(data) {
    try {
        excuteRequest(data);
    } catch(ex) {}
}

function create_select2() {
    $("#room_invite").select2({
        language: {
            inputTooShort: function (args) {
                return "Nhập tên user, SĐT hoặc email";//"Vui lòng gõ ít nhất ba ký tự";
            },
            inputTooLong: function (args) {
                return "You typed too much";
            },
            errorLoading: function () {
                return "Error loading results";
            },
            loadingMore: function () {
                return "Loading more results";
            },
            noResults: function () {
                return "Không tìm thấy dữ liệu";
            },
            searching: function () {
                return "Đang tìm kiếm...";
            },
            maximumSelected: function (args) {
                // args.maximum is the maximum number of items the user may select
                return "Error loading results";
            }
        },

        multiple: true,
        ajax: {
            url: "/phong-thi-thu/ajax",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
                return {
                    results: data.result
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 3,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection

    });
}