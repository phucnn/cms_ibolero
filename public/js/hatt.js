var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


$(document).ready(function () {
    defaultJs();
    subrisubscribeModalForm();
    loginModal();
    buttonDrop();
    buttonResetFrom();
    acceptedFriendOk();
    acceptedFriendCancel();
    readAllNotify();
});

function acceptedFriendCancel() {
    $(document).on('click', '.acceptedFriendCancel', function (e) {
        var remote = $(this).data('remote');
        var element = $(this);
        $.ajax({
            type: "POST",
            url: remote,
            data: {_token: CSRF_TOKEN},
            success: function (json) {
                element.closest('p').remove();
                alert(json.message);

            },
        });
    })
}

function acceptedFriendOk() {
    $(document).on('click', '.acceptedFriendOk', function (e) {
        var remote = $(this).data('remote');
        var element = $(this);
        $.ajax({
            type: "POST",
            url: remote,
            data: {_token: CSRF_TOKEN},
            success: function (json) {
                element.closest('p').remove();
                alert(json.message);
            },
        });
    })
}

function buttonDrop() {
    // $(".nav-vocabulary .disabled").draggable('disable');
    $('.nav-vocabulary li').draggable({
        revert: true,
        helper: 'clone'
    });
    $(".textbox").droppable({
        drop: function (event, ui) {
            $(ui.draggable).addClass('disabled');
            // $(".nav-vocabulary .disabled").draggable('disable');
            this.value = $(ui.draggable).data('word');
        }
    });
}


function buttonAudio(text) {
    event.preventDefault();

    if (responsiveVoice.voiceSupport()) {
        responsiveVoice.speak(text);
    }
}


function loginModal() {
    $("#form-login-modal").on("submit", function (event) {
        event.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (json) {
                if (json.auth == true) {
                    window.location.href = json.redirect;
                }
            },
            error: function (json) {
                console.log(json);
                var errors = json.responseJSON;
                $("#form-login-modal .form-login-alert").html('');
                $.each(errors.errors, function (key, value) {
                    $("#form-login-modal .form-login-alert").append("<p>" + value + "</p>");
                })
                $("#form-login-modal .form-login-alert").removeClass('d-none');
                setTimeout(function () {
                    $("#form-login-modal .form-login-alert").addClass('d-none');
                }, 2000)
            }
        });
    })

}

function buttonResetFrom() {
    $('button[type="reset"]').click(function () {
        form = $(this).closest('form');
        form.find(".nav-vocabulary .disabled").draggable('enable');
        form.find('.nav-vocabulary .disabled').removeClass('disabled ui-draggable-disabled');
    })
}

function subrisubscribeModalForm() {
    $("#subscribeModalForm").on("submit", function (event) {
        event.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: '/subscribe',
            data: data,
            success: function (msg) {
                $('#subscribeModalForm input[type="email"]').val('');
                if (typeof msg.success !== "undefined") {
                    $('#subscribeModalContent').addClass('d-none');
                    $('#subscribeModalSuccess').removeClass('d-none');
                    setTimeout(function () {
                        $('#subscribeModal').modal('toggle');
                        $('#subscribeModalContent').removeClass('d-none');
                        $('#subscribeModalSuccess').addClass('d-none');
                    }, 2000)
                } else {
                    $('.subscribeModalMessage').html(msg.error);
                    setTimeout(function () {
                        $('.subscribeModalMessage').html('')
                    }, 3000)
                }
            }
        });
    })

}

function defaultJs() {
    //toTop
    $('.to-top').toTop({
        //options with default values
        autohide: true,
        offset: 500,
        speed: 500,
        position: true,
        right: 0,
        bottom: 30
    });

//tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

//wow-js
    var wow = new WOW(
        {
            boxClass: 'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0,          // distance to the element when triggering the animation (default is 0)
            mobile: true,       // trigger animations on mobile devices (default is true)
            live: true,       // act on asynchronously loaded content (default is true)
            callback: function (box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null // optional scroll container selector, otherwise use window
        }
    );
    wow.init();


// owl-carousel
    $('#index-slide').owlCarousel({
        loop: true,
        margin: 10,
        animateIn: 'bounceInUp',
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        lazyLoad: true,
        items: 1
    })
// owl-carousel
    $('#expert-slide').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        animateIn: 'flipInY',
        autoplay: true,
        autoplayTimeout: 8000,
        autoplayHoverPause: true,
        lazyLoad: true,
        items: 1
    })
// owl-carousel
    $('#featured-post-slide').owlCarousel({
        loop: true,
        margin: 30,
        dots: true,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        lazyLoad: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                margin: 10,
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    })
// owl-carousel
    $('#yktv-slide').owlCarousel({
        loop: true,
        margin: 50,
        dots: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        lazyLoad: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    })
// owl-carousel
    $('#dkkh-slide').owlCarousel({
        loop: true,
        margin: 50,
        dots: true,
        lazyLoad: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3,
                loop: false,
            }
        }
    })
}

function readAllNotify() {
    $(document).on('click', '.readAllNotifiy', function (e) {
        event.preventDefault();
        $(this).find('.badge').remove();
        $.ajax({
            type: "POST",
            url: '/read-all-notify',
            data: {_token: CSRF_TOKEN},
            success: function (json) {
                console.log(json.status);
                if (json.status == 1) {
                    $('#readAllNotifications .modal-body').html(json.list);
                    $('#readAllNotifications').modal('show');
                } else {
                    console.log(json.message);
                }
            }
        });
    })

}