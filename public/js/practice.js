function moveToExerciseMenu(id) {
    hideAllExercise();
    showExercise(id);
    updateMoveToExerciseMenu(id)

}

function nextExerciseButton(id) {
    hideAllExercise();
    showExercise(id);
    updateMoveToExerciseMenu(id)

}

function prevExerciseButton(id) {
    hideAllExercise();
    showExercise(id);
    updateMoveToExerciseMenu(id)
}

function hideAllExercise() {
    $('#list-exercise .is_exercise').addClass('d-none');
}

function updateMoveToExerciseMenu(id) {
    $('#moveToExerciseMenu li').removeClass('active');
    $('#moveToExerciseMenu li[data-exercise="' + id + '"]').addClass('active')

}

function showExercise(id) {
    $('#' + id).removeClass('d-none');
}

$(document).ready(function () {

    $(document).on('click', '.moveToExerciseMenu', function () {
        var div_exercise = $(this).closest('li').data('exercise');
        moveToExerciseMenu(div_exercise);
    })

    $(document).on('click', '.prev-exercise', function () {
        var div_exercise = $(this).data('exercise');
        prevExerciseButton(div_exercise);
    })

    $(document).on('click', '.next-exercise', function () {
        var div_exercise = $(this).data('exercise');
        nextExerciseButton(div_exercise);
    })

    $(document).on('click', '.nghe_doc_lai_tu_next', function () {
        console.log('click nghe_doc_lai_tu_next');
        parent = $(this).closest('.nghe_doc_lai_tu_item');
        parent.parent().find('.nghe_doc_lai_tu_item').addClass('d-none');
        parent.next('.nghe_doc_lai_tu_item').removeClass('d-none')
    });
    //ghi am

    var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
    var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
    var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;
    var recognition = new SpeechRecognition();
    var recognitionStatus = '';
    recognition.lang = 'en-US';

    var lastQues;
    var recorderStream = false;
    var luyennoi_true = true;
    var chunks = [];
    var mediaRecorder;
    //dang bai nghe va doc lai tu

    $(document).on('click', '.nghe-doc-lai-tu-record', function () {
        recognition.start();
        inputTarget = $(this).data('input');
        aduioTarget = $(this).data('audio');
        buttonRecord = $(this);
        console.log(inputTarget);
        console.log(aduioTarget);
        type = 'nghe_doc_lai_tu';
        if (navigator.mediaDevices.getUserMedia) {
            var constraints = {audio: true};
            var constraints = { audio: {sampleRate:48000, channelCount: 1, volume: 1.0 }, video:false };
            var chunks = [];
            var onSuccess = function (stream) {
                buttonRecord.html('<i class="fal fa-microphone-alt mr-2"></i> Đang ghi âm');
                mediaRecorder = new MediaRecorder(stream);
                mediaRecorder.start();
                // console.log(mediaRecorder.state);
                // console.log("recorder started");
                mediaRecorder.onstop = function (e) {
                    buttonRecord.html('<i class="fal fa-microphone-alt mr-2"></i> Ghi âm');

                    // console.log("data available after MediaRecorder.stop() called.");
                    var blob = new Blob(chunks, {'type': 'audio/ogg; codecs="vorbis"'});
                    chunks = [];
                    var audioURL = window.URL.createObjectURL(blob);
                    $('#'+aduioTarget).attr('src',audioURL);
                    if (stream) {
                        stream.getAudioTracks().forEach(function (track) {
                            track.stop();
                        });
                        stream = null;
                    }
                }

                mediaRecorder.ondataavailable = function (e) {
                    chunks.push(e.data);
                }
            }

            var onError = function (err) {
                console.log('The following error occured: ' + err);
            }

            navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onError);

        }
    });

    recognition.onresult = function (event) {
        var last = event.results.length - 1;
        ans = event.results[last][0].transcript;
        recognition.stop();
        mediaRecorder.stop();
        $('#' + inputTarget).val(ans);
        if (type == 'nghe_doc_lai_tu') {
            // $('.nghe_doc_lai_tu_next').trigger('click');
            $('#'+inputTarget).closest('.card-body').find('.nghe-doc-lai-tu-replay').removeClass('d-none')
            $('#'+inputTarget).removeClass('d-none')

        }
    };
    recognition.onerror = function (event) {
        console.log(event.error);
        console.log(event.timeStamp - start_timestamp);
        if (event.error == 'no-speech') {
            alert('Không bắt được âm thanh. Hãy thử điều chỉnh các xác lập microphone trên máy tính của bạn.');
        }
        if (event.error == 'audio-capture') {
            alert('Yêu cầu nhận diện microphone đã bị từ chối. Hãy đảm bảo microphone đã được cài đặt và xác lập thành công trên máy tính của bạn.');
        }
        if (event.error == 'not-allowed') {
            if (event.timeStamp - start_timestamp < 100) {
                alert('Chưa nhận diện được microphone. Hãy đảm bảo microphone đã được cài đặt và xác lập thành công trên máy tính của bạn.')
            } else {
                alert('Yêu cầu nhận diện microphone đã bị từ chối. Hãy đảm bảo microphone đã được cài đặt và xác lập thành công trên máy tính của bạn.')
            }
        } else {
            alert('Không bắt được âm thanh');
        }

    }

    recognition.onspeechend = function () {
        recognition.stop();
        if (recorderStream) {
            recorderStream.stop();
        }
    }


    //submit form bai tap
    submitFormExecise();

});
function submitFormExecise() {
    $(".form-exercise").on("submit", function (event) {
        event.preventDefault();
        var form = $(this);
        var data = $(this).serialize();
        var url = $(this).attr('action');
        var id = $(this).find("input[name='id-submit-exercise']").val();
        var token = $(this).find("input[name='token-form']").val();
        var href_page_result = $('#page-ket-qua').attr('href');
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (json) {
                form.find('button[type="submit"]').remove();
                form.find('button[type="reset"]').remove();
                // form.find('.form-submit-content').html(json.html);
                form.find('.form-submit-content').each(function(e,i){
                    $(this).html(json.html[e])
                })
                if(json.ex_not_done==0){
                    setTimeout(function () {
                        window.location.href=href_page_result;
                    },2000)
                }
            },
            error: function (json) {
                if (json.status == 500) {
                    alert('Vui long thử lại. hoặc F5 lại trang');
                } else {
                    errors = json.responseJSON;
                    alert(errors.message)

                }
            },
        });
    })
}

function ngheDocLaiTuReplay(e) {
    $('#'+e).get(0).play();
}