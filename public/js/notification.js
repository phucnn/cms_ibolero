var messaging = firebase.messaging();

messaging.usePublicVapidKey("BC2ZlfeI3P7o_n1o00DxxyQbMT8-gkizKsn3pDN0FCMknteDo1QLkXVfHsJqexLzCzzj4jArETJDSZIEsE8cVV4");

messaging.requestPermission()
    .then(function () {
        console.log('Notification permission granted.');
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
    })
    .catch(function (err) {
        if (err.code = 'messaging/notifications-blocked') {
            // $('#allowNotifications').modal('toggle')
        }
        console.log('Unable to get permission to notify.', err);
    });


messaging.onMessage(function (payload) {
    console.log('onMessage', payload);
    notificationTitle = payload.notification.title,
        notificationOptions = {
            body: payload.notification.body,
            icon: payload.notification.icon,
            click_action: payload.notification.click_action
        };
    var notification = new Notification(notificationTitle, notificationOptions);
    notification.onclick = function (event) {
        event.preventDefault(); // prevent the browser from focusing the Notification's tab
        window.open(payload.notification.click_action, '_blank');
        notification.close();
    }

});


resetUI();

function resetUI() {
    messaging.getToken()
        .then(function (currentToken) {
            if (currentToken) {
                sendTokenToServer(currentToken);
            } else {
                // Show permission request.
                console.log('No Instance ID token available. Request permission to generate one.');
            }
        })
        .catch(function (err) {
            console.log('An error occurred while retrieving token. ', err);
        });
    messaging.onTokenRefresh(function () {
        messaging.getToken().then(function (refreshedToken) {
            console.log('Token refreshed.');
            // Indicate that the new Instance ID token has not yet been sent to the
            // app server.
            // Send Instance ID token to app server.
            sendTokenToServer(refreshedToken);
            // ...
        }).catch(function (err) {
            console.log('Unable to retrieve refreshed token ', err);
        });
    });
}

function sendTokenToServer(currentToken) {
    console.log(currentToken);
    $.ajax({
        method: "GET",
        url: "/user/save-token-user-firebase",
        data: {tokenDevice: currentToken}
    }).done(function (msg) {
        if (msg == "1") {

        }
    });
}
