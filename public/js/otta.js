//toTop
$('.to-top').toTop({
      //options with default values
      autohide: true,
      offset: 500,
      speed: 500,
      position: true,
      right: 0,
      bottom: 30
  });

//tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

//wow-js
var wow = new WOW(
  {
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {
      // the callback is fired every time an animation is started
      // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
  }
);
wow.init();


// owl-carousel
$('#index-slide').owlCarousel({
    loop:true,
    margin:10,
    animateIn: 'bounceInUp',
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    lazyLoad:true,
    items:1
})
// owl-carousel
$('#expert-slide').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots: false,
    animateIn: 'flipInY',
    autoplay:true,
    autoplayTimeout:8000,
    autoplayHoverPause:true,
    lazyLoad:true,
    items:1
})
// owl-carousel
$('#featured-post-slide').owlCarousel({
    loop:true,
    margin:30,
    dots: true,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    lazyLoad:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            margin:10,
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})
// owl-carousel
$('#yktv-slide').owlCarousel({
    loop:true,
    margin:50,
    dots: true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    lazyLoad:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})
// owl-carousel
$('#dkkh-slide').owlCarousel({
    loop:true,
    margin:50,
    dots: true,
    lazyLoad:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3,
            loop:false,
        }
    }
})
// Kéo thả
$('.nav-vocabulary li').draggable({
    revert: true,
    helper: 'clone'
});

$(".textbox").droppable({
    drop: function (event, ui) {
        this.value = $(ui.draggable).attr('id');
    }
});