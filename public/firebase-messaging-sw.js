importScripts('https://www.gstatic.com/firebasejs/5.1.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.1.0/firebase-messaging.js');
// importScripts('https://www.gstatic.com/firebasejs/5.1.0/firebase.js');
var config = {
    apiKey: "AIzaSyCNqXf8DPEa5R14sWdbXWE3nhcXRt_aEY4",
    authDomain: "onthitienganh-83ce8.firebaseapp.com",
    databaseURL: "https://onthitienganh-83ce8.firebaseio.com",
    projectId: "onthitienganh-83ce8",
    storageBucket: "us-central.appspot.com",
    messagingSenderId: "231010546525",
};
firebase.initializeApp(config);
var messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    // console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    var notificationTitle = payload.notification.title;
    var notificationOptions = {
        body: payload.notification.body,
        icon: payload.notification.icon,

    };
    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});


// self.addEventListener('notificationclick', function(event) {
//     event.notification.close();
//     event.waitUntil(clients.matchAll({
//         type: "window"
//     }).then(function(clientList) {
//         for (var i = 0; i < clientList.length; i++) {
//             var client = clientList[i];
//             if (client.url == '/' && 'focus' in client)
//                 return client.focus();
//         }
//         if (clients.openWindow)
//             return clients.openWindow('/');
//     }));
// });