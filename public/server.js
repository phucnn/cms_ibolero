var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var redis = require('redis');
var users = {};
var channels = {};
var net = require('net');
var messages = {};
var messTong = '';


var MongoClient = require('mongodb').MongoClient;
var dboMongo = null;

MongoClient.connect("mongodb://192.168.100.250:27017/", function(err, db) {
  if (err) {
	  return;
  }
  dboMongo = db.db("luyenthitienganh");
});


server.listen(8890);

var HOST = '127.0.0.1';
var PORT = 8891;
var client = new net.Socket();
client.connect(PORT, HOST, function () {
    console.log('CONNECTED TO: ' + HOST + ':' + PORT);
    //client.write('I am Chuck Norris!\0');
});

client.on('data', function (data) {
    try {
        console.log('------------------------DATA: ' + (data + "").substring(0,100));
        messTong += data + "";
        if(messTong.endsWith('!!@@!!')) {
            var dataSplit = messTong.split('##@@##');
            messTong = '';
            for (var loop = 0; loop < dataSplit.length; loop++) {
                if (dataSplit[loop].length > 10) {
                    dataSplit[loop] = dataSplit[loop].replace('!!@@!!','');
                    //console.log('------------------------dataSplit[' + loop + ']: ' + dataSplit[loop].substring(0,100));
                    var obj = JSON.parse(dataSplit[loop]);
                    //console.log('------------------------obj[' + loop + ']: ' + obj.member);
                    if (obj.member in users) {
                        console.log('emit for ' + obj.member);
                        users[obj.member].emit("message", dataSplit[loop] + "");
                    } else if(obj.code == 'RECEIVE_INVITE'){//request nhan yeu cau moi choi moi luu
                        var mess = messages[obj.member];
                        if(mess == null) {
                            mess = [];
                        }
                        mess[mess.length] = dataSplit[loop] + "";
                        messages[obj.member] = mess;
                    }
                }
            }
        }
    } catch (ex) {
        console.log(ex);
    }
});

//app.get('/sendMess/:data', function (req, res) {
//    try {
//        var data = req.params.data;
//        console.log('testMESS: ' + data);
        //var obj = JSON.parse(data);
        //console.log('testMESS: ' + obj.member);
        //users[obj.member].emit("message", data);
//        res.end(data);
//    } catch (ex) {
//        console.log(ex);
//        try {
//            res.end(data);
//        } catch (ex) {
//        }
//    }
//    return;
//});

io.on('connection', function (socket) {

    console.log(new Date() + " => new client connected");
    //var redisClient = redis.createClient();
    var redisClient = redis.createClient(6379, '127.0.0.1');
    redisClient.auth('123456$');

    redisClient.subscribe('message');

    redisClient.on("message", function (channel, message) {
        try {
            var username = socket.nickname;
            console.log(username + " => new message in queue: " + message);
            var obj = JSON.parse(message);
            if (username == obj.member) {
                if (obj.request != null && obj.request == 1) {
                    client.write(message + '\0');
                } else {
                    socket.emit(channel, message);
                    users[socket.nickname].emit(channel, message);
                }
            }
        } catch (ex) {
        }
    });

    socket.on('request', function (data) {
        try {
			data = Base64.decode(data);
            console.log('request: ' + data.substring(0,100));
            var obj = JSON.parse(data);
            if (socket.nickname == obj.member) {
                client.write(data + '\0');
            }
        } catch (ex) {
        }
    });

    socket.on('new-user', function (data) {
        try {
			data = Base64.decode(data);
			console.log('new user: ' + data);
			var obj = JSON.parse(data);
            			
			var query = { username: obj.member };
			dboMongo.collection("users").find(query).toArray(function(err, result) {
				if (err) {
					return;
				}
				if(result.length > 0) {
					var name = result[0].username;
					var token = result[0].remember_token;
					if(token == obj.remember_token) {
						socket.nickname = name;
						users[socket.nickname] = socket;
						console.log('add nickName: ' + name);
						if(name in messages) {
							var mess = messages[name];
							for(var i = 0; i < mess.length; i++) {
								users[name].emit("message", mess[i]);
							}
							delete messages[name];
						}
					}
					return;
				}
				//db.close();
			});
			
        } catch (ex) {
        }
    });

    socket.on('disconnect', function () {
        try {
            if (!socket.nickname)
                return;
            console.log('disconnect: ' + socket.nickname);
            delete users[socket.nickname];
            redisClient.quit();
        } catch (ex) {
        }
    });

});