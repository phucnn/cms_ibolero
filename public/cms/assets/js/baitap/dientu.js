

//Điền từ
function addAnswerDientu(obj) {
    var count = obj.closest('.box').find('.dapan-dientu').data('count') + 1;
    $('.dapan-dientu').data('count', count);
    var count_true = obj.closest('.box').find('.dapan-dientu div').length + 1;
    var eqQuestion = obj.closest('.box').data('eq');
    var html = '<div class="input-group remove-dap-an">\n' +
        '                            <span class="input-group-addon"><i class="fa fa-arrows-v"></i></span>\n' +
        '                            <input type="text" class="form-control" name="exercise[' + eqQuestion + '][answer][' + count + ']">\n' +
        '                            <span class="input-group-btn"><button type="button" onclick="removeAnswer($(this))" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button></span>\n' +
        '                        </div>';
    obj.closest('.is_question').find('.dapan-dientu').append(html);
    $('.dapan-sortable').sortable();
}

function addQuestionDientu() {
    var question = $("#list-question").data('count') + 1;
    $("#list-question").data('count', question);
    var html = $('#cauhoi-dientu').html();
    html = html.replace("exercise[][content]", 'exercise[' + question + '][content]');
    html = html.replace("exercise[][type]", 'exercise[' + question + '][type]');
    html = html.replace("exercise[][answer]", 'exercise[' + question + '][answer]');
    html = html.replace("exercise[][video]", 'exercise[' + question + '][video]');
    html = html.replace("exercise[][audio]", 'exercise[' + question + '][audio]');
    html = html.replace('preview-audio-dientu-1-a', 'preview-audio-dientu-' + question);
    html = html.replace('input-audio-dientu-1-a', 'input-audio-dientu-' + question);
    html = html.replace('preview-video-dientu-1-a', 'preview-video-dientu-' + question);
    html = html.replace('input-video-dientu-1-a', 'input-video-dientu-' + question);
    html = html.replace('id="preview-audio-dientu-1-a"', 'id="preview-audio-dientu-' + question + '"');
    html = html.replace('id="input-audio-dientu-1-a"', 'id="input-audio-dientu-' + question + '"');
    html = html.replace('id="preview-video-dientu-1-a"', 'id="preview-video-dientu-' + question + '"');
    html = html.replace('id="input-video-dientu-1-a"', 'id="input-video-dientu-' + question + '"');

    html = html.replace('data-eq="1"', 'data-eq="' + question + '"');
    $('#list-question').append(html);
    $('.uploadDientuVideo').filemanager('file');
    $('.uploadDientuAudio').filemanager('file');
}
