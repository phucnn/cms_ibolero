// set up basic variables for app

var record = document.querySelector('.record');
var stop = document.querySelector('.stop');
var soundClips = document.querySelector('.sound-clips');
var mainSection = document.querySelector('.main-controls');

// disable stop button while not recording

stop.disabled = true;
record.onclick = function () {
    StartMediaRecorder();
}
stop.onclick=function () {
    if (window.streamReference) {
        window.streamReference.getAudioTracks().forEach(function(track) {
            track.stop();
        });
        window.streamReference.getVideoTracks().forEach(function(track) {
            track.stop();
        });
        window.streamReference = null;
    }
}

//main block for doing the audio recording
function StartMediaRecorder() {
    if (navigator.mediaDevices.getUserMedia) {
        console.log('getUserMedia supported.');

        var constraints = {audio: true};
        var chunks = [];

        var onSuccess = function (stream) {
            var mediaRecorder = new MediaRecorder(stream);
            mediaRecorder.start();
            console.log(mediaRecorder.state);
            console.log("recorder started");
            record.style.background = "red";

            stop.disabled = false;
            record.disabled = true;

            stop.onclick = function () {
                mediaRecorder.stop();
                console.log(mediaRecorder.state);
                console.log("recorder stopped");
                record.style.background = "";
                record.style.color = "";
                // mediaRecorder.requestData();

                stop.disabled = true;
                record.disabled = false;
                navigator.mediaDevices.getUserMedia({audio:false});

            }

            mediaRecorder.onstop = function (e) {
                console.log("data available after MediaRecorder.stop() called.");

                var clipName = 'recoder_demo_';
                var clipContainer = document.createElement('article');
                var audio = document.createElement('audio');
                var deleteButton = document.createElement('button');
                var saveButton = document.createElement('save');
                var reader = new FileReader();


                clipContainer.classList.add('clip');
                audio.setAttribute('controls', '');
                deleteButton.textContent = 'Delete';
                deleteButton.className = 'delete';

                saveButton.textContent = 'Save';
                saveButton.className = 'save';


                clipContainer.appendChild(audio);
                clipContainer.appendChild(deleteButton);
                clipContainer.appendChild(saveButton);
                soundClips.appendChild(clipContainer);


                //them file audio vao the tag audio
                audio.controls = true;
                var blob = new Blob(chunks, {'type': 'audio/ogg; codecs="vorbis"'});
                //luu file
                saveButton.onclick = function (e) {
                    uploadAudio(blob);
                };
                chunks = [];

                var audioURL = window.URL.createObjectURL(blob);

                audio.src = audioURL;
                console.log("recorder stopped");


                //an xoa
                deleteButton.onclick = function (e) {
                    evtTgt = e.target;
                    evtTgt.parentNode.parentNode.removeChild(evtTgt.parentNode);
                }

            }

            mediaRecorder.ondataavailable = function (e) {
                chunks.push(e.data);
            }
        }

        var onError = function (err) {
            console.log('The following error occured: ' + err);
        }

        navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onError);

    } else {
        console.log('getUserMedia not supported on your browser!');
    }
}


function uploadAudio(mp3Data) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var fd = new FormData();
        var mp3Name = encodeURIComponent('audio_recording_' + new Date().getTime() + '.ogg');
        console.log("mp3name = " + mp3Name);
        fd.append('title', mp3Name);
        fd.append('file', reader.result);
        $.ajax({
            type: 'POST',
            url: '/admincp/upload-record',
            data: fd,
            processData: false,
            contentType: false
        }).done(function (data) {
            //console.log(data);
        });
    };
    reader.readAsDataURL(mp3Data);
}

function stopStream() {
    if (!window.streamReference) return;

    window.streamReference.getAudioTracks().forEach(function(track) {
        track.stop();
    });

    window.streamReference.getVideoTracks().forEach(function(track) {
        track.stop();
    });

    window.streamReference = null;
}