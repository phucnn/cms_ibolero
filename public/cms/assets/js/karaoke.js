var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent

var colors = [ 'aqua' , 'azure' , 'beige', 'bisque', 'black', 'blue', 'brown', 'chocolate', 'coral', 'crimson', 'cyan', 'fuchsia', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green', 'indigo', 'ivory', 'khaki', 'lavender', 'lime', 'linen', 'magenta', 'maroon', 'moccasin', 'navy', 'olive', 'orange', 'orchid', 'peru', 'pink', 'plum', 'purple', 'red', 'salmon', 'sienna', 'silver', 'snow', 'tan', 'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'white', 'yellow'];
var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;'

var recognition = new SpeechRecognition();
var speechRecognitionList = new SpeechGrammarList();
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;
//recognition.continuous = false;
recognition.lang = 'en-US';
recognition.interimResults = false;
recognition.maxAlternatives = 4;

var diagnostic = document.querySelector('.output');
var bg = document.querySelector('html');
var hints = document.querySelector('.hints');

var colorHTML= '';
colors.forEach(function(v, i, a){
    console.log(v, i);
    colorHTML += '<span style="background-color:' + v + ';"> ' + v + ' </span>';
});
hints.innerHTML = 'Tap/click then say a color to change the background color of the app. Try '+ colorHTML + '.';

document.body.onclick = function() {
    recognition.start();
    console.log('Ready to receive a color command.');
}

recognition.onresult = function(event) {
    // The SpeechRecognitionEvent results property returns a SpeechRecognitionResultList object
    // The SpeechRecognitionResultList object contains SpeechRecognitionResult objects.
    // It has a getter so it can be accessed like an array
    // The [last] returns the SpeechRecognitionResult at the last position.
    // Each SpeechRecognitionResult object contains SpeechRecognitionAlternative objects that contain individual results.
    // These also have getters so they can be accessed like arrays.
    // The [0] returns the SpeechRecognitionAlternative at position 0.
    // We then return the transcript property of the SpeechRecognitionAlternative object

    var last = event.results.length - 1;
    console.log(event.results.length);
    var color = event.results[last][0].transcript;

    diagnostic.textContent = 'Result received: ' + color + '.';
    diagnostic.style.backgroundColor = color;
    console.log('Confidence: ' + event.results[0][0].confidence);
    console.log(event.results);
}

recognition.onspeechend = function() {
    recognition.stop();
}

recognition.onnomatch = function(event) {
    diagnostic.textContent = "I didn't recognise that color.";
}

recognition.onerror = function(event) {
    diagnostic.textContent = 'Error occurred in recognition: ' + event.error;
}











// set up basic variables for app

var record = document.querySelector('.record');
var stop = document.querySelector('.stop');
var soundClips = document.querySelector('.sound-clips');
var mainSection = document.querySelector('.main-controls');

// disable stop button while not recording

stop.disabled = true;
record.onclick = function () {
    StartMediaRecorder();
}


//main block for doing the audio recording
function StartMediaRecorder() {
    if (navigator.mediaDevices.getUserMedia) {
        console.log('getUserMedia supported.');

        var constraints = {audio: true};
        var chunks = [];

        var onSuccess = function (stream) {
            var mediaRecorder = new MediaRecorder(stream);
            mediaRecorder.start();
            console.log(mediaRecorder.state);
            console.log("recorder started");
            record.style.background = "red";

            stop.disabled = false;
            record.disabled = true;

            stop.onclick = function () {
                mediaRecorder.stop();
                console.log(mediaRecorder.state);
                console.log("recorder stopped");
                record.style.background = "";
                record.style.color = "";
                // mediaRecorder.requestData();

                stop.disabled = true;
                record.disabled = false;

                if (stream) {
                    stream.getAudioTracks().forEach(function (track) {
                        track.stop();
                    });
                    stream.getVideoTracks().forEach(function (track) {
                        track.stop();
                    });
                    stream = null;
                }
            }

            mediaRecorder.onstop = function (e) {
                console.log("data available after MediaRecorder.stop() called.");

                var clipName = 'recoder_demo_';
                var clipContainer = document.createElement('article');
                var audio = document.createElement('audio');
                var deleteButton = document.createElement('button');
                var saveButton = document.createElement('save');
                var reader = new FileReader();


                clipContainer.classList.add('clip');
                audio.setAttribute('controls', '');
                deleteButton.textContent = 'Delete';
                deleteButton.className = 'delete';

                saveButton.textContent = 'Save';
                saveButton.className = 'save';


                clipContainer.appendChild(audio);
                clipContainer.appendChild(deleteButton);
                clipContainer.appendChild(saveButton);
                soundClips.appendChild(clipContainer);


                //them file audio vao the tag audio
                audio.controls = true;
                var blob = new Blob(chunks, {'type': 'audio/ogg; codecs="vorbis"'});
                //luu file
                saveButton.onclick = function (e) {
                    // uploadAudio(blob);
                };
                chunks = [];

                var audioURL = window.URL.createObjectURL(blob);

                audio.src = audioURL;
                console.log("recorder stopped");


                //an xoa
                deleteButton.onclick = function (e) {
                    evtTgt = e.target;
                    evtTgt.parentNode.parentNode.removeChild(evtTgt.parentNode);
                }

            }

            mediaRecorder.ondataavailable = function (e) {
                chunks.push(e.data);
            }
        }

        var onError = function (err) {
            console.log('The following error occured: ' + err);
        }

        navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onError);

    } else {
        console.log('getUserMedia not supported on your browser!');
    }
}


function uploadAudio(mp3Data) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var fd = new FormData();
        var mp3Name = encodeURIComponent('audio_recording_' + new Date().getTime() + '.ogg');
        console.log("mp3name = " + mp3Name);
        fd.append('title', mp3Name);
        fd.append('file', reader.result);
        $.ajax({
            type: 'POST',
            url: '/admincp/upload-record',
            data: fd,
            processData: false,
            contentType: false
        }).done(function (data) {
            //console.log(data);
        });
    };
    reader.readAsDataURL(mp3Data);
}

function stopStream() {
    if (!window.streamReference) return;

    window.streamReference.getAudioTracks().forEach(function (track) {
        track.stop();
    });

    window.streamReference.getVideoTracks().forEach(function (track) {
        track.stop();
    });

    window.streamReference = null;
}