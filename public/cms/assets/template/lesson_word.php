
[
    {
        "content": "<li class='col media border-bottom py-4'><div class='image-box ratio-1-1 position-relative mr-3 w-25'><img class='img-post' src='/uploads/photos/shares/thumbnail/1516853462_Verb_tenses.png' style='max-width: 100%;' /></div><div class='media-body'><h5 class='mt-0 mb-1 font-weight-bold text-blue'><button type='button' id='volume_{$id}' class='btn btn-sm btn-blue mr-3'><i class='fal fa-volume-up'></i></button>[Từ khóa]</h5><p>(Loại từ): [Dịch từ]</p><p class='mb-0'>[Ví dụ]</p><p class='text-muted font-italic'>Dịch: [Dịch ví dụ]</p><div class='cl_lesson_div'><button type='button' class='btn btn-danger mb-3 cl_lesson_btn_voice ' id='voice_{$id}''><i class='fal fa-microphone-alt mr-2'></i>Ghi âm&nbsp;</button>&nbsp;&nbsp;<button type='button' class='btn btn-info mb-3 cl_lesson_btn_replay ' id='replay_{$id}'><i class='fal fa-volume-up mr-2'></i>Nghe lại&nbsp;</button></div></div></li>"
    }
]
