<?php

return [
    'default_disk' => 'local',
    'ffmpeg.binaries' => 'D:\xampp71\ffmpeg\bin\ffmpeg',
    'ffmpeg.threads'  => 12,
    'ffprobe.binaries' => 'D:\xampp71\ffmpeg\bin\ffprobe',
    'timeout' => 3600,
];
